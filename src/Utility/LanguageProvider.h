/**
 * @file     LanguageProvider.C
 * @author   Jonas Toth <kontakt@jonas-toth.eu>
 *
 * @brief    Basic functionality to provide language snippets
 *           in an uniform interface for different lanaguages.
 *           These functions are used by all GUIs to provide
 *           translations.
 * @ingroup  alice_masterclass_base
 */

#ifndef LANGUAGEPROVIDER_C
#define LANGUAGEPROVIDER_C

#include <TString.h>
#include <map>
#include <memory>

namespace Utility
{
enum ESupportedLanguages {
  English,
  German,
  NLanguages, ///< last element allows looping over all language with a for loop
};

/// Return the english name for a language
TString GetLanguageName(ESupportedLanguages lang);

/// Return the shortcut for the chose language, e.g. "en" for "English".
TString GetLanguageNameShort(ESupportedLanguages lang);

/// Determine the value in the language enum for a given shortcut for a language.
ESupportedLanguages GetLanguageFromShort(const TString& Shortcut);

/// Determine the language from Environment variables and fallback to English.
ESupportedLanguages LanguageFromEnv();

/// Create Singleton for a translation object corresponding to the language.
/// Useful if the language will not change anymore (everything after the first
/// window).
template <typename Factory>
typename Factory::ReturnType& Translation(ESupportedLanguages lang)
{
  static std::unique_ptr<typename Factory::ReturnType> Translation = Factory::create(lang);
  return *Translation;
}

/// Return a translation object corresponding to the current language in
/// the environment.
template <typename Factory>
typename Factory::ReturnType& TranslationFromEnv()
{
  return Translation<Factory>(LanguageFromEnv());
}

/**
 * This class serves as an baseclass to provide basic infrastructure
 * to save a mapping of a TextID to a TextContent.
 *
 * Each translateable text must be inserted by a subclass. Access to the texts
 * will be provided by the subclasses via an typesafe enum-based interface.
 *
 * The advantage is that one gets correctness guarantees on compiletime. If
 * only the string is choosen, typos and other errors are hard to find and
 * easy to make.
 */
class TLanguageProvider
{
 public:
  TLanguageProvider(ESupportedLanguages language);
  ESupportedLanguages GetLanguage() const { return fLanguage; }
  virtual ~TLanguageProvider() = default;

 protected:
  /// Return the translated text for a specific text uniquely identified by 'TextID'.
  /// Called by subclasses that have a mapping for language snippet to 'TextID'.
  const TString& GetText(const TString& TextID) const;

  /// Save 'TextContent' under the key 'TextID'. This method will be called
  /// by the subclasses upon initialization.
  void RegisterText(const TString& TextID, const TString& TextContent)
  {
    fTextSnippets[TextID] = TextContent;
  }

 private:
  std::map<TString, TString> fTextSnippets;
  ESupportedLanguages fLanguage;
};

#define STR(x) #x
/// Define a utility macro that creates a method to query a specific key
/// used to retrieve a translation.
#define LANG_KEY(KeyName) \
  const TString& KeyName() const { return GetText(STR(KeyName)); }

/// Define a utility macro that creates a new subclass for a translation
/// and includes the registrations from a specific translation file.
#define LANG_TRANSLATION(LanguageClass, EnumVal)           \
  struct LanguageClass##EnumVal : LanguageClass##English { \
    LanguageClass##EnumVal();                              \
  };                                                       \
  inline LanguageClass##EnumVal::LanguageClass##EnumVal()  \
    : LanguageClass##English(Utility::EnumVal)
} // namespace Utility

#endif /* end of include guard: LANGUAGEPROVIDER_C */
