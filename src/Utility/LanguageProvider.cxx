#include "LanguageProvider.h"

#include <TString.h>
#include <sstream>
#include <stdexcept>
#include <utility>

#include "Utility/Utilities.h"

namespace Utility
{
TString GetLanguageName(ESupportedLanguages lang)
{
  switch (lang) {
    case English:
      return "English";
    case German:
      return "German";
    case NLanguages:
      break;
  }
  throw std::runtime_error("Invalid Language passed");
}

TString GetLanguageNameShort(ESupportedLanguages lang)
{
  switch (lang) {
    case English:
      return "en";
    case German:
      return "de";
    case NLanguages:
      break;
  }
  throw std::runtime_error("Invalid Language passed");
}

ESupportedLanguages GetLanguageFromShort(const TString& Shortcut)
{
  if (Shortcut == "en") {
    return English;
  }
  if (Shortcut == "de") {
    return German;
  }
  throw std::runtime_error("Could not determine language from shortcut");
}

ESupportedLanguages LanguageFromEnv()
{
  ESupportedLanguages lang = English;
  try {
    lang = GetLanguageFromShort(GetEnvValue("ALICE_MASTERCLASS_LANG"));
  } catch (std::runtime_error&) {
  }
  return lang;
}

TLanguageProvider::TLanguageProvider(ESupportedLanguages language)
  : fLanguage(language)
{
}

const TString& TLanguageProvider::GetText(const TString& TextID) const
{
  auto translated = fTextSnippets.find(TextID);
  if (translated == fTextSnippets.end()) {
    std::stringstream error_msg;
    error_msg << "Requested snippet '" << TextID.Data()
              << "' does not exist in this translation!\n";
    throw std::runtime_error(error_msg.str());
  }

  return translated->second;
}
} // namespace Utility
