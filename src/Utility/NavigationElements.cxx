#include "Utility/NavigationElements.h"

#include <TGButton.h>
#include <TGClient.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TString.h>

#include "Utility/GUITranslation.h"
#include "Utility/Instructions.h"

namespace Utility
{
namespace internal
{
TInstructionsFrame::TInstructionsFrame(TGCompositeFrame* parent, const TString& FrameText)
  : TGGroupFrame(parent, FrameText, kVerticalFrame)
  , fButtonInstructions(MakeInstructionsButton(this))
{
}

TEventsFrame::TEventsFrame(TGCompositeFrame* parent, const TGUIEnglish& Translation, Bool_t extendedEventNavigation)
  : TGGroupFrame(parent, Translation.EventNavigation(), kVerticalFrame)
  , fEventsHorizontalFrameLabels(new TGHorizontalFrame(this))
  , fEventsHorizontalFrameButtons(new TGHorizontalFrame(this))
  , fButtonPrevious(
      new TGPictureButton(fEventsHorizontalFrameButtons, gClient->GetPicture("tb_back.xpm")))
  , fLabelEventNumber(new TGLabel(fEventsHorizontalFrameButtons))
  , fButtonNext(
      new TGPictureButton(fEventsHorizontalFrameButtons, gClient->GetPicture("tb_forw.xpm")))
  , fLabelDataset(new TGLabel(this))
  , fButtonEventAnalyzed(nullptr)
  , fEventsHorizontalProgress(new TGHorizontalFrame(this))
  , fEventAnalysed(new TGLabel(fEventsHorizontalProgress))
{
  auto* ExpandX = new TGLayoutHints(kLHintsExpandX);
  // Labels
  fEventsHorizontalFrameLabels->AddFrame(
    new TGLabel(fEventsHorizontalFrameLabels, Translation.ButtonPrevious()), ExpandX);
  fEventsHorizontalFrameLabels->AddFrame(
    new TGLabel(fEventsHorizontalFrameLabels, Translation.ButtonCurrent()), ExpandX);
  fEventsHorizontalFrameLabels->AddFrame(
    new TGLabel(fEventsHorizontalFrameLabels, Translation.ButtonNext()), ExpandX);
  this->AddFrame(fEventsHorizontalFrameLabels, ExpandX);

  // --- Event navigation ------------------------------------------
  // Go to Previous event with button
  fEventsHorizontalFrameButtons->AddFrame(fButtonPrevious, ExpandX);
  fButtonPrevious->SetToolTipText(Translation.TipPreviousEvent());

  // The event number label
  fLabelEventNumber->SetText(1);
  fEventsHorizontalFrameButtons->AddFrame(fLabelEventNumber, ExpandX);

  // Go to next event with button
  fEventsHorizontalFrameButtons->AddFrame(fButtonNext, ExpandX);
  fButtonNext->SetToolTipText(Translation.TipNextEvent());
  this->AddFrame(fEventsHorizontalFrameButtons, ExpandX);

  if(extendedEventNavigation)
  {
  // Display information about the current event
  fLabelDataset->SetText("");
  this->AddFrame(fLabelDataset, ExpandX);

  // Button to do finish the current event
  fButtonEventAnalyzed = new TGTextButton(this, Translation.ButtonEventAnalyzed());
  fButtonEventAnalyzed->SetToolTipText(Translation.TipEventAnalyzed());
  this->AddFrame(fButtonEventAnalyzed, ExpandX);

  // new section about the current progress in the masterclass.
  fEventsHorizontalProgress->AddFrame(
    new TGLabel(fEventsHorizontalProgress, Translation.EventDone()), ExpandX);
  fEventsHorizontalProgress->AddFrame(fEventAnalysed, ExpandX);
  fEventAnalysed->SetText(0);
  this->AddFrame(fEventsHorizontalProgress, ExpandX);
  }
}
} // namespace internal
} // namespace Utility
