#include "Info.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TGTab.h>
#include <TString.h>
#include <iostream>

#include "Utility/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"

class TGPicture;
class TGWindow;

namespace Utility
{
/**
 * Constructor
 *
 * @param title  The title to show
 * @param names  Null terminated list of names to show
 * @param p      Parent
 * @param w      Width
 * @param h      Height
 */
InfoBox::InfoBox(const TString& title, const std::vector<TString>& names, const TGWindow* p,
                 UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h)
  , fTitle(title)
  , fTranslation(TranslationFromEnv<TUtilityLanguage>())
{
  auto *hf = new TGHorizontalFrame(this, 250, 250);
  auto *tab = new TGTab(hf);

  for (const auto& name : names) {
    auto *tabframe = tab->AddTab(name);

    ///TODO: fix this leftover hack
    TString tmp = name;
    tmp.ReplaceAll("+", "");

    const char* SlotCall = Form("MakeBigger(=\"%s\")", tmp.Data());

    auto* hftab = new TGHorizontalFrame(tabframe);

    TString picture_name = Form("%s_small.png", tmp.Data());
    const TGPicture *pic = Picture("Utility", picture_name);
    fPictures.push_back(pic);
    auto *b = new TGPictureButton(hftab, pic);

    std::cerr << "Creating slot for pictures: " << SlotCall << "\n";
    b->Connect("Clicked()", "Utility::InfoBox", this, SlotCall);
    hftab->AddFrame(b, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
    tabframe->AddFrame(hftab);
  }

  hf->AddFrame(tab);

  AddFrame(hf, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY, 1, 1, 5, 1));

  SetWindowName(title);
  MapSubwindows();

  // Initialize the layout algorithm via Resize()
  TGDimension size = GetDefaultSize();
  Resize(size);

  //Make the window non-resizable
  SetWMSize(size.fWidth, size.fHeight);
  SetWMSizeHints(size.fWidth, size.fHeight, 10000, 10000, 1, 1);

  SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize | kMWMDecorMinimize | kMWMDecorMenu,
              kMWMFuncAll |  kMWMFuncResize    | kMWMFuncMaximize | kMWMFuncMinimize,
              kMWMInputModeless);

  // Map main frame
  MapWindow();
}

InfoBox::~InfoBox()
{
}
} // namespace Utility
