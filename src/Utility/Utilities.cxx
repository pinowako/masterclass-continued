#include "Utilities.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TError.h>
#include <TFile.h>
#include <TGClient.h>
#include <TGMsgBox.h>
#include <TString.h>
#include <TStyle.h>
#include <TSystem.h>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "Utility/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/ResourceDir.h"

class TGPicture;

namespace Utility
{
void SetupStyleForClass()
{
  std::cerr << "Controlling visual settings to run masterclass\n";
  gStyle->SetOptStat(110);
  gStyle->SetPalette(1, nullptr);
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetTitleFillColor(0);
  gStyle->SetTextSize(0.5);
  gStyle->SetLabelSize(0.03, "xyz");
  gStyle->SetLabelOffset(0.002, "xyz");
  gStyle->SetTitleFontSize(0.04);
  gStyle->SetTitleOffset(1, "y");
  gStyle->SetTitleOffset(0.7, "x");
  gStyle->SetCanvasColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
}

TString GetEnvValue(const TString& env_var_name)
{
  if (gSystem->Getenv(env_var_name) != nullptr) {
    return TString(gSystem->Getenv(env_var_name));
  }

  return TString("");
}

TString MasterClassPath(const TString& filename)
{
  std::cerr << "MClass Path requested for '" << filename << "'\n";
  TString path;
  if (GetEnvValue("ALICE_MASTERCLASS").Length() > 0) {
    path = gSystem->ExpandPathName("$ALICE_MASTERCLASS/");
  }
  path.Append(filename);
  return path;
}

TString ResourcePath(const TString& file)
{
  const char* path = gSystem->ConcatFileName(gResourceDir.c_str(), file);
  TString result = path;
  delete[] path;
  return result;
}

const TGPicture* Picture(const char* module, const char* name)
{
  TString subsituted_path = Form("doc/%s/%s", module, name);
  std::cout << subsituted_path << "\n";

  TString mclass_path = ResourcePath(subsituted_path);
  std::cout << mclass_path << "\n";

  return gClient->GetPicture(mclass_path);
}

TString SetMasterClassPath(const char* self, Int_t depth)
{
  TString dir = GetEnvValue("ALICE_MASTERCLASS");
  if (dir.Length() > 0) {
    return dir;
  }

  // TODO Isn't this dead? I believe ALICE_MASTERCLASS is always set
  TString script;
  for (Int_t i = 0; i < gApplication->Argc(); i++) {
    if (TString(gApplication->Argv(i)).Contains(self)) {
      script = gApplication->Argv(i);
      break;
    }
  }

  // TODO refactor termination
  if (script.IsNull()) {
    Warning("MasterClass.C", "Couldn't find top-directory");
    TGUIEnglish& Translation = TranslationFromEnv<TUtilityLanguage>();
    new TGMsgBox(gClient->GetRoot(), gClient->GetRoot(),
                 Translation.ErrorInvalidDirectory() /*"Invalid directory"*/,
                 Translation.ErrorInvalidDirectoryMsg() /*"Couldn't find top-directory"*/,
                 kMBIconStop);
    gApplication->Terminate();
    return dir;
  }

  if (!gSystem->IsAbsoluteFileName(script)) {
    script = gSystem->ConcatFileName(gSystem->WorkingDirectory(), script);
    script = gSystem->ExpandPathName(script.Data());
  }

  // Change directory to top-directory
  dir = script;
  for (Int_t i = 0; i < depth; i++) {
    dir = gSystem->DirName(dir);
  }

  Printf("Setting ALICE_MASTERCLASS=\"%s\"", dir.Data());
  gSystem->Setenv("ALICE_MASTERCLASS", dir);
  return dir;
}

TFile* File(const TString& dir, const TString& name)
{
  TString rel = "";
  if (dir.Length() == 0) {
    rel = name;
  } else {
    rel = gSystem->ConcatFileName(dir, name);
  }

  TFile* file = TFile::Open(rel, "READ");

  if (file == nullptr) {
    ::Warning("File", "Failed to open \"%s\"", rel.Data());
  }

  return file;
}

Float_t Clamp(Float_t val, Float_t min, Float_t max)
{
    return TMath::Min(TMath::Max(min, val), max);
}
} // namespace Utility
