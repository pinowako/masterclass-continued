#include "Utility/TEventVisualization.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TError.h>
#include <TEveBrowser.h>
#include <TEveElement.h>
#include <TEveGeoShape.h>
#include <TEveGeoShapeExtract.h>
#include <TEveManager.h>
#include <TEveProjectionAxes.h>
#include <TEveProjectionManager.h>
#include <TEveProjections.h>
#include <TEveScene.h>
#include <TEveViewer.h>
#include <TEveWindow.h>
#include <TFile.h>
#include <TGLRnrCtx.h>
#include <TGLViewer.h>
#include <TObject.h>
#include <TSystem.h>
#include <iostream>
#include <list>
#include <stdexcept>

#include "Utility/ERenderElements.h"
#include "Utility/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/Utilities.h"
#include "Utility/VSDReader.h"

namespace
{
gsl::not_null<gsl::owner<TEveProjectionAxes*>> CreateAxes(
  gsl::not_null<TEveProjectionManager*> Parent, const TString& Name)
{
  gsl::not_null<gsl::owner<TEveProjectionAxes*>> a(new TEveProjectionAxes(Parent));
  a->SetMainColor(kWhite);
  a->SetTitle(Name);
  a->SetTitleSize(0.05);
  a->SetTitleFont(102);
  a->SetLabelSize(0.025);
  a->SetLabelFont(102);
  return a;
}
} // namespace
namespace Utility
{
TEventVisualization::TEventVisualization(gsl::not_null<TEveManager*> EveManager,
                                         gsl::not_null<TEveEventManager*> EventManager)
  : fEveManager(EveManager)
  , fParentEvent(EventManager)
  , fRPhiMgr(new TEveProjectionManager(TEveProjection::kPT_RPhi))
  , fRhoZMgr(new TEveProjectionManager(TEveProjection::kPT_RhoZ))
  , fVSDReference(nullptr)
{
  const TGUIEnglish& Translation(TranslationFromEnv<TUtilityLanguage>());

  // --- Scenes ----------------------------------------------------
  fRPhiGeomScene =
    fEveManager->SpawnNewScene(Translation.SceneRPhiGeom(), Translation.SceneRPhiDesc());
  fRhoZGeomScene =
    fEveManager->SpawnNewScene(Translation.SceneRhoZGeom(), Translation.SceneRhoZDesc());
  fRPhiEventScene =
    fEveManager->SpawnNewScene(Translation.SceneRPhiEventTitle(), Translation.SceneRPhiEventDesc());
  fRhoZEventScene =
    fEveManager->SpawnNewScene(Translation.SceneRhoZEventTitle(), Translation.SceneRhoZEventDesc());

  // --- Projection managers ---------------------------------------
  fEveManager->AddToListTree(fRPhiMgr, kFALSE);
  fRPhiGeomScene->AddElement(CreateAxes(fRPhiMgr, "R-Phi"));

  fEveManager->AddToListTree(fRhoZMgr, kFALSE);
  fRhoZGeomScene->AddElement(CreateAxes(fRhoZMgr, "Rho-Z"));

  // --- Viewers ---------------------------------------------------
  TEveWindowSlot* slot = nullptr;
  TEveWindowPack* pack = nullptr;

  slot = TEveWindow::CreateWindowInTab(fEveManager->GetBrowser()->GetTabRight());
  pack = slot->MakePack();
  pack->SetElementName(Translation.MultiView());
  pack->SetHorizontal();
  pack->SetShowTitleBar(kFALSE);
  pack->NewSlot()->MakeCurrent();

  f3DView = fEveManager->SpawnNewViewer(Translation.View3D(), "");
  f3DView->AddScene(fEveManager->GetGlobalScene());
  f3DView->AddScene(fEveManager->GetEventScene());

  pack = pack->NewSlot()->MakePack();
  pack->SetShowTitleBar(kFALSE);
  pack->NewSlot()->MakeCurrent();

  fRPhiView = fEveManager->SpawnNewViewer(Translation.ViewRPhi(), "");
  fRPhiView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
  fRPhiView->AddScene(fRPhiGeomScene);
  fRPhiView->AddScene(fRPhiEventScene);

  pack->NewSlot()->MakeCurrent();
  fRhoZView = fEveManager->SpawnNewViewer(Translation.ViewRhoZ(), "");
  fRhoZView->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
  fRhoZView->AddScene(fRhoZGeomScene);
  fRhoZView->AddScene(fRhoZEventScene);

  // Extracted from VSDReader
  f3DView->GetGLViewer()->SetStyle(TGLRnrCtx::kOutline);
}

void TEventVisualization::SetupVSD(gsl::not_null<TEveVSD *> VSDDataRef)
{
  fVSDReference = VSDDataRef;
}

void TEventVisualization::LoadDetectorGeometry(const TString& FileName)
{
  TFile* geom = File(gSystem->DirName(FileName), gSystem->BaseName(FileName));

  if (geom == nullptr)
    throw std::runtime_error(Form("Couldn't open geometry file \"%s\"", FileName.Data()));

  auto* gse = dynamic_cast<TEveGeoShapeExtract*>(geom->Get("Gentle"));
  geom->Close();

  fGeomGentle = TEveGeoShape::ImportShapeExtract(gse, nullptr);
  fGeomGentle->IncDenyDestroy();

  SetupDefaultDetectorGeometry();
} // namespace Utility

void TEventVisualization::SetupDefaultDetectorGeometry()
{
  TEveSceneList* sceneList = fEveManager->GetScenes();
  auto i = sceneList->BeginChildren();
  const auto end = sceneList->EndChildren();
  Int_t n = 0;
  while (i != end) {
    switch (n) {
      case 0:
      case 2:
      case 3: {
        auto* sub = dynamic_cast<TEveSceneList*>(*i);
        if (sub != nullptr) {
          sub->RemoveElements();
        }
        break;
      }
    }
    n++;
    i++;
  }
  // GlobalElement means that it will stay even when the Event switches.
  // These elements are meant to be 'long living' in the view.
  fEveManager->AddGlobalElement(fGeomGentle);
  fDetectorExists = true;

  SetDepth(-10);
  DestroyGeomRPhi();
  ImportGeomRPhi(fGeomGentle);

  DestroyGeomRhoZ();
  ImportGeomRhoZ(fGeomGentle);
  SetDepth(0);
  SetRPhiAxes();
  SetRhoZAxes();
}

namespace
{
void DestroyChildrenFromScene(TEveSceneList* SceneList, int IdxToDestroy)
{
  if (IdxToDestroy < 0)
    std::invalid_argument("Provide non-negative argument for destruction!");

  int idx = 0;
  auto begin = SceneList->BeginChildren(), end = SceneList->EndChildren();
  // Try to iterate to the Element in the list with idx == IdxToDestroy...
  for (/* Empty */; begin != end && idx < IdxToDestroy; begin++, idx++)
    ;

  // ... If the List does not have enough elements, throw an exception to signal
  // failure.
  if (idx != IdxToDestroy)
    std::runtime_error("List does not contain elements, prevent out of bounds!");

  // Remove the elements at this point if point was reachable.
  (*begin)->RemoveElements();
}
} // namespace

void TEventVisualization::DestroyDetectorGeometry()
{
  std::cerr << "Destroying Detector Geometry\n";

  TEveSceneList* SceneList = fEveManager->GetScenes();
  std::cerr << "SceneList ptr " << SceneList << "\n";

  DestroyChildrenFromScene(SceneList, 0);
  DestroyChildrenFromScene(SceneList, 2);
  DestroyChildrenFromScene(SceneList, 4);

  fDetectorExists = false;

  SetDepth(-10);
  DestroyGeomRPhi();
  DestroyGeomRhoZ();
  SetDepth(0);
  SetRPhiAxes();
  SetRhoZAxes();
}

namespace
{
template <typename T>
bool RenderablesExist(const T& Container)
{
  return std::all_of(std::begin(Container), std::end(Container),
                     [](const std::unique_ptr<IRenderable>& P) { return P != nullptr; });
}
} // namespace

void TEventVisualization::HideAll()
{
  Expects(RenderablesExist(fRenderables));
  for (auto& Renderable : fRenderables)
    Renderable->RemoveIfNecessary();

  ///TODO: Reloading of the geometry leaks memory! This is not fixed; preventing reloading avoids the problem.
  //HideDetector();

  // Drop old visualization structures.
  fEveManager->GetViewers()->DeleteAnnotations();

  Ensures(std::all_of(std::begin(fRenderables), std::end(fRenderables),
                      [](const std::unique_ptr<IRenderable>& R) { return !R->IsDisplayed(); }));
//  Ensures(!fDetectorExists);
}

void TEventVisualization::ShowDetector()
{
  if (!fDetectorExists)
    SetupDefaultDetectorGeometry();
  Ensures(fDetectorExists);
}
void TEventVisualization::HideDetector()
{
  if (fDetectorExists)
    DestroyDetectorGeometry();
  Ensures(!fDetectorExists);
}

void TEventVisualization::DispatchMouseSelection(TEveElement* el)
{
  ERenderElements E = StringToRenderElement(el->GetElementName());

  switch (E) {
      // Clicking on the Clusters results in a display of the Encyclopia data.
    case ERenderElements::kClustersITS:
      Emit("SelectedClustersITS()");
      break;
    case ERenderElements::kClustersTPC:
      Emit("SelectedClustersTPC()");
      break;
    case ERenderElements::kClustersTRD:
    case ERenderElements::kClustersTOF:
      Emit("SelectedClustersTRDTOF()");
      break;

    case ERenderElements::kESDTrack: {
      auto* track = dynamic_cast<TEveTrack*>(el);
      Expects(track != nullptr);
      Emit("SelectedESDTrack(TEveTrack*)", track);
    } break;

    case ERenderElements::kV0TrackNegative:
    case ERenderElements::kV0TrackPositive: {
      auto* track = dynamic_cast<TEveTrack*>(el);
      Expects(track != nullptr);
      Emit("SelectedV0Track(TEveTrack*)", track);
    } break;

    case ERenderElements::kV0PointingLine: {
      auto* line = dynamic_cast<TEveLine*>(el);
      Expects(line != nullptr);
      auto* neg = dynamic_cast<TEveTrack*>(line->FirstChild());
      auto* pos = dynamic_cast<TEveTrack*>(line->LastChild());
      Expects(neg != nullptr && pos != nullptr);

      Emit("SelectedV0PointingLine(TEveTrack*)", neg);
      Emit("SelectedV0PointingLine(TEveTrack*)", pos);

      TEveVector pca = line->GetLineEnd();
      ZoomAt(pca);
    } break;
    case ERenderElements::kCascadeTrackNegative:
    case ERenderElements::kCascadeTrackPositive:
    case ERenderElements::kCascadeTrackBachelor: {
      auto* track = dynamic_cast<TEveTrack*>(el);
      Expects(track != nullptr);

      Emit("SelectedCascadeTrack(TEveTrack*)", track);
    } break;
    case ERenderElements::kCascadeLine1: {
      auto* line = dynamic_cast<TEveLine*>(el);
      Expects(line != nullptr);
      auto* bac = dynamic_cast<TEveTrack*>(line->FirstChild());
      Expects(bac != nullptr);

      Emit("SelectedCascadeTrack(TEveTrack*)", bac);

      TEveVector pca = line->GetLineEnd();
      ZoomAt(pca);
    } break;
    case ERenderElements::kCascadeLine2: {
      auto* line = dynamic_cast<TEveLine*>(el);
      Expects(line != nullptr);
      auto* neg = dynamic_cast<TEveTrack*>(line->FirstChild());
      auto* pos = dynamic_cast<TEveTrack*>(line->LastChild());
      Expects(neg != nullptr && pos != nullptr);

      Emit("SelectedCascadeTrack(TEveTrack*)", neg);
      Emit("SelectedCascadeTrack(TEveTrack*)", pos);

      TEveVector pca = line->GetLineEnd();
      ZoomAt(pca);
    } break;
    case ERenderElements::kESDTracks:
    case ERenderElements::kV0NegativeTracks:
    case ERenderElements::kV0PositiveTracks:
    case ERenderElements::kV0s:
    case ERenderElements::kCascadeBachelorTracks:
    case ERenderElements::kCascadePositiveTracks:
    case ERenderElements::kCascadeNegativeTracks:
    case ERenderElements::kCascadeV0s:
    case ERenderElements::kAxisX:
    case ERenderElements::kAxisY:
    case ERenderElements::kAxisZ:
    case ERenderElements::kIP:
    case ERenderElements::kIPLine:
      break; // These elements are not interesting in a selection.
  }
} // namespace Utility

void TEventVisualization::SetDepth(Float_t d)
{
  fRPhiMgr->SetCurrentDepth(d);
  fRhoZMgr->SetCurrentDepth(d);
}

void TEventVisualization::ZoomAt(TEveVector v)
{
  Expects(fRPhiView != nullptr);
  Expects(fRhoZView != nullptr);

  gsl::not_null<TGLViewer*> glv_phi = fRPhiView->GetGLViewer();
  glv_phi->CurrentCamera().Reset();
  glv_phi->CurrentCamera().Truck(v.fX - 39.8, v.fY + 44.0);
  glv_phi->CurrentCamera().Dolly(200000, true, true);
  glv_phi->DoDraw();

  gsl::not_null<TGLViewer*> glv_rho = fRhoZView->GetGLViewer();
  glv_rho->CurrentCamera().Reset();
  glv_rho->CurrentCamera().Truck(v.fZ, v.Perp() + 10);
  glv_rho->CurrentCamera().Dolly(200000, true, true);
  glv_rho->DoDraw();
}

void TEventVisualization::SetRPhiAxes()
{
  fEveManager->AddToListTree(fRPhiMgr, kFALSE);
  auto* a = new TEveProjectionAxes(fRPhiMgr);
  a->SetMainColor(kWhite);
  a->SetTitle("R-Phi");
  a->SetTitleSize(0.05);
  a->SetTitleFont(102);
  a->SetLabelSize(0.025);
  a->SetLabelFont(102);
  fRPhiGeomScene->AddElement(a);
}

void TEventVisualization::SetRhoZAxes()
{
  fEveManager->AddToListTree(fRhoZMgr, kFALSE);
  auto* a = new TEveProjectionAxes(fRhoZMgr);
  a->SetMainColor(kWhite);
  a->SetTitle("Rho-Z");
  a->SetTitleSize(0.05);
  a->SetTitleFont(102);
  a->SetLabelSize(0.025);
  a->SetLabelFont(102);
  fRhoZGeomScene->AddElement(a);
}

void TEventVisualization::ShowRenderable(ERenderablePositions Idx, Bool_t WithVSD)
{
  Expects(RenderablesExist(fRenderables));
  std::cerr << "Ensuring Element # " << Idx << " is display!\n";

  // Reload the Renderable in this case (Fixes bug for redisplaying something
  // with different configuration, like tracks).
  if (fRenderables.at(Idx)->IsDisplayed())
    fRenderables.at(Idx)->Remove();

  // Clear the Right side of the views (orthogonal from one perspective)
  // to set it up afterwards. This fixes a problem, where these views are not
  // cleared properly.
  DestroyEventRPhi();
  DestroyEventRhoZ();

  if (WithVSD) {
    Expects(fVSDReference != nullptr);
    fRenderables.at(Idx)->Display(*fVSDReference);
  } else {
    fRenderables.at(Idx)->Display();
  }

  // Import the parent event in the side views to ensure consistent view of
  // all parts. Bad solution, but the views don't figure it out themself.
  ImportEventRPhi(fParentEvent);
  ImportEventRhoZ(fParentEvent);

  Ensures(fRenderables.at(Idx)->IsDisplayed());
}

void TEventVisualization::HideRenderable(ERenderablePositions Idx)
{
  Expects(RenderablesExist(fRenderables));

  // Clear the Right side of the views (orthogonal from one perspective)
  // to set it up afterwards. This fixes a problem, where these views are not
  // cleared properly.
  DestroyEventRPhi();
  DestroyEventRhoZ();

  fRenderables.at(Idx)->RemoveIfNecessary();

  // Import the parent event in the side views to ensure consistent view of
  // all parts. Bad solution, but the views don't figure it out themself.
  ImportEventRPhi(fParentEvent);
  ImportEventRhoZ(fParentEvent);

  Ensures(!fRenderables.at(Idx)->IsDisplayed());
}

void ConnectDataAndRenderables(VSDReader* Data, TEventVisualization& Vis)
{
  Vis.fRenderables.at(TEventVisualization::kAxesPos) =
    std_fix::make_unique<TRenderableAxes>(Vis.fEveManager, Vis.fParentEvent);
  Vis.fRenderables.at(TEventVisualization::kIPPos) =
    std_fix::make_unique<TRenderableIP>(Vis.fEveManager, Vis.fParentEvent);
  Vis.fRenderables.at(TEventVisualization::kClustersPos) =
    std_fix::make_unique<TRenderableClusters>(Vis.fEveManager, Vis.fParentEvent);
  Vis.fRenderables.at(TEventVisualization::kTracksPos) =
    std_fix::make_unique<TRenderableTracks>(Vis.fEveManager, Vis.fParentEvent, &Data->fTracks);
  Vis.fRenderables.at(TEventVisualization::kV0Pos) = std_fix::make_unique<TRenderableV0s>(
    Vis.fEveManager, Vis.fParentEvent, &Data->fV0Neg, &Data->fV0Pos, &Data->fV0s);
  Vis.fRenderables.at(TEventVisualization::kCascadesPos) =
    std_fix::make_unique<TRenderableCascades>(Vis.fEveManager, Vis.fParentEvent, &Data->fCascadeNeg,
                                              &Data->fCascadePos, &Data->fCascadeBachelor,
                                              &Data->fCascadeV0s);

  Ensures(RenderablesExist(Vis.fRenderables));
}

} // namespace Utility
