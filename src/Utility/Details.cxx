#include "Details.h"

#include <RtypesCore.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TString.h>
#include <TRootEmbeddedCanvas.h>
#include <TImage.h>

#include "Utility/Utilities.h"

class TGWindow;

namespace Utility
{
Details::Details(TString what, TString module, const TString& title, const TGWindow* p, UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h)
  , fPicture(nullptr)
  , fCanvas(nullptr)
{
  // Special case
  if (what.EqualTo("TRD+TOF")) {
    what = "TRDTOF";
  }
  fPicture = Picture(module.Data(), Form("%s.png", what.Data()));
  auto* hf = new TGHorizontalFrame(this, 250, 250);
  auto* b1 = new TGPictureButton(hf, fPicture, -1, TGPictureButton::GetDefaultGC().GetGC(), 0);
  SetWindowName(Form("%s - %s", title.Data(), what.Data()));

  hf->AddFrame(b1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  AddFrame(hf);
  MapSubwindows();
  TGDimension size = GetDefaultSize();
  Resize(size);

  //Make the window non-resizable
  SetWMSize(size.fWidth, size.fHeight);
  SetWMSizeHints(size.fWidth, size.fHeight, 10000, 10000, 1, 1);

  SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize | kMWMDecorMinimize | kMWMDecorMenu,
              kMWMFuncAll |  kMWMFuncResize    | kMWMFuncMaximize | kMWMFuncMinimize,
              kMWMInputModeless);
  MapWindow();
}

Details::~Details() { }

} // namespace Utility
