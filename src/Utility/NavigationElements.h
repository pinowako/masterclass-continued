#ifndef NAVIGATIONELEMENTS_H_DNGHMQ8K
#define NAVIGATIONELEMENTS_H_DNGHMQ8K

#include <Rtypes.h>
#include <TGFrame.h>

class TBuffer;
class TClass;
class TGButton;
class TGLabel;
class TGPictureButton;
class TGTextButton;
class TMemberInspector;
class TString;

namespace Utility
{
class VSDReader;
// Forward declaration to reduce header inclusions.
struct TGUIEnglish;

namespace internal
{
struct TInstructionsFrame : TGGroupFrame {
  TGTextButton* fButtonInstructions;
  TInstructionsFrame(TGCompositeFrame* parent, const TString& FrameText);
  ClassDef(TInstructionsFrame, 0);
};

struct TEventsFrame : TGGroupFrame {
  TGHorizontalFrame* fEventsHorizontalFrameLabels;
  TGHorizontalFrame* fEventsHorizontalFrameButtons;
  TGPictureButton* fButtonPrevious;
  TGLabel* fLabelEventNumber;
  TGPictureButton* fButtonNext;
  TGLabel* fLabelDataset;
  TGTextButton* fButtonEventAnalyzed;
  TGHorizontalFrame* fEventsHorizontalProgress;
  TGLabel* fEventAnalysed;

  TEventsFrame(TGCompositeFrame* parent, const TGUIEnglish& Translation, Bool_t extendedEventNavigation);

  ClassDef(TEventsFrame, 0);
};
} // namespace internal
} // namespace Utility

#endif /* end of include guard: NAVIGATIONELEMENTS_H_DNGHMQ8K */
