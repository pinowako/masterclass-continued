#include "Utility/Renderables.h"

#include <TEveEventManager.h>
#include <TEveLine.h>
#include <TEveManager.h>
#include <TEveTrack.h>
#include <TEveVSD.h>

#include "Utility/ERenderElements.h"
#include "Utility/GUITranslation.h"

namespace Utility
{
void IRenderable::Display(const TEveVSD& VSDData)
{
  Expects(!IsDisplayed());
  Setup(VSDData);
  RegisterInEventManager();
  fIsDisplayed = true;
  Ensures(IsDisplayed());
}

void IRenderable::Display()
{
  Expects(!IsDisplayed());
  Setup();
  RegisterInEventManager();
  fIsDisplayed = true;
  Ensures(IsDisplayed());
}

void IRenderable::Remove()
{
  Expects(IsDisplayed());
  UnregisterFromEventManager();
  TearDown();
  fIsDisplayed = false;
  Ensures(!IsDisplayed());
}

void IRenderable::RemoveIfNecessary()
{
  if (IsDisplayed())
    Remove();
  Ensures(!IsDisplayed());
}

namespace
{
template <typename T> // requires Iteratable(T)
void InitAxis(T& LineArray, const TString& Name)
{
  for (auto& Line : LineArray) {
    Line = new TEveLine(Name);
    Line->SetLineColor(kRed);
    Line->SetLineWidth(1);
    Line->SetLineStyle(1);
    // Increase reference counter, because the pointers are stored in this class.
    Line->IncDenyDestroy();
  }
}

template <typename T> // requires Iteratable(T)
void UninitAxis(T& LineArray)
{
  for (auto& Line : LineArray) {
    Line->DecDenyDestroy();
    Line = nullptr;
  }
}

template <typename T> // requires Iteratable(T)
void RegisterAxis(gsl::not_null<TEveManager*> Manager, gsl::not_null<TEveEventManager*> ParentEvent,
                  const T& LineArray)
{
  for (auto* Line : LineArray)
    Manager->AddElement(Line, ParentEvent);
}
template <typename T> // requires Iteratable(T)
void UnregisterAxis(gsl::not_null<TEveManager*> Manager,
                    gsl::not_null<TEveEventManager*> ParentEvent, const T& LineArray)
{
  for (auto* Line : LineArray)
    Manager->RemoveElement(Line, ParentEvent);
}
} // namespace

void TRenderableAxes::Setup()
{
  Expects(!IsDisplayed());

  InitAxis(fAxisX, RenderElementToString(ERenderElements::kAxisX));
  InitAxis(fAxisY, RenderElementToString(ERenderElements::kAxisY));
  InitAxis(fAxisZ, RenderElementToString(ERenderElements::kAxisZ));

  // line X
  fAxisX[0]->SetPoint(0, -100, 0, 0);

  fAxisX[0]->SetPoint(1, 100, 0, 0);

  // arrows X
  fAxisX[1]->SetPoint(0, -100, 0, 0);
  fAxisX[1]->SetPoint(1, -95, 5, 0);

  fAxisX[2]->SetPoint(0, -100, 0, 0);
  fAxisX[2]->SetPoint(1, -95, -5, 0);

  fAxisX[3]->SetPoint(0, 100, 0, 0);
  fAxisX[3]->SetPoint(1, 95, 5, 0);

  fAxisX[4]->SetPoint(0, 100, 0, 0);
  fAxisX[4]->SetPoint(1, 95, -5, 0);

  //+X
  fAxisX[5]->SetPoint(0, 95, 10, 0);
  fAxisX[5]->SetPoint(1, 90, 10, 0);

  fAxisX[6]->SetPoint(0, 92.5, 7.5, 0);
  fAxisX[6]->SetPoint(1, 92.5, 12.5, 0);

  fAxisX[7]->SetPoint(0, 90, 6, 0);
  fAxisX[7]->SetPoint(1, 86, 14, 0);

  fAxisX[8]->SetPoint(0, 90, 14, 0);
  fAxisX[8]->SetPoint(1, 86, 6, 0);

  //-X
  fAxisX[9]->SetPoint(0, -85, 10, 0);
  fAxisX[9]->SetPoint(1, -90, 10, 0);

  fAxisX[10]->SetPoint(0, -95, 6, 0);
  fAxisX[10]->SetPoint(1, -91, 14, 0);

  fAxisX[11]->SetPoint(0, -95, 14, 0);
  fAxisX[11]->SetPoint(1, -91, 6, 0);

  // line Y
  fAxisY[0]->SetPoint(0, 0, -100, 0);
  fAxisY[0]->SetPoint(1, 0, 100, 0);

  // arrows Y
  fAxisY[1]->SetPoint(0, 0, -100, 0);
  fAxisY[1]->SetPoint(1, 0, -95, 5);

  fAxisY[2]->SetPoint(0, 0, -100, 0);
  fAxisY[2]->SetPoint(1, 0, -95, -5);

  fAxisY[3]->SetPoint(0, 0, 100, 0);
  fAxisY[3]->SetPoint(1, 0, 95, 5);

  fAxisY[4]->SetPoint(0, 0, 100, 0);
  fAxisY[4]->SetPoint(1, 0, 95, -5);

  //+Y
  fAxisY[5]->SetPoint(0, 0, 95, 10);
  fAxisY[5]->SetPoint(1, 0, 90, 10);

  fAxisY[6]->SetPoint(0, 0, 92.5, 7.5);
  fAxisY[6]->SetPoint(1, 0, 92.5, 12.5);

  fAxisY[7]->SetPoint(0, 0, 96.5, 12.5);
  fAxisY[7]->SetPoint(1, 0, 92.5, 14.5);

  fAxisY[8]->SetPoint(0, 0, 96.5, 16.5);
  fAxisY[8]->SetPoint(1, 0, 92.5, 14.5);

  fAxisY[9]->SetPoint(0, 0, 92.5, 14.5);
  fAxisY[9]->SetPoint(1, 0, 88.5, 14.5);

  //-Y
  fAxisY[10]->SetPoint(0, 0, -92.5, 7.5);
  fAxisY[10]->SetPoint(1, 0, -92.5, 12.5);

  fAxisY[11]->SetPoint(0, 0, -88.5, 12.5);
  fAxisY[11]->SetPoint(1, 0, -92.5, 14.5);

  fAxisY[12]->SetPoint(0, 0, -88.5, 16.5);
  fAxisY[12]->SetPoint(1, 0, -92.5, 14.5);

  fAxisY[13]->SetPoint(0, 0, -92.5, 14.5);
  fAxisY[13]->SetPoint(1, 0, -96.5, 14.5);

  // line Z
  fAxisZ[0]->SetPoint(0, 0, 0, -100);
  fAxisZ[0]->SetPoint(1, 0, 0, 100);

  // Z arrows
  fAxisZ[1]->SetPoint(0, 0, 0, -100);
  fAxisZ[1]->SetPoint(1, 8, 0, -92);

  fAxisZ[2]->SetPoint(0, 0, 0, -100);
  fAxisZ[2]->SetPoint(1, -8, 0, -92);

  fAxisZ[3]->SetPoint(0, 0, 0, 100);
  fAxisZ[3]->SetPoint(1, 8, 0, 92);

  fAxisZ[4]->SetPoint(0, 0, 0, 100);
  fAxisZ[4]->SetPoint(1, -8, 0, 92);

  //+Z
  fAxisZ[5]->SetPoint(0, 0, 10, 90);
  fAxisZ[5]->SetPoint(1, 0, 10, 85);

  fAxisZ[6]->SetPoint(0, 0, 7.5, 87.5);
  fAxisZ[6]->SetPoint(1, 0, 12.5, 87.5);

  fAxisZ[7]->SetPoint(0, 0, 14, 95);
  fAxisZ[7]->SetPoint(1, 0, 14, 90);

  fAxisZ[8]->SetPoint(0, 0, 14, 95);
  fAxisZ[8]->SetPoint(1, 0, 6, 90);

  fAxisZ[9]->SetPoint(0, 0, 6, 90);
  fAxisZ[9]->SetPoint(1, 0, 6, 95);

  //-Z
  fAxisZ[10]->SetPoint(0, 0, 10, -90);
  fAxisZ[10]->SetPoint(1, 0, 10, -95);

  fAxisZ[11]->SetPoint(0, 0, 14, -90);
  fAxisZ[11]->SetPoint(1, 0, 14, -85);

  fAxisZ[12]->SetPoint(0, 0, 14, -85);
  fAxisZ[12]->SetPoint(1, 0, 6, -90);

  fAxisZ[13]->SetPoint(0, 0, 6, -85);
  fAxisZ[13]->SetPoint(1, 0, 6, -90);
}

void TRenderableAxes::TearDown()
{
  UninitAxis(fAxisX);
  UninitAxis(fAxisY);
  UninitAxis(fAxisZ);
}

void TRenderableAxes::RegisterInEventManager()
{
  RegisterAxis(fManager, fParentEvent, fAxisX);
  RegisterAxis(fManager, fParentEvent, fAxisY);
  RegisterAxis(fManager, fParentEvent, fAxisZ);
}

void TRenderableAxes::UnregisterFromEventManager()
{
  UnregisterAxis(fManager, fParentEvent, fAxisX);
  UnregisterAxis(fManager, fParentEvent, fAxisY);
  UnregisterAxis(fManager, fParentEvent, fAxisZ);
}

TRenderableIP::TRenderableIP(gsl::not_null<TEveManager*> Manager,
                             gsl::not_null<TEveEventManager*> ParentEvent) noexcept
  : IRenderable(Manager, ParentEvent)
  //, fPointingLine3(new TEveLine(RenderElementToString(ERenderElements::kIPLine)))
{
  // The Pointingline is created only once, because it does
  // not change between different events.
//  fPointingLine3->IncDenyDestroy();
//  fPointingLine3->SetPoint(0, 0, 0, 0);
//  fPointingLine3->SetPoint(1, 0, 0, 0);
//  fPointingLine3->SetLineColor(kBlue);
//  fPointingLine3->SetLineWidth(10);
//  fPointingLine3->SetLineStyle(1);
//
//  Ensures(fPointingLine3 != nullptr);
}

TRenderableIP::~TRenderableIP() noexcept
{
  //fPointingLine3->DecDenyDestroy();
  RemoveIfNecessary();
}

void TRenderableIP::Setup(const TEveVSD& VSDData)
{
  Expects(VSDData.fTreeR != nullptr);
  const TGUIEnglish& Translation = TranslationFromEnv<TUtilityLanguage>();

  // Create the pointset with the wanted properties
  fIP = new TEvePointSet(RenderElementToString(ERenderElements::kIP));
  fIP->SetMainColor(kBlue);
  fIP->SetMarkerSize(2);
  fIP->SetMarkerStyle(2);
  fIP->IncDenyDestroy();
  fIP->SetTitle(Translation.PrimaryVertex());

  // Set the interaction point to the correct position given
  // by the VSDData.
  TEvePointSelector ss(VSDData.fTreeR, fIP, "fPVX:fPVY:fPVZ");
  auto SelectedRows = ss.Select();
  Expects(SelectedRows > 0);

  Ensures(fIP != nullptr);
}

void TRenderableIP::TearDown()
{
  Expects(fIP != nullptr);

  // Reference counting will ensure proper deletion (hopefully)
  fIP->DecDenyDestroy();
  fIP = nullptr;

  Ensures(fIP == nullptr);
}

void TRenderableIP::RegisterInEventManager()
{
  //fManager->AddElement(fPointingLine3, fParentEvent);
  fManager->AddElement(fIP, fParentEvent);
}

void TRenderableIP::UnregisterFromEventManager()
{
  //fManager->RemoveElement(fPointingLine3, fParentEvent);
  fManager->RemoveElement(fIP, fParentEvent);
}

namespace
{
void SetupTracks(gsl::not_null<TEveTrackList*> Track, const TString& Name, Color_t Color,
                 Style_t Style, Double_t MarkerSize)
{
  Track->SetName(Name);
  Track->SetMainColor(Color);
  Track->SetMarkerColor(kYellow);
  Track->SetMarkerStyle(Style);
  Track->SetMarkerSize(MarkerSize);
  Track->SetLineWidth(1);
}

void SetupPoints(gsl::not_null<TEvePointSet*> Points, const TString& Name, Color_t Color,
                 Style_t Style, Double_t MarkerSize)
{
  Points->SetName(Name);
  Points->SetMainColor(Color);
  Points->SetMarkerStyle(Style);
  Points->SetMarkerSize(MarkerSize);
}

gsl::not_null<TEvePointSet*> LoadClusters(const TEveVSD& VSDData, const TString& DetectorName,
                                          Int_t DetectorID)
{
  Expects(VSDData.fTreeC != nullptr);

  gsl::not_null<TEvePointSet*> ps = new TEvePointSet;
  ps->IncDenyDestroy();
  SetupPoints(ps, DetectorName, DetectorID + 2, 2, 0.5);

  TEvePointSelector ss(VSDData.fTreeC, ps, "fV.fX:fV.fY:fV.fZ",
                       TString::Format("fDetId==%d", DetectorID));
  ss.Select();
  ps->SetTitle(DetectorName + " Clusters");

  return ps;
}

} // namespace
void TRenderableClusters::Setup(const TEveVSD& VSDData)
{
  Expects(fITSClusters == nullptr);
  Expects(fTPCClusters == nullptr);
  Expects(fTRDClusters == nullptr);
  Expects(fTOFClusters == nullptr);

  fITSClusters = LoadClusters(VSDData, "ITS", 0);
  fTPCClusters = LoadClusters(VSDData, "TPC", 1);
  fTRDClusters = LoadClusters(VSDData, "TRD", 2);
  fTOFClusters = LoadClusters(VSDData, "TOF", 3);

  Ensures(fITSClusters != nullptr);
  Ensures(fTPCClusters != nullptr);
  Ensures(fTRDClusters != nullptr);
  Ensures(fTOFClusters != nullptr);
}

void TRenderableClusters::TearDown()
{
  Expects(fITSClusters != nullptr);
  Expects(fTPCClusters != nullptr);
  Expects(fTRDClusters != nullptr);
  Expects(fTOFClusters != nullptr);

  fITSClusters->DecDenyDestroy();
  fITSClusters = nullptr;
  fTPCClusters->DecDenyDestroy();
  fTPCClusters = nullptr;
  fTRDClusters->DecDenyDestroy();
  fTRDClusters = nullptr;
  fTOFClusters->DecDenyDestroy();
  fTOFClusters = nullptr;

  Ensures(fITSClusters == nullptr);
  Ensures(fTPCClusters == nullptr);
  Ensures(fTRDClusters == nullptr);
  Ensures(fTOFClusters == nullptr);
}

void TRenderableClusters::RegisterInEventManager()
{
  Expects(fITSClusters != nullptr);
  Expects(fTPCClusters != nullptr);
  Expects(fTRDClusters != nullptr);
  Expects(fTOFClusters != nullptr);
  fManager->AddElement(fITSClusters, fParentEvent);
  fManager->AddElement(fTPCClusters, fParentEvent);
  fManager->AddElement(fTRDClusters, fParentEvent);
  fManager->AddElement(fTOFClusters, fParentEvent);
}

void TRenderableClusters::UnregisterFromEventManager()
{
  Expects(fITSClusters != nullptr);
  Expects(fTPCClusters != nullptr);
  Expects(fTRDClusters != nullptr);
  Expects(fTOFClusters != nullptr);
  fManager->RemoveElement(fITSClusters, fParentEvent);
  fManager->RemoveElement(fTPCClusters, fParentEvent);
  fManager->RemoveElement(fTRDClusters, fParentEvent);
  fManager->RemoveElement(fTOFClusters, fParentEvent);
}

void TRenderableTracks::Setup()
{
  Expects(*fTracksLocation != nullptr);
  SetupTracks(*fTracksLocation, RenderElementToString(ERenderElements::kESDTracks), 40, 4, 0.5);
}

void TRenderableTracks::TearDown()
{
  Expects(*fTracksLocation != nullptr);
  //fParentEvent->DestroyElements();
  //(*fTracksLocation)->DestroyElements();
}


void TRenderableTracks::RegisterInEventManager()
{
  Expects(*fTracksLocation != nullptr);
  fManager->AddElement(*fTracksLocation, fParentEvent);
}

void TRenderableTracks::UnregisterFromEventManager()
{
  Expects(*fTracksLocation != nullptr);
  fManager->RemoveElement(*fTracksLocation, fParentEvent);
}

void TRenderableV0s::Setup()
{
  Expects(*fV0NegLocation != nullptr);
  Expects(*fV0PosLocation != nullptr);
  Expects(*fV0sLocation != nullptr);

  SetupTracks(*fV0NegLocation, RenderElementToString(ERenderElements::kV0NegativeTracks),
              kGreen - 3, 10, 5);
  SetupTracks(*fV0PosLocation, RenderElementToString(ERenderElements::kV0PositiveTracks), 2, 10, 5);
  SetupPoints(*fV0sLocation, RenderElementToString(ERenderElements::kV0s), 14, 2, 0.5);
}

void TRenderableV0s::RegisterInEventManager()
{
  Expects(*fV0NegLocation != nullptr);
  Expects(*fV0PosLocation != nullptr);
  Expects(*fV0sLocation != nullptr);

  fManager->AddElement(*fV0NegLocation, fParentEvent);
  fManager->AddElement(*fV0PosLocation, fParentEvent);
  fManager->AddElement(*fV0sLocation, fParentEvent);
}
void TRenderableV0s::UnregisterFromEventManager()
{
  Expects(*fV0NegLocation != nullptr);
  Expects(*fV0PosLocation != nullptr);
  Expects(*fV0sLocation != nullptr);

  fManager->RemoveElement(*fV0NegLocation, fParentEvent);
  fManager->RemoveElement(*fV0PosLocation, fParentEvent);
  fManager->RemoveElement(*fV0sLocation, fParentEvent);
}

void TRenderableCascades::Setup()
{
  Expects(*fCascadePosLocation != nullptr);
  Expects(*fCascadeNegLocation != nullptr);
  Expects(*fCascadeBachelorLocation != nullptr);
  Expects(*fCascadeV0sLocation != nullptr);

  SetupTracks(*fCascadeBachelorLocation,
              RenderElementToString(ERenderElements::kCascadeBachelorTracks), kBlue, 4, .5);
  SetupTracks(*fCascadePosLocation, RenderElementToString(ERenderElements::kCascadePositiveTracks),
              2, 10, 5);
  SetupTracks(*fCascadeNegLocation, RenderElementToString(ERenderElements::kCascadeNegativeTracks),
              kGreen - 3, 10, 5);
  SetupPoints(*fCascadeV0sLocation, RenderElementToString(ERenderElements::kCascadeV0s), 1, 2, 2);
}

void TRenderableCascades::RegisterInEventManager()
{
  Expects(*fCascadePosLocation != nullptr);
  Expects(*fCascadeNegLocation != nullptr);
  Expects(*fCascadeBachelorLocation != nullptr);
  Expects(*fCascadeV0sLocation != nullptr);

  fManager->AddElement(*fCascadePosLocation, fParentEvent);
  fManager->AddElement(*fCascadeNegLocation, fParentEvent);
  fManager->AddElement(*fCascadeBachelorLocation, fParentEvent);
  fManager->AddElement(*fCascadeV0sLocation, fParentEvent);
}
void TRenderableCascades::UnregisterFromEventManager()
{
  Expects(*fCascadePosLocation != nullptr);
  Expects(*fCascadeNegLocation != nullptr);
  Expects(*fCascadeBachelorLocation != nullptr);
  Expects(*fCascadeV0sLocation != nullptr);

  fManager->RemoveElement(*fCascadePosLocation, fParentEvent);
  fManager->RemoveElement(*fCascadeNegLocation, fParentEvent);
  fManager->RemoveElement(*fCascadeBachelorLocation, fParentEvent);
  fManager->RemoveElement(*fCascadeV0sLocation, fParentEvent);
}
} // namespace Utility
