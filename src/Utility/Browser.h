/**
 * @file   Browser.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar 16 23:09:27 2017
 *
 * @brief  Base class for ALICE browsers
 * @ingroup  alice_masterclass_base
 */
#ifndef ALICEBROWSER_C
#define ALICEBROWSER_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TRootBrowser.h>
#include <TString.h>

class TBuffer;
class TClass;
class TMemberInspector;

namespace Utility
{
class Exercise;
struct TGUIEnglish;
} // namespace Utility

namespace Utility
{
/**
 * A base class for ALICE browsers.
 *
 * @ingroup  alice_masterclass_base
 */
struct Browser : public TRootBrowser {
  Exercise* fExercise;
  const TGUIEnglish& fTranslation;

  Browser(const TString& title);
  ~Browser() override = default;

  void Setup(Exercise* exercise, Bool_t cheat = false);

  void Auto();

  ClassDefOverride(Browser, 0);
};
} // namespace Utility

#endif
