#ifndef HELPERPRIMARYTRACKS_H_GLHYCOW4
#define HELPERPRIMARYTRACKS_H_GLHYCOW4

#include <TEveTrack.h>

namespace Utility
{
inline Bool_t IsPrimary(const TEveTrack* T) { return T->TestBit(1 << 14); }
inline void MarkPrimary(TEveTrack* T) { T->SetBit(1 << 14); }
inline void ClearPrimary(TEveTrack* T) { T->ResetBit(1 << 14); }

inline Bool_t IsSelected(const TEveTrack* T) { return T->TestBit(1 << 15); }
inline void MarkSelected(TEveTrack* T, Color_t Color = kRed)
{
  T->SetBit(1 << 15);
  T->SetLineColor(Color);
}
inline void ClearSelected(TEveTrack* T) { T->ResetBit(1 << 15); }
} // namespace Utility

#endif /* end of include guard: HELPERPRIMARYTRACKS_H_GLHYCOW4 */
