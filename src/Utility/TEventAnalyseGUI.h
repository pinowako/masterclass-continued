#ifndef TEVENTANALYSEGUI_H_ZTCSRADF
#define TEVENTANALYSEGUI_H_ZTCSRADF

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TEveManager.h>
#include <TEveViewer.h>
#include <TString.h>
#include <gsl/gsl>
#include <memory>
#include <TRootHelpDialog.h>

#include "Raa/part1/EActiveTask.h"
#include "Utility/EventDisplay.h"
#include "Utility/GUITranslation.h"
#include "Utility/INavigation.h"
#include "Utility/TEventVisualization.h"
#include "Utility/VSDReader.h"

class TBuffer;
class TClass;
class TMemberInspector;

namespace Utility
{
struct TGUIEnglish;

/// Defines the state of the Raa Analysis, defining which tracks are interesting
class TEventAnalyseGUI
{
  // FIXME Prevent double analysis
 public:
  /// Construct the Eve-Browser and set up the GUI context.
  explicit TEventAnalyseGUI(const TString& geomFileName);
  virtual ~TEventAnalyseGUI() = default;

  /// @{
  /// @name Finalize the Setup and Connection of all Components

  /// Setup GUI and everything, EXTRACTED from VSDReader
  void Setup(std::unique_ptr<INavigation> Navigation, std::unique_ptr<EventDisplay> exercise,
             const TString& title, const TString& dataFileName, Bool_t cheat);
  /// Connects all events of of the internals of the GUI
  void SetupSignalSlots();
  /// @}

  /// @{
  /// @name Event Handling for Event Switching

  /// Slot that chooses the first Element and updates everything.
  void ChooseFirstEvent();
  /// Slot that chooses the previous Element and updates everything.
  void ChoosePreviousEvent();
  /// Slot that chooses the next Element and updates everything.
  void ChooseNextEvent();
  /// Slot that reloads current Element and updates everything.
  void ChooseCurrentEvent();
  /// Slot that is triggered if one event analysis is finished.
  void FinishCurrentEvent();

  void FastForward(Int_t eventsNumber);
  /// Perform automatic analysis. EXTRACTED from VSDReader
  void Auto();
  /// @}

  /// @{
  /// @name General purpose slots

  /// Slot that opens the Instructions for the current MasterClass.
  void OpenInstructions();
  /// @}

  /// @{
  /// @name Slots for controlling the presentation of the Data

  /// Toggle the presentation of exactly one Renderable. Ensures data load and change
  /// in visualization.
  void ToggleRenderable(ERenderables ElementID);

  /// Change background. EXTRACTED from VSDReader
  void ChangeBackgroundColor() { fEveManager->GetViewers()->SwitchColorSet(); }
  /// @}

  /// @{
  /// @name Slots that do not fit somewhere else.

  /// Detector = (ITS|TPC|TRD+TOF)
  void ShowDetectorDetails(const char* Detector);

  /// @}

  /// @{
  /// @name Slots to control the current Raa-Task
  /// FIXME Not nice, should be in a subclass of TEventAnalyseGUI specific
  /// to Raa. For now will be here and slightly unclean. It is only a
  /// dispatch though, so the logic lives in Raa-specific code!
  void DispatchTaskChoice(Int_t NTask);
  /// @}

 private:
  /// Utility helper function to load and display a new event.
  Bool_t SwitchToEvent(Int_t EventIdx);

  /// Check if an Element is set as active.
  Bool_t IsActive(ERenderables Element);

  /// Whenever the visualization state changes some data loading might have to
  /// happen. Do everything necessary to have a consistent state with all
  /// components.
  void ConfigureState();

  /// Helper to visualize the current configured state (e.g. no Tracks but Clusters
  /// are activated, ensure that tracks are not shown but clusters are)
  void VisualizeState();

  gsl::not_null<TEveManager*> fEveManager;
  gsl::not_null<TEveBrowser*> fEveBrowser;
  gsl::not_null<TEveEventManager*> fEveEventManager;

  // MasterClass data and logic
  std::unique_ptr<VSDReader> fVSDData; ///< Injected Reader object
  std::unique_ptr<EventDisplay> fExercise; ///< Injected MasterClass object

  ERenderables fActiveRenderables;

  // GUI Elements
  std::unique_ptr<INavigation> fNavigation;
  TEventVisualization fEventVisualization;
  const TGUIEnglish& fTranslation;

  // State of the MasterClass
  Int_t fCurrentEventIdx;
  Int_t fTotalEventCount;
  Int_t fFinishedEvents; ///< Number of analysed events.
  Bool_t fUpdateGraphics;

  ClassDef(TEventAnalyseGUI, 0);
};

} // namespace Utility

#endif /* end of include guard: TEVENTANALYSEGUI_H_ZTCSRADF */
