/**
 * @file   Instructions.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 09:02:35 2017
 *
 * @brief  A widget to show instructions
 *
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 *
 */
#ifndef ALICEINSTRUCTIONS_C
#define ALICEINSTRUCTIONS_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>
#include <TString.h>

#include "Utility/GUITranslation.h"
#include "Utility/Utilities.h"

class TBuffer;
class TClass;
class TGTextButton;
class TGWindow;
class TMemberInspector;

namespace Utility
{
struct TGUIEnglish;

TGTextButton* MakeInstructionsButton(TGCompositeFrame* p);

//====================================================================
/**
 * A widget to show an HTML page
 *
 * @throws std::runtime_error if the file 'file' can not be read with 'ReadHtml'.
 * @ingroup alice_masterclass_base_eventdisplay
 */
class Instructions : public TGMainFrame
{
 private:
  const TGUIEnglish& fTranslation;

 public:
  Instructions(const TGWindow* p, Int_t w, Int_t h, const TString& Content, Bool_t exit = true);

  ClassDef(Instructions, 0);
};
} // namespace Utility

#endif
