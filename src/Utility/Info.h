/**
 * @file   Info.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 09:05:46 2017
 *
 * @brief  A widget to show information about ALICE and sub-detectors
 *
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 *
 */
#ifndef ALICEINFO_C
#define ALICEINFO_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TString.h>
#include <vector>

#include "Utility/Details.h"
#include "Utility/GUITranslation.h"
#include "Utility/Utilities.h"

class TBuffer;
class TClass;
class TGPicture;
class TGPicture;
class TGPictureButton;
class TGPictureButton;
class TGWindow;
class TMemberInspector;

namespace Utility
{
struct TGUIEnglish;
} // namespace Utility

namespace Utility
{
//====================================================================
/**
 * Show information about ALICE
 *
 * @ingroup alice_masterclass_base_eventdisplay
 */
class InfoBox : public TGMainFrame
{
 private:
  TString fTitle;
  const TGUIEnglish& fTranslation;
  std::vector<const TGPicture*> fPictures;

 public:
  /**
   * Constructor
   *
   * @param title  The title to show
   * @param names  Null terminated list of names to show
   * @param p      Parent
   * @param w      Width
   * @param h      Height
   */
  InfoBox(const TString& title, const std::vector<TString>& names, const TGWindow* p, UInt_t w,
          UInt_t h);

  virtual ~InfoBox();

  /**
   * Show more information about the selected detector
   */
  void MakeBigger(const char* pic) { fClient->WaitFor(new Details(pic, "Utility", fTitle, gClient->GetRoot(), 100, 100)); }

  ClassDef(InfoBox, 0);
};
} // namespace Utility

#endif
