#ifndef GUITRANSLATION_H_2TF7SD8C
#define GUITRANSLATION_H_2TF7SD8C

#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep
#include "Utility/Utilities.h" // IWYU pragma: keep

namespace Utility
{
struct TGUIEnglish : TLanguageProvider {
  TGUIEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TLanguageProvider(lang)
  {
    std::cerr << "Registering English GUI for Utility\n";
#include "Utility/keys_gui_trans.txt.en" // IWYU pragma: keep
  };

  LANG_KEY(AutoAnalyseTitle)
  LANG_KEY(AutoAnalyseText)
  LANG_KEY(ButtonAxes)
  LANG_KEY(ButtonBackground)
  LANG_KEY(ButtonClose)
  LANG_KEY(ButtonCurrent)
  LANG_KEY(ButtonDecayPatterns)
  LANG_KEY(ButtonEventAnalyzed)
  LANG_KEY(ButtonExit)
  LANG_KEY(ButtonExport)
  LANG_KEY(ButtonGeometry)
  LANG_KEY(ButtonNext)
  LANG_KEY(ButtonPrevious)
  LANG_KEY(ButtonPrint)
  LANG_KEY(ButtonShowPrimary)
  LANG_KEY(Clusters)
  LANG_KEY(Display)
  LANG_KEY(Encyclopaedia)
  LANG_KEY(ErrorInvalidDirectory)
  LANG_KEY(ErrorInvalidDirectoryMsg)
  LANG_KEY(EventDone)
  LANG_KEY(EventNavigation)
  LANG_KEY(GuardAutoMode)
  LANG_KEY(GuardDestroyingGeometry)
  LANG_KEY(GuardLoadingAxes)
  LANG_KEY(GuardLoadingCascades)
  LANG_KEY(GuardLoadingClusters)
  LANG_KEY(GuardLoadingIP)
  LANG_KEY(GuardLoadingEvent)
  LANG_KEY(GuardLoadingGeometry)
  LANG_KEY(GuardLoadingV0)
  LANG_KEY(GuardSettingUp)
  LANG_KEY(Instructions)
  LANG_KEY(IP)
  LANG_KEY(MultiView)
  LANG_KEY(Navigation)
  LANG_KEY(PrimaryVertex)
  LANG_KEY(QuestionAutomaticAnalysis)
  LANG_KEY(QuestionAutomaticAnalysisMsg)
  LANG_KEY(SceneRPhiGeom)
  LANG_KEY(SceneRPhiDesc)
  LANG_KEY(SceneRhoZGeom)
  LANG_KEY(SceneRhoZDesc)
  LANG_KEY(SceneRPhiEventTitle)
  LANG_KEY(SceneRPhiEventDesc)
  LANG_KEY(SceneRhoZEventTitle)
  LANG_KEY(SceneRhoZEventDesc)
  LANG_KEY(Task1)
  LANG_KEY(Task2)
  LANG_KEY(Task3)
  LANG_KEY(Task4)
  LANG_KEY(Tasks)
  LANG_KEY(TipPrint)
  LANG_KEY(TipEventAnalyzed)
  LANG_KEY(TipExit)
  LANG_KEY(TipExport)
  LANG_KEY(TipNextEvent)
  LANG_KEY(TipPreviousEvent)
  LANG_KEY(TipShowInformation)
  LANG_KEY(TipShowDecays)
  LANG_KEY(TipToggleAxes)
  LANG_KEY(TipToggleBackground)
  LANG_KEY(TipToggleCascades)
  LANG_KEY(TipToggleClusters)
  LANG_KEY(TipToggleDetectorVolumes)
  LANG_KEY(TipTogglePrimary)
  LANG_KEY(TipTogglePrimaryOnly)
  LANG_KEY(TipToggleTracks)
  LANG_KEY(TipToggleV0)
  LANG_KEY(Tracks)
  LANG_KEY(View3D)
  LANG_KEY(ViewRPhi)
  LANG_KEY(ViewRhoZ)

  LANG_KEY(DataSet)
  LANG_KEY(SelectDataSet)
  LANG_KEY(DemoSet)
  LANG_KEY(Start)
  LANG_KEY(ParticleInfo)
};

LANG_TRANSLATION(TGUI, German)
{
  std::cerr << "Registering German GUI for Utility\n";
#include "Utility/keys_gui_trans.txt.de" // IWYU pragma: keep
}

struct TUtilityLanguage {
  using ReturnType = TGUIEnglish;
  static std::unique_ptr<TGUIEnglish> create(ESupportedLanguages lang)
  {
    switch (lang) {
      case English:
        return std_fix::make_unique<TGUIEnglish>();
      case German:
        return std_fix::make_unique<TGUIGerman>();
      case NLanguages:
        break;
    }
    throw std::runtime_error("Unsupported language for Utility requested!");
  }
};
} // namespace Utility

#endif /* end of include guard: GUITRANSLATION_H_2TF7SD8C */
