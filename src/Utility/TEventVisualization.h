/**
 * @file   Utility/TEventVisualization.h
 * @author Matevz Tadel
 * @date   2009
 *
 * @brief Multi-view (3d, rphi, rhoz) service class using EVE Window
 * Manager.
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */

#ifndef MULTIVIEW_C
#define MULTIVIEW_C

#include <RQ_OBJECT.h>
#include <RtypesCore.h>
#include <TEveBrowser.h>
#include <TEveElement.h>
#include <TEveGeoShape.h>
#include <TEveManager.h>
#include <TEveProjectionAxes.h>
#include <TEveProjectionManager.h>
#include <TEveScene.h>
#include <TEveViewer.h>
#include <TGLViewer.h>
#include <TString.h>
#include <gsl/gsl>

#include "Utility/GUITranslation.h"
#include "Utility/Renderables.h"

class TEveElement;
class TEveTrack;
class TEveGeoShape;
class TEveViewer;

namespace Utility
{
struct TGUIEnglish;
class VSDReader;

/**
 * TEventVisualization
 *
 * Structure encapsulating standard views: 3D, @f$(r,\phi)@f$ and
 * @f$(\rho,z)@f$.  Includes scenes and projection managers.
 *
 * Should be used in compiled mode.
 *
 * @ingroup  alice_masterclass_base_eventdisplay
 */
class TEventVisualization : public TQObject
{
  RQ_OBJECT("Utility::TEventVisualization")

 public:
  /// Creates required scenes, projection managers and GL viewers.
  TEventVisualization(gsl::not_null<TEveManager*> EveManager,
                      gsl::not_null<TEveEventManager*> EventManager);

  void SetupVSD(gsl::not_null<TEveVSD*> VSDDataRef);

  /// @{
  /// @name Detector geometry functionality

  /// Return handle to the Detector geometry to allow exercise specific fixing.
  TEveGeoShape* GetDetectorGeometry() { return fGeomGentle; }

  /// Load the detector geometry from the file 'FileName'.
  void LoadDetectorGeometry(const TString& FileName);

  /// Import @f$(r,\phi)@f$ event
  void ImportEventRPhi(TEveElement* el) { fRPhiMgr->ImportElements(el, fRPhiEventScene); }

  /// Import @f$(\rho,z)@f$ event
  void ImportEventRhoZ(TEveElement* el) { fRhoZMgr->ImportElements(el, fRhoZEventScene); }

  /// Remove @f$(r,\phi)@f$ event
  void DestroyEventRPhi() { fRPhiEventScene->DestroyElements(); }

  /// Remove @f$(\rho,z)@f$ event.
  void DestroyEventRhoZ() { fRhoZEventScene->DestroyElements(); }
  /// @}

  /// @{
  /// @name Control visualization of the Renderables.

  void HideAll();

  void ShowDetector();
  void HideDetector();

  void ShowAxes() { ShowRenderable(kAxesPos); }
  void HideAxes() { HideRenderable(kAxesPos); }

  void ShowIntersectionPoint() { ShowRenderable(kIPPos, true); }
  void HideIntersectionPoint() { HideRenderable(kIPPos); }

  void ShowClusters() { ShowRenderable(kClustersPos, true); }
  void HideClusters() { HideRenderable(kClustersPos); }

  void ShowTracks() { ShowRenderable(kTracksPos); }
  void HideTracks() { HideRenderable(kTracksPos); }

  void ShowV0s() { ShowRenderable(kV0Pos); }
  void HideV0s() { HideRenderable(kV0Pos); }

  void ShowCascades() { ShowRenderable(kCascadesPos); }
  void HideCascades() { HideRenderable(kCascadesPos); }
  /// @}

  /// @{
  /// @name Selection Handling

  /// Dispatches an element-selection to the appropriate signal.
  void DispatchMouseSelection(TEveElement* el);

  void SelectedClustersITS();    //*SIGNAL*
  void SelectedClustersTPC();    //*SIGNAL*
  void SelectedClustersTRDTOF(); //*SIGNAL*

  void SelectedESDTrack(TEveTrack* Track); //*SIGNAL*

  void SelectedV0Track(TEveTrack* Track);        //*SIGNAL*
  void SelectedV0PointingLine(TEveTrack* Track); //*SIGNAL*

  void SelectedCascadeTrack(TEveTrack* Track); //*SIGNAL*

  /// @}

 private:
  /// Set current depth on all projection managers.
  void SetDepth(Float_t d);

  /// Zoom at a specific point \p v in the Side-views.
  void ZoomAt(TEveVector v);

  /// @{
  /// @name Manage the display of the Sideviews, interface for VSDReader.

  /// Import \c el into the @f$(r,\phi)@f$ projection
  void ImportGeomRPhi(TEveElement* el) { fRPhiMgr->ImportElements(el, fRPhiGeomScene); }

  /// Import \c el into the @f$(\rho,z)@f$ projection
  void ImportGeomRhoZ(TEveElement* el) { fRhoZMgr->ImportElements(el, fRhoZGeomScene); }

  /// Remove @f$(r,\phi)@f$ projection
  void DestroyGeomRPhi() { fRPhiGeomScene->DestroyElements(); }

  /// Remove @f$(\rho,z)@f$ projection
  void DestroyGeomRhoZ() { fRhoZGeomScene->DestroyElements(); }

  /// Configure the default view for the Detector geometry.
  void SetupDefaultDetectorGeometry();

  /// Destroys and frees the Detector Geometry.
  void DestroyDetectorGeometry();

  /// Make axes for @f$(r,\phi)@f$ projection
  void SetRPhiAxes();

  /// Make axes for @f$(\rho,z)@f$ projection
  void SetRhoZAxes();
  /// @}

  gsl::not_null<TEveManager*> fEveManager;
  gsl::not_null<TEveEventManager*> fParentEvent;
  gsl::not_null<TEveProjectionManager*> fRPhiMgr;
  gsl::not_null<TEveProjectionManager*> fRhoZMgr;
  TEveViewer* f3DView;
  TEveViewer* fRPhiView;
  TEveViewer* fRhoZView;
  TEveScene* fRPhiGeomScene;
  TEveScene* fRhoZGeomScene;
  TEveScene* fRPhiEventScene;
  TEveScene* fRhoZEventScene;

  Bool_t fDetectorExists{ false };
  TEveGeoShape* fGeomGentle; // Detector geometry with low resolution.

  /// Reference to the loaded data, updated on event switch
  const TEveVSD* fVSDReference{ nullptr };

  /// These constants define the position of the element in the array of
  /// Renderables.
  enum ERenderablePositions {
    kAxesPos = 0,
    kIPPos,
    kClustersPos,
    kTracksPos,
    kV0Pos,
    kCascadesPos,
    kNumberRenderables
  };
  /// Internal implementation for showing elements.
  void ShowRenderable(ERenderablePositions Idx, Bool_t WithVSD = false);
  /// Internal implemenation for hiding an element again
  void HideRenderable(ERenderablePositions Idx);

  /// All renderable elements, Axis, IntersectionPoint, Clusters, Tracks, V0s, Cascades
  std::array<std::unique_ptr<IRenderable>, kNumberRenderables> fRenderables;

  // This friend is used to setup data location from VSDReader and here.
  // Easiest and fastest way to break the strong coupling but still get
  // the necessary access.
  friend void ConnectDataAndRenderables(VSDReader* Data, TEventVisualization& Vis);
};

/// This function will connect the data to the visualization and accesses
/// internal state of both for it. The locations where the data is stored
/// are connected to the elements that shall visualize them.
void ConnectDataAndRenderables(VSDReader* Data, TEventVisualization& Vis);
} // namespace Utility

#endif
