#ifndef RENDERABLES_H_TWBPOYND
#define RENDERABLES_H_TWBPOYND

#include <Rtypes.h>
#include <RtypesCore.h>
#include <array>
#include <gsl/gsl>
#include <stdexcept>

#include "Utility/ERenderables.h"

class TEveEventManager;
class TEveLine;
class TEveManager;
class TEvePointSet;
class TEveTrackList;
class TEveVSD;

namespace Utility
{
/// Class defining the interface how a thing (e.g. a list of Tracks) shall
/// work together with the event display.
/// The interface is there to have a uniform of showing, or removing the thing
/// from the rendered elements of the display.
/// Note, that it is necessary to keep the pointers to the parent and
/// underlying data alive, that are used to setup and tear down the Renderable.
class IRenderable
{
 private:
  Bool_t fIsDisplayed = false;

 protected:
  gsl::not_null<TEveManager*> fManager;
  gsl::not_null<TEveEventManager*> fParentEvent;

  /// Prepare the internal data that shall be displayed.
  virtual void Setup(const TEveVSD& /*VSDData*/) { throw std::runtime_error("Not Implemented!"); }
  virtual void Setup() { throw std::runtime_error("Not Implemented!"); }

  /// Undo operations from `Setup` if necessary.
  virtual void TearDown() = 0;

  /// Add the internal state to the Scene managed by TEveEventManager.
  virtual void RegisterInEventManager() = 0;
  /// Remove all elements from the Scene managed by TEveEventManager.
  virtual void UnregisterFromEventManager() = 0;

 public:
  IRenderable(gsl::not_null<TEveManager*> Manager,
              gsl::not_null<TEveEventManager*> ParentEvent) noexcept
    : fManager(Manager)
    , fParentEvent(ParentEvent)
  {
  }
  virtual ~IRenderable() noexcept = default;

  IRenderable(const IRenderable&) = delete;
  IRenderable& operator=(const IRenderable&) = delete;

  IRenderable(IRenderable&&) = default;
  IRenderable& operator=(IRenderable&&) = default;

  /// Add the Renderable to an TEveEventManager that is rendered.
  void Display(const TEveVSD& VSDData);

  /// Add the Renderable to the view, without prior calculations.
  void Display();

  /// Remove the Renderable from an TEveEventManager that renders it.
  /// Expects, that `Display()` has been called with the same ParentEvent!
  void Remove();

  /// Removes the Renderable from an TEveEventManager that renders it, if it
  /// is on display.
  void RemoveIfNecessary();

  /// Query if the Element is currently on Display or not.
  Bool_t IsDisplayed() const { return fIsDisplayed; }
};

/// Display the XYZ-Axes in the scene.
class TRenderableAxes : public IRenderable
{
 private:
  std::array<TEveLine*, 12> fAxisX{ nullptr };
  std::array<TEveLine*, 14> fAxisY{ nullptr };
  std::array<TEveLine*, 14> fAxisZ{ nullptr };

 protected:
  void Setup() override;
  void TearDown() override;

  void RegisterInEventManager() override;
  void UnregisterFromEventManager() override;

 public:
  TRenderableAxes(gsl::not_null<TEveManager*> Manager, gsl::not_null<TEveEventManager*> ParentEvent)
    : IRenderable(Manager, ParentEvent)
  {
  }
  ~TRenderableAxes() noexcept override { RemoveIfNecessary(); }
};

/// Display the intersection point (Colission point) of the beam.
class TRenderableIP : public IRenderable
{
 private:
  TEvePointSet* fIP;
  //TEveLine* fPointingLine3;

 protected:
  void Setup(const TEveVSD& VSDData) override;
  void TearDown() override;

  void RegisterInEventManager() override;
  void UnregisterFromEventManager() override;

 public:
  TRenderableIP(gsl::not_null<TEveManager*> Manager,
                gsl::not_null<TEveEventManager*> ParentEvent) noexcept;
  ~TRenderableIP() noexcept override;
};

/// Display the Clusters of the detectors in ALICE.
class TRenderableClusters : public IRenderable
{
 private:
  TEvePointSet* fITSClusters{ nullptr };
  TEvePointSet* fTPCClusters{ nullptr };
  TEvePointSet* fTRDClusters{ nullptr };
  TEvePointSet* fTOFClusters{ nullptr };

 protected:
  void Setup(const TEveVSD& VSDData) override;
  void TearDown() override;

  void RegisterInEventManager() override;
  void UnregisterFromEventManager() override;

 public:
  TRenderableClusters(gsl::not_null<TEveManager*> Manager,
                      gsl::not_null<TEveEventManager*> ParentEvent)
    : IRenderable(Manager, ParentEvent)
  {
  }
  ~TRenderableClusters() noexcept override { RemoveIfNecessary(); }
};

/// Display the normal tracks of the collision.
class TRenderableTracks : public IRenderable
{
 private:
  gsl::not_null<TEveTrackList**> fTracksLocation;

 protected:
  void Setup() override;
  void TearDown() override;

  void RegisterInEventManager() override;
  void UnregisterFromEventManager() override;

 public:
  /// Stores all information necessary to display the
  /// Tracks. \c TracksLocation is the place, where the pointer
  /// to the tracklist (loaded from VSDReader) is stored.
  TRenderableTracks(gsl::not_null<TEveManager*> Manager,
                    gsl::not_null<TEveEventManager*> ParentEvent,
                    gsl::not_null<TEveTrackList**> TracksLocation)
    : IRenderable(Manager, ParentEvent)
    , fTracksLocation(TracksLocation)
  {
  }
  ~TRenderableTracks() noexcept override { RemoveIfNecessary(); }
};

class TRenderableV0s : public IRenderable
{
 private:
  gsl::not_null<TEveTrackList**> fV0NegLocation;
  gsl::not_null<TEveTrackList**> fV0PosLocation;
  gsl::not_null<TEvePointSet**> fV0sLocation;

 protected:
  void Setup() override;
  void TearDown() override {}

  void RegisterInEventManager() override;
  void UnregisterFromEventManager() override;

 public:
  /// Stores all information necessary to display the
  /// Tracks. \c TracksLocation is the place, where the pointer
  /// to the tracklist (loaded from VSDReader) is stored.
  TRenderableV0s(gsl::not_null<TEveManager*> Manager, gsl::not_null<TEveEventManager*> ParentEvent,
                 gsl::not_null<TEveTrackList**> V0NegLocation,
                 gsl::not_null<TEveTrackList**> V0PosLocation,
                 gsl::not_null<TEvePointSet**> V0sLocation)
    : IRenderable(Manager, ParentEvent)
    , fV0NegLocation(V0NegLocation)
    , fV0PosLocation(V0PosLocation)
    , fV0sLocation(V0sLocation)
  {
  }
  ~TRenderableV0s() noexcept override { RemoveIfNecessary(); }
};

class TRenderableCascades : public IRenderable
{
 private:
  gsl::not_null<TEveTrackList**> fCascadeNegLocation;
  gsl::not_null<TEveTrackList**> fCascadePosLocation;
  gsl::not_null<TEveTrackList**> fCascadeBachelorLocation;
  gsl::not_null<TEvePointSet**> fCascadeV0sLocation;

 protected:
  void Setup() override;
  void TearDown() override {}

  void RegisterInEventManager() override;
  void UnregisterFromEventManager() override;

 public:
  TRenderableCascades(gsl::not_null<TEveManager*> Manager,
                      gsl::not_null<TEveEventManager*> ParentEvent,
                      gsl::not_null<TEveTrackList**> CascadeNegLocation,
                      gsl::not_null<TEveTrackList**> CascadePosLocation,
                      gsl::not_null<TEveTrackList**> CascadeBachelorLocation,
                      gsl::not_null<TEvePointSet**> CascadeV0sLocation)
    : IRenderable(Manager, ParentEvent)
    , fCascadeNegLocation(CascadeNegLocation)
    , fCascadePosLocation(CascadePosLocation)
    , fCascadeBachelorLocation(CascadeBachelorLocation)
    , fCascadeV0sLocation(CascadeV0sLocation)
  {
  }
  ~TRenderableCascades() noexcept override { RemoveIfNecessary(); }
};

} // namespace Utility

#endif /* end of include guard: RENDERABLES_H_TWBPOYND */
