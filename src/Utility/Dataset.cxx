#include "Dataset.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TEveManager.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TString.h>
#include <memory>
#include <utility>

#include "Utility/EventDisplay.h"
#include "Utility/INavigation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"
#include "Utility/TEventAnalyseGUI.h"
#include "Utility/Utilities.h"

class TGPicture;

namespace Utility
{
    Dataset::Dataset(Bool_t allowAuto, Bool_t hasDemoSet)
            : TGMainFrame(gClient->GetRoot(), 0, 0)
            , fAllowAuto(allowAuto)
            , fHasDemoSet(hasDemoSet)
            , fTranslation(Utility::TranslationFromEnv<TUtilityLanguage>())
            , fSelectionCombo(nullptr)
    {
      auto* lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2);
      const TGPicture* al = Utility::Logo();
      AddFrame(new TGPictureButton(this, al), lh);

      fInstructions = new TGHtml(this, 450, 400);

      AddFrame(fInstructions, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 2));

      auto* gframe = new TGGroupFrame(this, fTranslation.SelectDataSet());

      fSelectionCombo= new TGComboBox(gframe);
      fSelectionCombo->AddEntry(fTranslation.SelectDataSet(), 0);

      fSelectionCombo->Resize(100, 20);
      fSelectionCombo->Select(0, kFALSE);
      fSelectionCombo->Connect("Selected(Int_t)", "Utility::Dataset", this, "Choice(Int_t)");
      gframe->AddFrame(fSelectionCombo, lh);
      AddFrame(gframe, lh);

      fStart = new TGTextButton(this, fTranslation.Start());
      fStart->SetEnabled(false);
      AddFrame(fStart, new TGLayoutHints(kLHintsExpandX, 3, 3, 3, 4));
      fStart->Connect("Clicked()", "Utility::Dataset", this, "Start()");

      // Instead of closing the window, terminate the whole app
      DontCallClose();
      Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");

      SetWindowName("MasterClass");
      MapSubwindows();

      Resize(GetDefaultSize());

      MapWindow();
    }

    Dataset::~Dataset() { Cleanup(); }

    void Dataset::setInstructions(const TString& instructionsHTML)
    {
      if (!instructionsHTML.IsNull()) {
        fInstructions->ParseText(const_cast<char*>(instructionsHTML.Data()));
      }
    }

    void Dataset::fillSelectionBox()
    {

    }

    void Dataset::Choice(Int_t id)
    {
      fDataset = id - 1;
      fStart->SetEnabled(fDataset != -1);
    }

    void Dataset::Init()
    {
      fillSelectionBox();
    }

    void StandardDataset::fillSelectionBox()
    {
      if(fHasDemoSet)
        fSelectionCombo->AddEntry(fTranslation.DemoSet(), 1);

      for (Int_t i = 1; i <= fDatasetCount; i++) {
        fSelectionCombo->AddEntry(Form("%s %2d", fTranslation.DataSet().Data(), i), i + 1);
      }
    }

    void StandardDataset::setMainWindowTitle(const TString& windowTitle)
    {
      fMainWindowTitle = windowTitle;
    }

    void StandardDataset::Code(const TString& data, const TString& geom, Bool_t cheat, Bool_t demo)
    {
      auto* r = new Utility::TEventAnalyseGUI(geom);
      std::unique_ptr<Utility::EventDisplay> Exercise = createEventDisplay(demo);
      std::unique_ptr<Utility::INavigation> Navigation = createNavigation(Exercise.get(), cheat);
      r->Setup(std::move(Navigation), std::move(Exercise), fMainWindowTitle, data, cheat);
      r->SetupSignalSlots();

      if (cheat)
        r->Auto();
    }

    void StandardDataset::Start()
    {
      Utility::SetupStyleForClass();

      TString df;
      Int_t no = fDataset;
      if (fHasDemoSet && no == 0) {
        df = "AliVSD_example.root";
      } else {
        df.Form("AliVSD_MasterClass_%d.root", no);
      }

      UnmapWindow();
      TString gf = Utility::ResourcePath("data/Utility/geometry.root");
      Code(df, gf, fAllowAuto, no == 0);
    }

} // namespace Utility
