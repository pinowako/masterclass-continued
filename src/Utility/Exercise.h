/**
 * @file   Exercise.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 10:22:41 2017
 *
 * @brief  Base class for exercises
 *
 * @ingroup  alice_masterclass_base
 */
#ifndef ALICEEXERCISE_C
#define ALICEEXERCISE_C

#include <RConfig.h>
#include <RtypesCore.h>
#include <TApplication.h>
#include "VSDReader.h"

class TFile;
class TRootBrowser;

namespace Utility
{
/**
 * This defines the interface for exercises.  A specific exercise
 * should implement this interface to set up the GUI elements needed
 * by the exercise.
 *
 * @ingroup alice_masterclass_base
 */
class Exercise
{
 public:
  Exercise() = default;
  virtual ~Exercise() = default;

  /**
   * Set-up the elements needed by the exercise - e.g., GUI elements
   * or the like
   *
   * @param browser The browser we're using
   * @param cheat   If true, set-up for cheating
   */
  virtual void Setup(TRootBrowser* browser, Bool_t cheat) = 0;

  /// Toggle cheats on/off
  virtual void ToggleCheat(Bool_t /*on*/) {}

  /// Return a HTML styled string that explains what to do.
  virtual const TString& Instructions() const = 0;

  /// Return true if we can cheat
  virtual Bool_t CanCheat() const { return true; }

  /// Return true if we can save
  virtual Bool_t CanSave() const { return false; }

  /// Return true if we can print
  virtual Bool_t CanPrint() const { return false; }

  /**
   * Print results to a PDF
   * @param out Output file name
   */
  virtual void PrintPdf(const char* /*out*/) {}

  /// Export results to external file \p out.
  virtual void Export(const TString& /*out*/) {}

  /// Exit the exercise - default is to quit ROOT completely
  virtual void Exit() { gApplication->Terminate(); }

  /// Run the exercise in auto-mode
  virtual void Auto() {}

  /**
   * Return the path to data relative to the top-level directory
   *
   * @return Path to data, relative to top-level directory
   */
  virtual TString DataDir() const = 0;

  /**
   * Open a data file.
   *
   * @param filename The file name in the data directory
   * @param dir      (Optional) directory (relative to top)
   *
   * @return Pointer to file instance or null if not found.
   */
  virtual TFile* DataFile(const char* filename, const char* dir = nullptr) const;

  virtual std::unique_ptr<VSDReader> CreateReader();
};
} // namespace Utility

#endif
