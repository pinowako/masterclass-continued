/**
 * @file   MasterClass.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:53:56 2017
 *
 * @brief  Main entry point for the ALICE master classes
 *
 * @ingroup alice_masterclass
 */
#include <RQ_OBJECT.h>
#include <RtypesCore.h>
#include <memory>

/** @defgroup alice_masterclass ALICE Master classes */
#include "EntryPoint/GUIElements.h"
#include "EntryPoint/GUITranslation.h"
#include "Utility/LanguageProvider.h"

class TGLayoutHints;
class TGMainFrame;

namespace EntryPoint
{
/** A GUI to select the master class
 * @image html ClassSelector.png
 * @ingroup alice_masterclass
 */
    class ClassSelector
    {
    RQ_OBJECT("ClassSelector")

    private:
        // Provider for text in all supported Translations.
        std::unique_ptr<TGUIEnglish> fTranslation;

        /// Control how the separate areas behave when the window is scaled.
        TGLayoutHints* fLayoutHints;

        // Members for all subsections of the Window
        TGMainFrame* fMainFrame;

        TGUILogo fLogoArea;
        TGUISettings fSettingsArea;
        TGUIDescription fDescriptionArea;
        TGUIClasses fClassesArea;
        TGUIControlArea fControlArea;

        Bool_t fAllowAuto;
        Int_t fLastClassSelected;

    public:
        ClassSelector(Utility::ESupportedLanguages lang);
        ~ClassSelector() = default;

        /// Create the whole GUI.
        void SetupGUI();

        /// Toggle the god mode on/off. Activated by logo button.
        void ToggleAutoAnalysis();

        /** Called when user selects entry on the first (class) drop-down
         * @param i Entry selected
         */
        void ClassSelected(Int_t SelectionIdx);

        /** Called when user selects entry on the second (excersize) drop-down
         * @param i Entry selected
         */
        void ExerciseSelected(Int_t SelectionIdx);

        /// Enables the start button if the class and excersize is chosen correctly
        void checkIsSelectionCorrect();

        /** Called when user clicks the start button
         */
        void ExerciseStarted();

        /** Called when user selects an entry in the data source drop-down
         * @param i Entry selected
         */
        void SourceSelected(Int_t i);

        /** Called when user selects the language
         * @param i Language number
         */
        void LanguageSelected(Int_t i);
    };

} // namespace EntryPoint
