#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

// GUIElement.h
#pragma link C++ struct EntryPoint::ITranslateable+;
#pragma link C++ struct EntryPoint::TGUILogo+;
#pragma link C++ struct EntryPoint::TGUISettings+;
#pragma link C++ struct EntryPoint::TGUIDescription+;
#pragma link C++ struct EntryPoint::TGUIClasses+;
#pragma link C++ struct EntryPoint::TGUIControlArea+;

// GUITranslation.h
#pragma link C++ struct EntryPoint::TGUIEnglish+;
#pragma link C++ struct EntryPoint::TGUIGerman+;

// MasterClassRegistry.h
#pragma link C++ class EntryPoint::TMasterClassRegistry+;

// ClassSelector.h
#pragma link C++ class EntryPoint::ClassSelector+;

#endif
