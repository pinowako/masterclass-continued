#ifndef MASTERCLASSLANGUAGE_C_PYMZQKO6
#define MASTERCLASSLANGUAGE_C_PYMZQKO6

#include <iostream>
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"  // IWYU pragma: keep
#include "Utility/Utilities.h" // IWYU pragma: keep

namespace EntryPoint
{
/// Implement the English Translation (default used).
struct TGUIEnglish : Utility::TLanguageProvider {
  TGUIEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TLanguageProvider(lang)
  {
    std::cerr << "Registering English  GUI Selection\n";
#include "EntryPoint/keys_gui_trans.txt.en" // IWYU pragma: keep

    RegisterText("Description",
#include "EntryPoint/Description.en.html" // IWYU pragma: keep
    );
  }

  LANG_KEY(GUISettings)
  LANG_KEY(GUIDataSource)
  LANG_KEY(GUILabelClassConfig)
  LANG_KEY(GUIChooseLanguage)
  LANG_KEY(GUIChooseClass)
  LANG_KEY(GUIChooseExercise)
  LANG_KEY(Description)
  LANG_KEY(GUIStartButton)
};

/// Provide German translations for all text elements of main selection
/// GUI. Fallback to english (done via inheritance).
LANG_TRANSLATION(TGUI, German)
{
  std::cerr << "Registering German GUI Selection - EntryPoint\n";
#include "EntryPoint/keys_gui_trans.txt.de" // IWYU pragma: keep

  RegisterText("Description",
#include "EntryPoint/Description.de.html" // IWYU pragma: keep
  );
}

/// Factory function that allocates a new language provider according to
/// the requested language. Memory Management must be done by the caller.
struct TEntryPointLanguage {
  using ReturnType = TGUIEnglish;
  static std::unique_ptr<TGUIEnglish> create(Utility::ESupportedLanguages lang)
  {
    switch (lang) {
      case Utility::English:
        return std_fix::make_unique<TGUIEnglish>();
      case Utility::German:
        return std_fix::make_unique<TGUIGerman>();
      case Utility::NLanguages:
        break;
    }
    throw std::runtime_error("Unsupported language for  requested!");
  }
};
} // namespace EntryPoint

#endif /* end of include guard: MASTERCLASSLANGUAGE_C_PYMZQKO6 */
