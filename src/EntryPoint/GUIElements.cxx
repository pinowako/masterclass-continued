#include "GUIElements.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGGC.h>
#include <TGHtml.h>
#include <TString.h>
#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

#include "EntryPoint/GUITranslation.h"
#include "EntryPoint/MasterClassRegistry.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/Utilities.h"

class TGLayoutHints;
class TGWindow;

namespace EntryPoint
{
TGUILogo::TGUILogo(TGWindow* paren)
  : fLogoPicture(Utility::Logo())
  , fLogoButton(new TGPictureButton(paren, fLogoPicture, -1, TGButton::GetDefaultGC().GetGC(),
                                    kFitWidth | kFitHeight))
{
  // Configure the button behaviour while pressing
  fLogoButton->AllowStayDown(true);
}

TGUISettings::TGUISettings(TGWindow* paren, TGLayoutHints* lh)
  : fContentFrame(new TGGroupFrame(paren, ""))
  , fLanguageSelectionBox(new TGComboBox(fContentFrame))
  //, fDataSource(new TGComboBox(fContentFrame))
{
  fLanguageSelectionBox->Resize(300, 20);
  fContentFrame->AddFrame(fLanguageSelectionBox, lh);

//  fDataSource->Resize(300, 20);
//  fContentFrame->AddFrame(fDataSource, lh);
}

void TGUISettings::Translate(const TGUIEnglish& TextProvider)
{
  fContentFrame->SetTitle(TextProvider.GUISettings());

  // Update all available languages. Necessary, because the first element
  // will be translated.
  // No previous selection results in -1
  Int_t PreviousSelected = std::max(0, fLanguageSelectionBox->GetSelected());
  fLanguageSelectionBox->RemoveAll();

  // Write the first "Select Language" message translated. The following
  // languages are in English, to avoid confusion after a wrong selection.
  //
  // NOTE: The index of the added languages is increased by one, to allow
  // the instruction text to be identified correctly. This must be considered
  // when parsing out the language ID after the selection is updated.
  fLanguageSelectionBox->AddEntry(TextProvider.GUIChooseLanguage(), 0);
  for (Int_t lang_id = 0; lang_id < Utility::NLanguages; ++lang_id) {
    fLanguageSelectionBox->AddEntry(
      GetLanguageName(static_cast<Utility::ESupportedLanguages>(lang_id)).Data(), lang_id + 1);
  }

  fLanguageSelectionBox->Select(PreviousSelected, false);

  // Update all sources, first element is translated
  //SetupSources(TextProvider);
}

void TGUISettings::SetupSources(const TGUIEnglish& TextProvider)
{
  // No previous selection results in -1
  Int_t PreviousSelected = std::max(0, fDataSource->GetSelected());
  fDataSource->RemoveAll();

  fDataSource->AddEntry(TextProvider.GUIDataSource(), 0);

  Int_t id = 1;
  if (Utility::GetEnvValue("ALICE_MASTERCLASS").Length() > 0) {
    fDataSource->AddEntry(Utility::GetEnvValue("ALICE_MASTERCLASS"), id++);
  }

  if (Utility::GetEnvValue("ALICE_MASTERCLASS_DATA").Length() > 0) {
    fDataSource->AddEntry(Utility::GetEnvValue("ALICE_MASTERCLASS_DATA"), id++);
  }

  fDataSource->AddEntry(".", id++);
  fDataSource->AddEntry("https://cern.ch/cholm/alice-masterclasses-data", id++);
  fDataSource->AddEntry("http://cern.ch/alice-masterclasses/data", id++);

  // FIXME: EnableTextInput trigger as use-after-free
  // Allow raw input.
  // fDataSource->EnableTextInput(true);

  // Select nothing
  fDataSource->Select(PreviousSelected, false);
}

TGUIDescription::TGUIDescription(TGWindow* paren)
  : fDesc(new TGHtml(paren, 450, 400, 1))
{
}

void TGUIDescription::Translate(const TGUIEnglish& TextProvider)
{
  fDesc->Clear();
  fDesc->ParseText(const_cast<char*>(TextProvider.Description().Data()));
}

TGUIClasses::TGUIClasses(TGWindow* paren, TGLayoutHints* lh, Utility::ESupportedLanguages lang)
  : fClassRegistry(lang)
  , fContentFrame(new TGGroupFrame(paren, ""))
  , fClassesBox(new TGComboBox(fContentFrame))
  , fExerciseBox(new TGComboBox(fContentFrame))
{
  fClassesBox->Resize(300, 20);
  fContentFrame->AddFrame(fClassesBox, lh);

  fExerciseBox->Resize(300, 20);
  fExerciseBox->SetEnabled(false);
  fContentFrame->AddFrame(fExerciseBox, lh);
}

void TGUIClasses::Translate(const TGUIEnglish& TextProvider)
{
  fContentFrame->SetTitle(TextProvider.GUILabelClassConfig());

  // Save which class and exercise has been selected, to reselect it
  // after translation.
  Int_t SelectedClassIdx = std::max(0, fClassesBox->GetSelected());
  FillClassesSelection(TextProvider, SelectedClassIdx);
}

/// Fill all entries for the masterclass selection including the exercises
/// if an actual class is selected.
void TGUIClasses::FillClassesSelection(const TGUIEnglish& TextProvider, Int_t SelectedClassIdx)
{
  std::cerr << "Filling Classes\n";
  fClassesBox->RemoveAll();
  fClassesBox->AddEntry(TextProvider.GUIChooseClass(), 0);

  for (std::size_t i = 0; i < fClassRegistry.GetClasses().size(); ++i) {
    const Utility::TAbstractMasterClassContent* Class = fClassRegistry.GetClasses().at(i).get();
    fClassesBox->AddEntry(Class->GetName(), i + 1);
  }
  fClassesBox->Select(SelectedClassIdx, false);

  // A Masterclass is choosen, so fill all possible exercises, too.
  if (SelectedClassIdx >= 0) {
    // NOTE Minus 1 is necessary, because exercises is expected to be the
    // correct index into the vector!
    FillExercises(TextProvider, SelectedClassIdx - 1);
  }
}

void TGUIClasses::FillExercises(const TGUIEnglish& TextProvider, Int_t SelectedClass)
{
  Int_t SelectedExerciseIdx = std::max(0, fExerciseBox->GetSelected());
  std::cerr << "Filling exercise choices for class # " << SelectedClass << "\n";
  fExerciseBox->RemoveAll();
  fExerciseBox->AddEntry(TextProvider.GUIChooseExercise(), 0);

  if (SelectedClass >= 0) {
    // Gets incremented in each loop, index of the element in the selection.
    int i = 1;
    for (const auto& Name : fClassRegistry.GetClasses().at(SelectedClass)->GetExerciseNames()) {
      fExerciseBox->AddEntry(Name, i++);
    }
  }

  fExerciseBox->Select(SelectedExerciseIdx, false);
}

TGUIControlArea::TGUIControlArea(TGWindow* paren)
  : fStartButton(new TGTextButton(paren, ""))
{
}

void TGUIControlArea::Translate(const TGUIEnglish& TextProvider)
{
  fStartButton->SetText(TextProvider.GUIStartButton());
}
} // namespace EntryPoint
