#ifndef MASTERCLASSES_SIGNALEXTRACTION_H
#define MASTERCLASSES_SIGNALEXTRACTION_H

#include <TGFrame.h>
#include "GUITranslation.h"
#include <TGNumberEntry.h>
#include <Utility/VSDReader.h>
#include <Utility/TEventAnalyseGUI.h>


namespace Jpsi
{
    class EventDisplay;
    class SignalExtraction  : public TGMainFrame
    {
    private:
        TGNumberEntryField *fMinmF, *fMaxmF, *fJpsis, *fSignalBackground, *fSignificance;
        Float_t fMinm,fMaxm;
        Utility::VSDReader *fReader;
        Utility::TEventAnalyseGUI *fGUI;
        EventDisplay *fDisplay;
        Bool_t fIsEngaged;

    public:
        const TGUIEnglish& fTranslation;
        TGTextButton *fExtract;
        TGTextButton *fApply;

        SignalExtraction(const TGWindow* p, UInt_t w, UInt_t h);

        void ExtractClicked();
        void SetResults(Float_t jpsiCount, Float_t signalBackground, Float_t significance);

//        void LockInputs(Bool_t lock);
//
//        void ToggleCuts();
//        void ToggleFrameEnabled();
        void Instructions();
        void Reset();
//
        void CalculateSignal(Float_t *m1, Float_t *m2); //*SIGNAL*

    ClassDef(SignalExtraction, 0);
    };
}



#endif //MASTERCLASSES_SIGNALEXTRACTION_H
