#include <TRootHelpDialog.h>
#include "SignalExtraction.h"
#include "InvariantMass.h"

namespace Jpsi {

    SignalExtraction::SignalExtraction(const TGWindow *p, UInt_t w, UInt_t h)
            : TGMainFrame(p, w, h, kFixedHeight | kHorizontalFrame)
            , fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
            , fMinmF(nullptr), fMaxmF(nullptr), fJpsis(nullptr), fSignalBackground(nullptr), fSignificance(nullptr)
            , fApply(nullptr)
            , fExtract(nullptr)
            , fIsEngaged(kFALSE) {
        DontCallClose();

        auto *lh = new TGLayoutHints(kLHintsExpandX);

        // Make Left hand side for particles
        auto *gf = new TGGroupFrame(this, "Signal Extraction", kFixedWidth | kVerticalFrame);
        gf->Resize(300, 150);

        // Make header frames
        auto *hf = new TGHorizontalFrame(gf);

        auto *val1 = new TGLabel(hf, "    ");
        val1->SetTextJustify(kTextCenterX);
        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

        val1 = new TGLabel(hf, "min");
        val1->SetTextJustify(kTextCenterX);
        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

        val1 = new TGLabel(hf, "max");
        val1->SetTextJustify(kTextCenterX);
        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        gf->AddFrame(hf, lh);

        hf = new TGHorizontalFrame(gf);

        val1 = new TGLabel(hf, fTranslation.MassRange());
        fMinmF = new TGNumberEntryField(hf);
        fMinmF->SetNumber(0.);
        fMaxmF = new TGNumberEntryField(hf);
        fMaxmF->SetNumber(0.);

        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        hf->AddFrame(fMinmF, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        hf->AddFrame(fMaxmF, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        gf->AddFrame(hf, lh);

        fExtract = new TGTextButton(gf, fTranslation.ButtonExtractSignal());
        fExtract->Connect("Clicked()", "Jpsi::SignalExtraction", this, "ExtractClicked()");
////        fExtract->Connect("Pressed()", "Jpsi::TVSDReader", parent, "CalculateIntegral()");
////        fExtract->Connect("Pressed()", "Jpsi::TVSDReader", parent, "SetMassLineValues()");
////        fExtract->Connect("Pressed()", "Jpsi::TVSDReader", parent, "PlotMassLines()");
////        fExtract->Connect("Pressed()", "Jpsi::TVSDReader", parent, "SetExtractorFieldValues()");
////        fExtract->Connect("Pressed()", "Jpsi::TVSDReader", parent, "Autosave()");
        gf->AddFrame(fExtract, new TGLayoutHints(kLHintsExpandX | kLHintsBottom));
        AddFrame(gf, new TGLayoutHints(kLHintsLeft | kLHintsExpandY, 4, 2, 2, 4));

        // Make operations
        gf = new TGGroupFrame(this, "Operations", kVerticalFrame);

        // --- Instructions button ---------------------------------------
        auto *InstButton = Utility::MakeInstructionsButton(gf);
        InstButton->Connect("Clicked()", "Jpsi::SignalExtraction", this, "Instructions()");

        lh = new TGLayoutHints(kLHintsExpandX | kLHintsBottom);

        hf = new TGHorizontalFrame(gf);

        val1 = new TGLabel(hf, fTranslation.NumberJpsis());
        val1->SetTextJustify(kTextCenterX);
        fJpsis = new TGNumberEntryField(hf);
        fJpsis->SetEnabled(kFALSE);
        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        hf->AddFrame(fJpsis, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        gf->AddFrame(hf, lh);

        hf = new TGHorizontalFrame(gf);

        val1 = new TGLabel(hf, fTranslation.SignalBackground());
        val1->SetTextJustify(kTextCenterX);
        fSignalBackground = new TGNumberEntryField(hf);
        fSignalBackground->SetEnabled(kFALSE);
        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        hf->AddFrame(fSignalBackground, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        gf->AddFrame(hf, lh);

        hf = new TGHorizontalFrame(gf);

        val1 = new TGLabel(hf, fTranslation.Significance());
        val1->SetTextJustify(kTextCenterX);
        fSignificance = new TGNumberEntryField(hf);
        fSignificance->SetEnabled(kFALSE);
        hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        hf->AddFrame(fSignificance, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
        gf->AddFrame(hf, lh);

        AddFrame(gf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 4, 2, 4));

        MapSubwindows();
        TGDimension d = GetDefaultSize();
        Resize(d.fWidth, 400);
        MapWindow();
    }

    void SignalExtraction::Instructions()
    {
        auto* instructions =
                new TRootHelpDialog(gClient->GetRoot(), fTranslation.SelectorInstructionsTitle(), 700, 400);
        instructions->SetText(fTranslation.SignalExtractionInstructions());

        instructions->Popup();
    }

    void SignalExtraction::ExtractClicked() {
        Float_t arr[2];
        arr[0] = Utility::Clamp(fMinmF->GetNumber(), InvariantMass::minX, InvariantMass::maxX);
        arr[1] = Utility::Clamp(fMaxmF->GetNumber(), InvariantMass::minX, InvariantMass::maxX);

        fMinmF->SetNumber(arr[0]);
        fMaxmF->SetNumber(arr[1]);

        Long_t args[2];
        args[0] = (Long_t)&arr[0];
        args[1] = (Long_t)&arr[1];

        Emit("CalculateSignal(Float_t*, Float_t*)", args);
    }

    void SignalExtraction::SetResults(Float_t jpsiCount, Float_t signalBackground, Float_t significance) {
        fJpsis->SetNumber(jpsiCount);
        fSignalBackground->SetNumber(signalBackground);
        fSignificance->SetNumber(significance);
    }

    void SignalExtraction::Reset() {
        fMinmF->SetNumber(0.);
        fMaxmF->SetNumber(0.);
        fJpsis->SetNumber(0.);
        fSignalBackground->SetNumber(0.);
        fSignificance->SetNumber(0.);
    }
}
