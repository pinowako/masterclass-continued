#ifndef MASTERCLASSES_TRACKCUTS_H
#define MASTERCLASSES_TRACKCUTS_H

#include <TGFrame.h>
#include "GUITranslation.h"
#include <TGNumberEntry.h>


namespace Jpsi
{
    class EventDisplay;
class TrackCuts  : public TGMainFrame
{
private:
    TGNumberEntryField *fP1F, *fP2F, *fD1F, *fD2F;
    Bool_t fIsEngaged;
public:
    const TGUIEnglish& fTranslation;
    TGTextButton *fApply;

    TrackCuts(const TGWindow* p, UInt_t w, UInt_t h);

    void LockInputs(Bool_t lock);

    void ToggleCuts();
    void ToggleFrameEnabled();
    void Instructions();
    void ParticleId();
    void Reset();

    void CutsChanged(Bool_t enabled, Float_t *p1, Float_t *p2, Float_t *d1, Float_t *d2); //*SIGNAL*

ClassDef(TrackCuts, 0);
};
}

#endif //MASTERCLASSES_TRACKCUTS_H
