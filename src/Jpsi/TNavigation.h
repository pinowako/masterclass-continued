#ifndef TNAVIGATION_H_MPCOYTDS
#define TNAVIGATION_H_MPCOYTDS

#include <Rtypes.h>
#include <TGFrame.h>
#include <TString.h>

#include "Utility/INavigation.h"

class TBuffer;
class TClass;
class TGCheckButton;
class TGCompositeFrame;
class TGHorizontalFrame;
class TGLabel;
class TGNumberEntryField;
class TGPictureButton;
class TGTextButton;
class TGVerticalFrame;
class TMemberInspector;

namespace Utility
{
class EventDisplay;
class TEventVisualization;
} // namespace Utility

namespace Jpsi
{
class TVSDReader;
struct TGUIEnglish;

namespace internal
{
struct TAnalysis : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;
  TGTextButton* fButtonFillPidHistos;
  TGTextButton* fButtonFillInvMass;
  TAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(TAnalysis, 0);
};
struct TDisplay : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;
  TGTextButton* fButtonShowTracks;
  TGTextButton* fButtonShowDetector;
  TGTextButton* fButtonToggleBackground;

  TDisplay(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(TDisplay, 0);
};
struct THistogram : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;
  TGTextButton* fButtonClearHistogram;
  TGTextButton* fButtonRestart;

  THistogram(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(THistogram, 0);
};
struct TQuickAnalysis : TGGroupFrame {
  TGVerticalFrame* fMotherFrame;

  TGHorizontalFrame* fQuickAnalysisFrame;
  TGTextButton* fButtonQuickAnalysis;

  TGHorizontalFrame* fEventFrame;
  TGLabel* fLoopNumberLabel;
  TGNumberEntryField* fNLoop;

  TGHorizontalFrame* fJumpFrame;
  TGTextButton* fButtonJumpEvents;

  TQuickAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation);
  ClassDefOverride(TQuickAnalysis, 0);
};
struct TParticleInfo : TGGroupFrame {
  TParticleInfo(TGCompositeFrame* parent, const TGUIEnglish& Translation);
  ClassDef(TParticleInfo, 0);
};
} // namespace internal

class TNavigation : public Utility::INavigation
{
 private:
  friend class TVSDReader;
  const TGUIEnglish& fTranslation;

  internal::TAnalysis* fAnalysisJpsiFrame;
  internal::TDisplay* fDisplayJpsiFrame;
  internal::THistogram* fHistogramJpsiFrame;
  internal::TQuickAnalysis* fQuickAnalysisJpsiFrame;
  Utility::TEventAnalyseGUI *fAnalysisGUI;
  internal::TParticleInfo *fParticleInfo;

 public:
  TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat);

  void SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI, Utility::TEventVisualization *EventVisualization, Utility::VSDReader* Reader) override;

  void ToggleQuickAnalysis();
  void StartFastForward();
  void Reset();

protected:
    void InitialLoad() override;

  ClassDefOverride(TNavigation, 0);
};
} // namespace Jpsi

#endif /* end of include guard: TNAVIGATION_H_MPCOYTDS */
