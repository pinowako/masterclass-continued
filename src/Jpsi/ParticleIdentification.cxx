#include "ParticleIdentification.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TLine.h>
#include <TString.h>
#include <TMath.h>
#include <TH2F.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Jpsi
{
    ParticleIdentification::ParticleIdentification()
            : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
            , fEnergyLoss(nullptr)
            , fCanvas(nullptr)
            , fL1(nullptr), fL2(nullptr), fL3(nullptr), fL4(nullptr)
            , fNSelectedTracks(0)
    {
      Int_t nbinsX = 200;
      Int_t nbinsY = 120;
      auto* binLimitsX = new Float_t[nbinsX + 1];
      auto* binLimitsY = new Float_t[nbinsX + 1];
      Float_t expMax = TMath::Log(maxX / minX);
      for (Int_t i = 0; i < nbinsX + 1; ++i) {
        binLimitsX[i] = minX * TMath::Exp(expMax / nbinsX * (Float_t)i);
      }
      for (Int_t i = 0; i < nbinsY + 1; ++i) {
        binLimitsY[i] = minY + i * (maxY - minY) / nbinsY;
      }

      fEnergyLoss = new TH2F(fTranslation.SpecificEnergyLoss(), fTranslation.SpecificEnergyLoss(),
                             nbinsX, binLimitsX, nbinsY, binLimitsY);
      fEnergyLoss->GetXaxis()->SetTitle(fTranslation.MomentumGeVc());
      fEnergyLoss->GetXaxis()->SetTitleOffset(1.5);
      fEnergyLoss->GetYaxis()->SetTitle("dE/dx in TPC (arb. units)");
      fEnergyLoss->SetStats(false);
    }

    void ParticleIdentification::Setup()
    {
      fCanvas = new TCanvas();

      fCanvas->cd();
      fCanvas->SetLogx(1);
      fEnergyLoss->Draw();
      fCanvas->Modified();
      fCanvas->Update();
      fCanvas->SetEditable(kFALSE);
    }

    void ParticleIdentification::Fill()
    {
      fCanvas->SetEditable(kTRUE);
      for (int i = 0; i < fNSelectedTracks; i++) {
        fEnergyLoss->Fill(fArrP[i], fArrdEdx[i]);
      }

      ClearData();
      Update();
      fCanvas->SetEditable(kFALSE);
    }

    void ParticleIdentification::Update()
    {
      fCanvas->SetEditable(kTRUE);
      fCanvas->cd();
      fEnergyLoss->Draw("colz");
      if(IsEnabled())
      {
        fL1->Draw();
        fL2->Draw();
        fL3->Draw();
        fL4->Draw();
      }
      fCanvas->Modified();
      fCanvas->Update();
      fCanvas->SetEditable(kFALSE);
    }

    void ParticleIdentification::AddParticle(Float_t momentum, Float_t d_energy)
    {
      fArrP[fNSelectedTracks] = momentum;
      fArrdEdx[fNSelectedTracks] = d_energy;

      fNSelectedTracks++;
    }

    void ParticleIdentification::ClearData()
    {
      for (int i = 0; i < fNSelectedTracks; i++) {
        fArrP[i] = 0;
        fArrdEdx[i] = 0;
      }
      fNSelectedTracks = 0;
    }

    void ParticleIdentification::Export(const char* name)
    {
      TFile* out = TFile::Open(name, "RECREATE");
      out->cd();
      fEnergyLoss->Write();
      out->Write();
      out->Close();
    }

    void ParticleIdentification::SetPIDCutValues(Bool_t enabled, Float_t p1, Float_t p2, Float_t d1, Float_t d2)
    {
      delete fL1;
      delete fL2;
      delete fL3;
      delete fL4;

      if(enabled) {
        fL1 = new TLine;
        fL2 = new TLine;
        fL3 = new TLine;
        fL4 = new TLine;

        SetupLine(fL1, p1, p1, d1, d2);
        SetupLine(fL2, p2, p2, d1, d2);
        SetupLine(fL3, p1, p2, d1, d1);
        SetupLine(fL4, p1, p2, d2, d2);
      }
      else {
        fL1 = fL2 = fL3 = fL4 = nullptr;
      }
    }

    void ParticleIdentification::SetupLine(TLine* l, Float_t x1, Float_t x2, Float_t y1, Float_t y2, Int_t LineColor)
    {
      l->SetX1(x1);
      l->SetX2(x2);
      l->SetY1(y1);
      l->SetY2(y2);
      l->SetLineColor(LineColor);
      l->SetLineStyle(5);
    }

    void ParticleIdentification::ClearHisto() {
      fEnergyLoss->Reset();
      Update();
    }

    Bool_t ParticleIdentification::IsEnabled() {
        return fL1 != nullptr;
    }

} // namespace Jpsi

