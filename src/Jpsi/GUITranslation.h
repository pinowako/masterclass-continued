#ifndef GUITRANSLATION_H_LEBOYD0F
#define GUITRANSLATION_H_LEBOYD0F

#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h" // IWYU pragma: keep
#include "Utility/Utilities.h" // IWYU pragma: keep

namespace Jpsi
{
struct TGUIEnglish : Utility::TLanguageProvider {
  TGUIEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TLanguageProvider(lang)
  {
    std::cerr << "Registering English GUI Selection for Jpsi\n";
#include "Jpsi/keys_gui_trans.txt.en" // IWYU pragma: keep

    RegisterText("DataSetInstruction",
#include "Jpsi/Dataset.en.html"
    );
    RegisterText("PathEventDisplay",
#include "Jpsi/EventDisplay.en.html"
    );
  }

  LANG_KEY(RPhiGeom)
  LANG_KEY(RPhiGeomDesc)
  LANG_KEY(RhoZGeom)
  LANG_KEY(RhoZGeomDesc)

  LANG_KEY(RPhiEvent)
  LANG_KEY(RPhiEventDesc)
  LANG_KEY(RhoZEvent)
  LANG_KEY(RhoZEventDesc)

  LANG_KEY(MultiView)
  LANG_KEY(View3D)
  LANG_KEY(RPhiView)
  LANG_KEY(RhoZView)

  LANG_KEY(ALICEDetector)
  LANG_KEY(DetectorInfoTitle)

  LANG_KEY(SelectorTitle)
  LANG_KEY(SelectorInstructionsTitle)
  LANG_KEY(SelectorInstructionsText)

  LANG_KEY(MassRange)
  LANG_KEY(NumberJpsis)
  LANG_KEY(SignalBackground)
  LANG_KEY(SignalExtraction)
  LANG_KEY(SignalExtractionInstructions)
  LANG_KEY(Significance)
  LANG_KEY(StatisticsJpsi)
  LANG_KEY(DifferenceEPmEEpPP)
  LANG_KEY(Counts)
  LANG_KEY(StatisticsElectronPositron)
  LANG_KEY(InvariantMassElectronPositron)
  LANG_KEY(StatisticsElectronElectron)
  LANG_KEY(InvariantMassElectronElectron)
  LANG_KEY(StatisticsPositronPositron)
  LANG_KEY(InvariantMassPositronPositron)
  LANG_KEY(CopyElectronElectron)
  LANG_KEY(ElectronElectronDistribution)
  LANG_KEY(CopyPositronPositron)
  LANG_KEY(PositronPositronDistribution)
  LANG_KEY(SpecificEnergyLoss)
  LANG_KEY(MomentumGeVc)

  LANG_KEY(InstructionsJpsi)
  LANG_KEY(InstructionsJpsiText)

  LANG_KEY(ParticleIdentificationPlot)
  LANG_KEY(MasterclassTitle)
  LANG_KEY(EventNavigation)
  LANG_KEY(AnalysisTools)
  LANG_KEY(Display)
  LANG_KEY(Histogram)
  LANG_KEY(QuickAnalysis)
  LANG_KEY(Tools)
  LANG_KEY(ParticleIdentification)
  LANG_KEY(InvariantMass)
  LANG_KEY(WarningTooManyEvents)
  LANG_KEY(WarningQuickAnalysis)
  LANG_KEY(WarningLoadBeforeFilling)
  LANG_KEY(WarningLoadBforeEnergy)
  LANG_KEY(DataSelection)
  LANG_KEY(CollisionPP)
  LANG_KEY(CollisionPPb)

  LANG_KEY(MasterclassInstructionsTitle)
  LANG_KEY(MasterclassInstructionsText)

  LANG_KEY(ButtonApplyCuts)
  LANG_KEY(ButtonBackgroundColor)
  LANG_KEY(ButtonClearHistograms)
  LANG_KEY(ButtonClose)
  LANG_KEY(ButtonCurrent)
  LANG_KEY(ButtonDefineCuts)
  LANG_KEY(ButtonEngage)
  LANG_KEY(ButtonExit)
  LANG_KEY(ButtonExtractSignal)
  LANG_KEY(ButtonFillInvariantMassHistogram)
  LANG_KEY(ButtonFillPIDHistogram)
  LANG_KEY(ButtonJumpNEvents)
  LANG_KEY(ButtonInstructions)
  LANG_KEY(ButtonShowTracks)
  LANG_KEY(ButtonNext)
  LANG_KEY(ButtonPrevious)
  LANG_KEY(ButtonRestart)
  LANG_KEY(ButtonShowDetector)
  LANG_KEY(ButtonStartAnalysis)

  LANG_KEY(DataSetInstruction)
  LANG_KEY(PathEventDisplay)
};

LANG_TRANSLATION(TGUI, German)
{
  std::cerr << "Registering German Jpsi Content\n";
#include "Jpsi/keys_gui_trans.txt.de"

    RegisterText("DataSetInstruction",
#include "Jpsi/Dataset.de.html"
    );

}

/// Factory function that allocates a new language provider according to
/// the requested language.
struct TJPsiLanguage {
  using ReturnType = TGUIEnglish;
  static std::unique_ptr<TGUIEnglish> create(Utility::ESupportedLanguages lang)
  {
    switch (lang) {
      case Utility::English:
        return std_fix::make_unique<TGUIEnglish>();
      case Utility::German:
        return std_fix::make_unique<TGUIGerman>();
      case Utility::NLanguages:
        break;
    }
    throw std::runtime_error("Unsupported language for Jpsi requested!");
  }
};

} // namespace Jpsi

#endif /* end of include guard: GUITRANSLATION_H_LEBOYD0F */
