#ifndef MASTERCLASSES_INVARIANTMASS_H
#define MASTERCLASSES_INVARIANTMASS_H


#include <RtypesCore.h>
#include <TCanvas.h>
#include <TFile.h>

#include "Jpsi/GUITranslation.h"

class TH1F;
class TLine;

namespace Jpsi
{
    struct TGUIEnglish;

    struct InvariantMass {

        static constexpr Float_t minX = 0.0;
        static constexpr Float_t maxX = 6.0;

        const TGUIEnglish& fTranslation;
        TH1F *fJpsiHist, *fMinvHist, *fEeHist, *fPpHist, *fEHist, *fPHist;
        TLine *fL1, *fL2;

        Float_t fEl[100][4];
        Float_t fPo[100][4];
        Int_t fNPositrons;
        Int_t fNElectrons;

        TCanvas* fCanvas{};

        InvariantMass();

        void Setup();
        void Fill();
        void Update();
        void AddPositron(Float_t energy, Float_t px, Float_t py, Float_t pz);
        void AddElectron(Float_t energy, Float_t px, Float_t py, Float_t pz);
        void ClearData();
        void ClearHisto();
        void ClearTemp();
        void CalculateIntegral(Float_t minm, Float_t maxm, Float_t& numJpsi, Float_t& signalBackground, Float_t& significance);
        void SetMassLineValues(Bool_t enabled, Float_t minm, Float_t maxm);
//        void Export(const char* name);
//        void PrintPdf(const char* name) { fCanvas->SaveAs(name); }
//        void SetPIDCutValues(Float_t p1, Float_t p2, Float_t d1, Float_t d2);
//        void PlotPIDLines();
//        void SetupLine(TLine* l, Float_t x1, Float_t x2, Float_t y1, Float_t y2, Int_t LineColor = 2);

    };
} // namespace Jpsi

#endif //MASTERCLASSES_INVARIANTMASS_H
