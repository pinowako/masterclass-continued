#ifndef MASTERCLASSES_PARTICLEIDENTIFICATION_H
#define MASTERCLASSES_PARTICLEIDENTIFICATION_H

#include <RtypesCore.h>
#include <TCanvas.h>
#include <TFile.h>

#include "Jpsi/GUITranslation.h"

class TH2F;
class TLine;

namespace Jpsi
{
    struct TGUIEnglish;

    struct ParticleIdentification {

        static constexpr Float_t minX =   0.1;
        static constexpr Float_t maxX =  20.0;
        static constexpr Float_t minY =  20.0;
        static constexpr Float_t maxY = 140.0;

        const TGUIEnglish& fTranslation;
        TH2F* fEnergyLoss;
        TCanvas* fCanvas{};
        TLine *fL1, *fL2, *fL3, *fL4;

        Int_t fNSelectedTracks;
        Float_t fArrP[300];
        Float_t fArrdEdx[300];

        ParticleIdentification();

        void Setup();
        void Fill();
        void Update();
        void AddParticle(Float_t momentum, Float_t d_energy);
        void ClearData();
        void ClearHisto();
        Bool_t IsEnabled();
        void Export(const char* name);
        void PrintPdf(const char* name) { fCanvas->SaveAs(name); }
        void SetPIDCutValues(Bool_t enabled, Float_t p1, Float_t p2, Float_t d1, Float_t d2);
        void SetupLine(TLine* l, Float_t x1, Float_t x2, Float_t y1, Float_t y2, Int_t LineColor = 2);

    };
} // namespace Jpsi

#endif //MASTERCLASSES_PARTICLEIDENTIFICATION_H
