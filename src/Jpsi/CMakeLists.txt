set(MODULE Jpsi)

set(SRCS
  TJpsiClass.cxx
  TNavigation.cxx
  EventDisplay.cxx
  TrackCuts.cxx
  SignalExtraction.cxx
  InvariantMass.cxx
  ParticleIdentification.cxx
  JpsiVSDReader.cxx
 )

list(TRANSFORM SRCS REPLACE ".cxx" ".h" OUTPUT_VARIABLE HDRS)

list(APPEND SRCS GUITranslation.h ClassContent.h)

list(TRANSFORM HDRS PREPEND "${MODULE}/" OUTPUT_VARIABLE CLING_HDRS)

root_generate_dictionary("G__${MODULE}" "${CLING_HDRS}" MODULE "${MODULE}" LINKDEF "${CMAKE_SOURCE_DIR}/src/${MODULE}/${MODULE}LinkDef.h" OPTIONS -noIncludePaths)

add_library(${MODULE} SHARED ${SRCS} ${HDRS} ${MODULE}LinkDef.h G__${MODULE}.cxx)

target_link_libraries(${MODULE}
  PUBLIC ROOT::Core ROOT::Gui Utility
  PRIVATE gsl
)

# Install the library file
install(TARGETS ${MODULE} LIBRARY DESTINATION lib RUNTIME DESTINATION bin)
# Install the ROOT dicitonary files
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/lib${MODULE}.rootmap" DESTINATION ${ROOTDICT_DESTINATION})
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/lib${MODULE}_rdict.pcm" DESTINATION ${ROOTDICT_DESTINATION})
