//
// Created by user on 7/7/19.
//

#include "JpsiVSDReader.h"

namespace Jpsi
{

    void JpsiVSDReader::LoadJpsiParameters(Bool_t active, Float_t *p1, Float_t *p2, Float_t *d1, Float_t *d2)
    {
        fApplyTrackCuts = active;
        fP1 = *p1;
        fP2 = *p2;
        fD1 = *d1;
        fD2 = *d2;
    }

    Bool_t JpsiVSDReader::handleTrack(TEveTrack *track)
    {
        Float_t px = track->GetMomentum().fX;
        Float_t py = track->GetMomentum().fY;
        Float_t pz = track->GetMomentum().fZ;
        Float_t dedx = track->GetStatus();
        Int_t charge = track->GetCharge();
        Float_t pSquared = px * px + py * py + pz * pz;

        TString ident = charge > 0 ? "electron" : "positron";

        track->SetTitle(Form("p=%5.3f\nde/dx=%5.3f\nident=%s", TMath::Sqrt(pSquared), dedx, ident.Data()));

        if (fApplyTrackCuts && (dedx >= fD2 || dedx < fD1 || pSquared >= fP2 * fP2 || pSquared <= fP1 * fP1))
        {
            return kFALSE;
        }
        track->SetLineColor(9);
        return kTRUE;
    }
}
