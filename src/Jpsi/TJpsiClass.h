#ifndef FULLCLASS_H_ZQNNWRO8
#define FULLCLASS_H_ZQNNWRO8

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>
#include <TString.h>
#include <Utility/Dataset.h>

#include "Utility/AbstractMasterClassContent.h"
#include "Jpsi/GUITranslation.h"

class TBuffer;
class TClass;
class TGComboBox;
class TGTextButton;
class TMemberInspector;

namespace Jpsi
{
void alice_vsd(Int_t dataset);

class JpsiDataset : public Utility::Dataset
{
private:
    TGUIEnglish& fTranslation;

//    std::unique_ptr<Utility::EventDisplay> createEventDisplay(Bool_t demo) override;
//    std::unique_ptr<Utility::INavigation> createNavigation(Utility::EventDisplay* exercise, Bool_t cheat) override;
    void fillSelectionBox() override;
public:
    JpsiDataset(Bool_t allowAuto = false);
    void Start() override;

    ClassDefOverride(JpsiDataset, 0)
};

//class MasterClassFrame : public TGMainFrame
//{
// private:
//  TGTextButton* fExample;
//  TGTextButton* fStudent;
//  TGTextButton* fTeacher;
//  TGTextButton* fExit;
//  TGComboBox* fDataset;
//
// public:
//  MasterClassFrame();
//  ~MasterClassFrame() override { Cleanup(); }
//  void Start();
//  ClassDefOverride(MasterClassFrame, 0);
//};

class TJpsiClass : public Utility::TAbstractExercise
{
 public:
  TJpsiClass()
    : TAbstractExercise("Jpsi Analysis")
  {
  }

  void RunExercise(Bool_t AllowAuto = false) override;
};
} // namespace Jpsi

#endif /* end of include guard: FULLCLASS_H_ZQNNWRO8 */
