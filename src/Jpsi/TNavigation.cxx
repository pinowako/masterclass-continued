#include "Jpsi/TNavigation.h"

#include <GuiTypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFont.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGNumberEntry.h>
#include <TSystem.h>
#include <TLatex.h>
#include <TRootEmbeddedCanvas.h>

#include "Jpsi/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/TEventAnalyseGUI.h"
#include "EventDisplay.h"

namespace Utility
{
class EventDisplay;
} // namespace Utility

namespace Jpsi
{
namespace internal
{
TAnalysis::TAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.AnalysisTools())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))
  , fButtonFillPidHistos(new TGTextButton(fMotherFrame, Translation.ButtonFillPIDHistogram()))
  , fButtonFillInvMass(
      new TGTextButton(fMotherFrame, Translation.ButtonFillInvariantMassHistogram()))
{
  auto* KHintX2 = new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1);

  fMotherFrame->AddFrame(fButtonFillPidHistos, KHintX2);
  fMotherFrame->AddFrame(fButtonFillInvMass, KHintX2);
  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}

TDisplay::TDisplay(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.Display())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))
  , fButtonShowTracks(new TGTextButton(fMotherFrame, Translation.ButtonShowTracks()))
  , fButtonShowDetector(new TGTextButton(fMotherFrame, Translation.ButtonShowDetector()))
  , fButtonToggleBackground(new TGTextButton(fMotherFrame, Translation.ButtonBackgroundColor()))
{
  auto* KExpandX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);

  fButtonShowTracks->AllowStayDown(true);
  fButtonShowDetector->AllowStayDown(true);
  fButtonToggleBackground->AllowStayDown(true);

  fMotherFrame->AddFrame(fButtonShowTracks, KExpandX);
  fMotherFrame->AddFrame(fButtonShowDetector, KExpandX);
  fMotherFrame->AddFrame(fButtonToggleBackground, KExpandX);

  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}

THistogram::THistogram(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.Histogram())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))
  , fButtonClearHistogram(new TGTextButton(fMotherFrame, Translation.ButtonClearHistograms()))
  , fButtonRestart(new TGTextButton(fMotherFrame, Translation.ButtonRestart()))
  {
    auto* KExpandX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);
    fMotherFrame->AddFrame(fButtonClearHistogram, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 1));
    fMotherFrame->AddFrame(fButtonRestart, KExpandX);

    this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}

TQuickAnalysis::TQuickAnalysis(TGCompositeFrame* Parent, const TGUIEnglish& Translation)
  : TGGroupFrame(Parent, Translation.QuickAnalysis())
  , fMotherFrame(new TGVerticalFrame(this, 200, 20, kFixedWidth))

  , fQuickAnalysisFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonQuickAnalysis(new TGTextButton(fQuickAnalysisFrame, Translation.QuickAnalysis()))

  , fEventFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fLoopNumberLabel(new TGLabel(fEventFrame, " N      = "))
  , fNLoop(new TGNumberEntryField(fEventFrame))

  , fJumpFrame(new TGHorizontalFrame(fMotherFrame, 200, 20, kFixedWidth))
  , fButtonJumpEvents(new TGTextButton(fJumpFrame, Translation.ButtonJumpNEvents()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1);

  fQuickAnalysisFrame->AddFrame(fButtonQuickAnalysis, HintExpandX);
  fButtonQuickAnalysis->AllowStayDown(true);
  fMotherFrame->AddFrame(fQuickAnalysisFrame, HintExpandX);

  fEventFrame->AddFrame(fLoopNumberLabel, HintExpandX);
  fEventFrame->AddFrame(fNLoop, HintExpandX);
  fNLoop->SetEnabled(kFALSE);
  fMotherFrame->AddFrame(fEventFrame, HintExpandX);

  fButtonJumpEvents->SetEnabled(kFALSE);
  fJumpFrame->AddFrame(fButtonJumpEvents, HintExpandX);
  fMotherFrame->AddFrame(fJumpFrame, HintExpandX);

  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsCenterY));
}
TParticleInfo::TParticleInfo(TGCompositeFrame *parent, const TGUIEnglish &Translation)
  : TGGroupFrame(parent, "Particle Info", kVerticalFrame)
{
  std::vector<Int_t> particleIDs{11, 443};
  std::vector<TString> particleNames{"e^{-}, e^{+}", "J/#psi"};

  TDatabasePDG* pdgDb = TDatabasePDG::Instance();

  Double_t fontSize = 0.8/particleIDs.size();

  TLatex latex;
  latex.SetTextFont(42);
  latex.SetTextAlign(kHAlignLeft +kVAlignBottom);

  auto *frame = new TGHorizontalFrame(this);

  auto *left = new TGLayoutHints(kLHintsLeft | kLHintsExpandX);
  auto *right = new TGLayoutHints(kLHintsRight);
  auto *expandX = new TGLayoutHints(kLHintsExpandX);

  auto *pName = new TRootEmbeddedCanvas("", frame, 110, 30, 0);
  auto *pMass = new TRootEmbeddedCanvas("", frame, 110, 30, 0);
  frame->AddFrame(pMass, right);
  frame->AddFrame(pName, left);
  AddFrame(frame, expandX);
  pName->GetCanvas()->SetFillColor(18);
  pMass->GetCanvas()->SetFillColor(18);

  for(int i = particleIDs.size()-1; i >= 0; --i)
  {
    pName->GetCanvas()->cd();
    latex.SetTextSize(fontSize*1.2);
    latex.DrawLatex(0, fontSize*(i+0.1), "#it{" + particleNames[i] + "}");
    pMass->GetCanvas()->cd();
    latex.SetTextSize(fontSize);
    latex.DrawLatex(0, fontSize*(i+0.1), Form("%5.4f GeV/#it{c}^{2}", pdgDb->GetParticle(particleIDs[i])->Mass()));
  }

  pName->GetCanvas()->SetEditable(kFALSE);
  pMass->GetCanvas()->SetEditable(kFALSE);

  pName->GetCanvas()->Modified();
  pName->GetCanvas()->Update();
  pMass->GetCanvas()->Modified();
  pMass->GetCanvas()->Update();
}
} // namespace internal

TNavigation::TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat)
  : INavigation(Exercise, false, kFALSE)
  , fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
  , fAnalysisJpsiFrame(new internal::TAnalysis(this, fTranslation))
  , fDisplayJpsiFrame(new internal::TDisplay(this, fTranslation))
  , fHistogramJpsiFrame(new internal::THistogram(this, fTranslation))
  , fQuickAnalysisJpsiFrame(new internal::TQuickAnalysis(this, fTranslation))
  , fParticleInfo(new internal::TParticleInfo(this, fTranslation))
{
  auto* LayoutExpandX = new TGLayoutHints(kLHintsExpandX);

  AddFrame(fAnalysisJpsiFrame, LayoutExpandX);
  AddFrame(fDisplayJpsiFrame, LayoutExpandX);
  AddFrame(fHistogramJpsiFrame, LayoutExpandX);
  AddFrame(fQuickAnalysisJpsiFrame, LayoutExpandX);
  AddFrame(fParticleInfo, LayoutExpandX);

  Resize();
}

void TNavigation::SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI, Utility::TEventVisualization *EventVisualization, Utility::VSDReader* Reader)
{
  fAnalysisGUI = AnalysisGUI;

  auto *disp = dynamic_cast<Jpsi::EventDisplay*>(fEventDisplay);

  disp->fTrackCuts->Connect("CutsChanged(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*)", "Jpsi::JpsiVSDReader", Reader,
    "LoadJpsiParameters(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*)");

  disp->fTrackCuts->Connect("CutsChanged(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*)", "Jpsi::EventDisplay", fEventDisplay,
    "OnTrackCutChanged(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*)");

  disp->fTrackCuts->Connect("CutsChanged(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*)", "Utility::TEventAnalyseGUI", fAnalysisGUI,
    "ChooseCurrentEvent()");

  disp->fSignalExtraction->Connect("CalculateSignal(Float_t*, Float_t*)", "Jpsi::EventDisplay", fEventDisplay, "OnExtractSignal(Float_t*, Float_t*)");

  fInstructionButtonFrame->fButtonInstructions->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                                        fAnalysisGUI, "OpenInstructions()");
  fAnalysisJpsiFrame->fButtonFillPidHistos->Connect("Clicked()", "Jpsi::EventDisplay", fEventDisplay, "FillEnergyLossHisto()");
  fAnalysisJpsiFrame->fButtonFillInvMass->Connect("Clicked()", "Jpsi::EventDisplay", fEventDisplay, "FillInvariantMassHistos()");

  Reader->Connect("ClearingTracks()", "Jpsi::EventDisplay", fEventDisplay, "TracksCleared()");
  Reader->Connect("FinishedTracks()", "Jpsi::EventDisplay", fEventDisplay, "FinishedTracks()");

  Reader->Connect("LoadedTrack(TEveTrack*,Bool_t)", "Jpsi::EventDisplay", fEventDisplay,
                   "OnTrackLoaded(TEveTrack*,Bool_t)");


  fDisplayJpsiFrame->fButtonShowTracks->Connect("Clicked()", "Utility::TEventAnalyseGUI", fAnalysisGUI,
                                                "ToggleRenderable(=Utility::ERenderables::kTracks)");
  fDisplayJpsiFrame->fButtonShowDetector->Connect("Clicked()", "Utility::TEventAnalyseGUI", fAnalysisGUI,
                                                  "ToggleRenderable(=Utility::ERenderables::kGeometry)");
  fDisplayJpsiFrame->fButtonToggleBackground->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                                      fAnalysisGUI, "ChangeBackgroundColor()");

  fHistogramJpsiFrame->fButtonClearHistogram->Connect("Clicked()", "Jpsi::EventDisplay", fEventDisplay, "ClearHisto()");

  fHistogramJpsiFrame->fButtonRestart->Connect("Clicked()", "Jpsi::EventDisplay", fEventDisplay, "ClearHisto()");
  fHistogramJpsiFrame->fButtonRestart->Connect("Clicked()", "Utility::TEventAnalyseGUI", fAnalysisGUI, "ChooseFirstEvent()");
  fHistogramJpsiFrame->fButtonRestart->Connect("Clicked()", "Jpsi::TNavigation", this, "Reset()");

  fQuickAnalysisJpsiFrame->fButtonQuickAnalysis->Connect("Clicked()", "Jpsi::TNavigation", this, "ToggleQuickAnalysis()");
  fQuickAnalysisJpsiFrame->fButtonQuickAnalysis->Connect("Clicked()", "Jpsi::EventDisplay", fEventDisplay, "ToggleQuickAnalysis()");

  fQuickAnalysisJpsiFrame->fButtonJumpEvents->Connect("Clicked()", "Jpsi::TNavigation", this, "StartFastForward()");

  InitialLoad();
}

void TNavigation::InitialLoad()
{
  fDisplayJpsiFrame->fButtonShowDetector->Toggle(kTRUE);
  fDisplayJpsiFrame->fButtonShowDetector->SetState(kButtonEngaged);
  //fDisplayJpsiFrame->fButtonShowTracks->Toggle(kTRUE);
  //fDisplayJpsiFrame->fButtonShowTracks->SetState(kButtonEngaged);
}

void TNavigation::ToggleQuickAnalysis()
{
  Bool_t quickAnalysisMode = fQuickAnalysisJpsiFrame->fButtonJumpEvents->IsEnabled();

  fAnalysisJpsiFrame->fButtonFillPidHistos->SetEnabled(quickAnalysisMode);
  fAnalysisJpsiFrame->fButtonFillInvMass->SetEnabled(quickAnalysisMode);
  fAnalysisJpsiFrame->fButtonFillInvMass->SetEnabled(quickAnalysisMode);
  fHistogramJpsiFrame->fButtonRestart->SetEnabled(quickAnalysisMode);
  fHistogramJpsiFrame->fButtonClearHistogram->SetEnabled(quickAnalysisMode);

  fQuickAnalysisJpsiFrame->fButtonJumpEvents->SetEnabled(!quickAnalysisMode);
  fQuickAnalysisJpsiFrame->fNLoop->SetEnabled(!quickAnalysisMode);

  auto *disp = dynamic_cast<Jpsi::EventDisplay*>(fEventDisplay);
  disp->fTrackCuts->ToggleFrameEnabled();
}

void TNavigation::StartFastForward()
{
  fAnalysisGUI->FastForward(fQuickAnalysisJpsiFrame->fNLoop->GetNumber());
}

void TNavigation::Reset()
{
  auto *disp = dynamic_cast<Jpsi::EventDisplay*>(fEventDisplay);
  disp->fSignalExtraction->Reset();
  disp->fInvariantMass.ClearHisto();
}
} // namespace Jpsi
