#include "InvariantMass.h"

#include <TH1.h>
#include <TMath.h>
#include <TLine.h>

namespace Jpsi {

    InvariantMass::InvariantMass()
      : fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
      , fJpsiHist(nullptr), fMinvHist(nullptr), fEeHist(nullptr), fPpHist(nullptr), fEHist(nullptr), fPHist(nullptr)
      , fL1(nullptr), fL2(nullptr)
      , fCanvas(nullptr)
    {
      // Creating the histogram for tab 4
      fJpsiHist = new TH1F(fTranslation.StatisticsJpsi(), fTranslation.DifferenceEPmEEpPP(), 60, minX, maxX);
      fJpsiHist->SetLineColor(6);
      fJpsiHist->SetMarkerColor(6);
      fJpsiHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
      fJpsiHist->GetYaxis()->SetTitle(fTranslation.Counts());
      fJpsiHist->Sumw2();

      fMinvHist = new TH1F(fTranslation.StatisticsElectronPositron(),
                           fTranslation.InvariantMassElectronPositron(), 60, minX, maxX);
      fMinvHist->SetLineColor(2);
      fMinvHist->SetMarkerColor(2);
      fMinvHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
      fMinvHist->GetYaxis()->SetTitle(fTranslation.Counts());
      fMinvHist->Sumw2();

      fEeHist = new TH1F(fTranslation.StatisticsElectronElectron(),
                         fTranslation.InvariantMassElectronElectron(), 60, minX, maxX);
      fEeHist->SetLineColor(4);
      fEeHist->SetMarkerColor(4);
      fEeHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
      fEeHist->GetYaxis()->SetTitle(fTranslation.Counts());
      fEeHist->Sumw2();

      fPpHist = new TH1F(fTranslation.StatisticsPositronPositron(),
                         fTranslation.InvariantMassPositronPositron(), 60, minX, maxX);
      fPpHist->SetLineColor(416);
      fPpHist->SetMarkerColor(416);
      fPpHist->GetXaxis()->SetTitle("m_{ee} (GeV/c^{2})");
      fPpHist->GetYaxis()->SetTitle(fTranslation.Counts());
      fPpHist->Sumw2();

      fEHist = new TH1F(fTranslation.CopyElectronElectron(),
                        fTranslation.ElectronElectronDistribution(), 60, minX, maxX);
      fEHist->Sumw2();

      fPHist = new TH1F(fTranslation.CopyPositronPositron(),
                        fTranslation.PositronPositronDistribution(), 60, minX, maxX);
      fPHist->Sumw2();

      ClearData();
    }

    void InvariantMass::Setup() {
      fCanvas = new TCanvas();
      fCanvas->Divide(2, 2);
      Update();
    }

    template <typename H1, typename H2>
    void FillHistogram(Int_t n1, Int_t n2, Float_t Particle[100][4], H1* Hist1, H2* Hist2)
    {
      for (int i = 0; i < n1 - 1; i++) {
        for (int j = i + 1; j < n2; j++) {
          Float_t mass =
            TMath::Sqrt((Particle[i][0] + Particle[j][0]) * (Particle[i][0] + Particle[j][0]) -
                        (Particle[i][1] + Particle[j][1]) * (Particle[i][1] + Particle[j][1]) -
                        (Particle[i][2] + Particle[j][2]) * (Particle[i][2] + Particle[j][2]) -
                        (Particle[i][3] + Particle[j][3]) * (Particle[i][3] + Particle[j][3]));
          Hist1->Fill(mass);
          Hist2->Fill(mass);
        }
      }
    }

    void InvariantMass::Fill()
    {
      //nEvents++;
      FillHistogram(fNElectrons, fNElectrons, fEl, fEeHist, fEHist);
      FillHistogram(fNPositrons, fNPositrons, fPo, fPpHist, fPHist);

      for(int i = 0; i < fNElectrons; i++)
      {
        for(int j = 0; j < fNPositrons; j++)
        {
          Float_t mass =
            TMath::Sqrt((fEl[i][0]+fPo[j][0])*(fEl[i][0]+fPo[j][0]) -
                        (fEl[i][1]+fPo[j][1])*(fEl[i][1]+fPo[j][1]) -
                        (fEl[i][2]+fPo[j][2])*(fEl[i][2]+fPo[j][2]) -
                        (fEl[i][3]+fPo[j][3])*(fEl[i][3]+fPo[j][3]));
          fMinvHist->Fill(mass);
          fJpsiHist->Fill(mass);
        }
      }
      //FillHistogram(fNElectrons, fNPositrons, fEl, fMinvHist, fJpsiHist);

      fCanvas->cd(1);
      Float_t yMax = fMinvHist->GetBinContent(fMinvHist->GetMaximumBin());
      yMax += sqrt(yMax);
      yMax *= 1.05;
      fMinvHist->GetYaxis()->SetRangeUser(0, yMax);
      fMinvHist->Draw();

      fCanvas->cd(2);
      fJpsiHist->Draw();

      fCanvas->cd(3);
      fEeHist->GetYaxis()->SetRangeUser(0, yMax);
      fEeHist->Draw();

      fCanvas->cd(4);
      fPpHist->GetYaxis()->SetRangeUser(0, yMax);
      fPpHist->Draw();

      fJpsiHist->Add(fEHist, -1);
      fJpsiHist->Add(fPHist, -1);
    }

    void InvariantMass::Update()
    {
      fCanvas->cd(1);
      fMinvHist->Draw("ep");

      fCanvas->cd(2);
      fJpsiHist->Draw("ep");

      if(fL1 != nullptr)
      {
        fL1->Draw();
        fL2->Draw();
      }

      fCanvas->cd(3);
      fEeHist->Draw("ep");

      fCanvas->cd(4);
      fPpHist->Draw("ep");

      fCanvas->Modified();
      fCanvas->Update();
    }

    void InvariantMass::AddPositron(Float_t energy, Float_t px, Float_t py, Float_t pz)
    {
      fPo[fNPositrons][0] = energy;
      fPo[fNPositrons][1] = px;
      fPo[fNPositrons][2] = py;
      fPo[fNPositrons][3] = pz;
      fNPositrons++;
    }

    void InvariantMass::AddElectron(Float_t energy, Float_t px, Float_t py, Float_t pz)
    {
      fEl[fNElectrons][0] = energy;
      fEl[fNElectrons][1] = px;
      fEl[fNElectrons][2] = py;
      fEl[fNElectrons][3] = pz;
      fNElectrons++;
    }

    void InvariantMass::ClearData() {
      for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 4; j++) {
          fEl[i][j] = 0;
          fPo[i][j] = 0;
        }
      }
      fNPositrons = 0;
      fNElectrons = 0;

      ClearTemp();
    }

    void InvariantMass::ClearHisto() {
      fCanvas->cd(1);
      fMinvHist->Reset();
      fMinvHist->Draw();

      fCanvas->cd(2);
      fJpsiHist->Reset();
      fJpsiHist->Draw();

      fCanvas->cd(3);
      fEeHist->Reset();
      fEeHist->Draw();

      fCanvas->cd(4);
      fPpHist->Reset();
      fPpHist->Draw();

      fCanvas->Modified();
      fCanvas->Update();
    }

    void InvariantMass::ClearTemp() {
      fEHist->Reset();
      fPHist->Reset();
    }

    void InvariantMass::CalculateIntegral(Float_t minm, Float_t maxm, Float_t& numJpsi, Float_t& signalBackground, Float_t& significance)
    {
        Float_t epsilon = 0.01;
        Int_t massBin1 = fJpsiHist->FindBin(minm + epsilon);
        Int_t massBin2 = fJpsiHist->FindBin(maxm - epsilon);

        numJpsi = fJpsiHist->Integral(massBin1, massBin2);
        Float_t bg = (fEeHist->Integral(massBin1, massBin2)) + (fPpHist->Integral(massBin1, massBin2));

        if (numJpsi + bg > 0.) {
            significance = numJpsi / (TMath::Sqrt(numJpsi + bg));
        }

        if (bg > 0.0) {
            signalBackground = 1. * numJpsi / bg;
        } else if (numJpsi > 0.0) {
            signalBackground = 9999.;
        }
    }

    void InvariantMass::SetMassLineValues(Bool_t enabled, Float_t minm, Float_t maxm)
    {
        delete fL1;
        delete fL2;

        if(enabled) {
          fL1 = new TLine;
          fL2 = new TLine;
          fL1->SetX1(minm);
          fL1->SetY1(-100);
          fL1->SetX2(minm);
          fL1->SetY2(100);
          fL1->SetLineColor(1);
          fL1->SetLineStyle(5);

          fL2->SetX1(maxm);
          fL2->SetY1(-100);
          fL2->SetX2(maxm);
          fL2->SetY2(100);
          fL2->SetLineColor(1);
          fL2->SetLineStyle(5);
        } else {
          fL1 = fL2 = nullptr;
        }
    }

}
