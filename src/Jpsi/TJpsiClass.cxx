#include "Jpsi/TJpsiClass.h"

#include <TApplication.h>
#include <TEveBrowser.h>
#include <TEveEventManager.h>
#include <TEveManager.h>
#include <TEveVSD.h>
#include <TEveViewer.h>
#include <TEveWindowManager.h>
#include <TFile.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGLRnrCtx.h>
#include <TGLViewer.h>
#include <TGLabel.h>
#include <TGLayout.h>
#include <TGTab.h>
#include <TRootHelpDialog.h>

#include "Jpsi/GUITranslation.h"
#include "Jpsi/EventDisplay.h"
#include "Jpsi/TNavigation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/TEventAnalyseGUI.h"
#include "Utility/Utilities.h"

using namespace std;

namespace Jpsi
{
    JpsiDataset::JpsiDataset(Bool_t allowAuto)
          : Dataset(allowAuto, kTRUE)
          , fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
    {
      setInstructions(fTranslation.DataSetInstruction());
    }

//    std::unique_ptr<Utility::EventDisplay> JpsiDataset::createEventDisplay(Bool_t /*demo*/)
//    {
//
//    }
//    std::unique_ptr<Utility::INavigation> JpsiDataset::createNavigation(Utility::EventDisplay* exercise, Bool_t cheat)
//    {
//
//    }

    void JpsiDataset::Start()
    {
      Utility::SetupStyleForClass();

      TString vsd_file_name = Form("events_%i.root", fDataset);

      TString gf = Utility::ResourcePath("data/Utility/geometry.root");

      UnmapWindow();

      auto* r = new Utility::TEventAnalyseGUI(gf);
      std::unique_ptr<Utility::EventDisplay> Exercise = std_fix::make_unique<Jpsi::EventDisplay>();
      std::unique_ptr<Utility::INavigation> Navigation = std_fix::make_unique<Jpsi::TNavigation>(Exercise.get(), kFALSE);
      r->Setup(std::move(Navigation), std::move(Exercise), "J/psi", vsd_file_name, kFALSE);
      r->SetupSignalSlots();
    }

    void JpsiDataset::fillSelectionBox()
    {
      fSelectionCombo->AddEntry(fTranslation.CollisionPP(), 1);
      fSelectionCombo->AddEntry(fTranslation.CollisionPPb(), 2);
    }

//______________________________________________________________________________
//void alice_vsd(Int_t dataset)
//{
//  TString vsd_file_name;
//
//  vsd_file_name = Utility::ResourcePath(Form("vsdData/Jpsi/events_%i.root", dataset));
//
//  // Main function, initializes the application.
//  //
//  // 1. Load the auto-generated library holding ESD classes and ESD dictionaries.
//  // 2. Open ESD data-files.
//  // 3. Load cartoon geometry.
//  // 4. Spawn simple GUI.
//  // 5. Load first event.
//
//  TFile::SetCacheFileDir(".");
//
//  TEveVSD::DisableTObjectStreamersForVSDStruct();
//
//  TEveManager::Create(kTRUE, "FV");
//
//  // Final stuff
//  //=============
//
//  gEve->GetDefaultGLViewer()->SetStyle(TGLRnrCtx::kOutline);
//  auto* gVSDReader = new TVSDReader(vsd_file_name);
//  gVSDReader->GeometryDefault();
//
//  gEve->GetViewers()->SwitchColorSet();
//
//  TEveBrowser* browser = gEve->GetBrowser();
//
//  browser->GetTabLeft()->RemoveTab(1);
//  browser->GetTabLeft()->RemoveTab(0);
//
//  Utility::SetupStyleForClass();
//
//  gEve->AddEvent(new TEveEventManager("Event", "ALICE VSD Event"));
//  gVSDReader->StudentSet();
//  gVSDReader->GotoEvent(0);
//
//  gEve->GetWindowManager()->HideAllEveDecorations();
//  gEve->Redraw3D(kTRUE); // Reset camera after the first event has been shown.
//}
//
//MasterClassFrame::MasterClassFrame()
//{
//  const TGUIEnglish& Translation = Utility::TranslationFromEnv<TJPsiLanguage>();
//  auto* hf = new TGVerticalFrame(this);
//  {
//    auto* label = new TGLabel(hf, Translation.DataSelection());
//    hf->AddFrame(label, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
//
//    auto* b1 = new TGPictureButton(
//      hf, gClient->GetPicture(Utility::ResourcePath("doc/Utility/ALICE_logo.png")));
//    hf->AddFrame(b1, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
//
//    fStudent = new TGTextButton(hf, Translation.ButtonStartAnalysis());
//    hf->AddFrame(fStudent, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5));
//    fStudent->Connect("Clicked()", "Jpsi::MasterClassFrame", this, "Start()");
//
//    fDataset = new TGComboBox(hf);
//    fDataset->AddEntry(Translation.CollisionPP(), 1);
//    fDataset->AddEntry(Translation.CollisionPPb(), 2);
//
//    fDataset->Resize(200, 20);
//    fDataset->Select(1, kFALSE);
//    hf->AddFrame(fDataset, new TGLayoutHints(kLHintsExpandX));
//
//    fExit = new TGTextButton(hf, Translation.ButtonExit());
//    hf->AddFrame(fExit, new TGLayoutHints(kLHintsExpandX, 5, 5, 10, 10));
//    fExit->Connect("Clicked()", "TApplication", gApplication, "Terminate()");
//  }
//
//  AddFrame(hf);
//
//  SetWindowName(Translation.MasterclassTitle());
//  MapSubwindows();
//
//  Resize(GetDefaultSize());
//
//  MapWindow();
//}
//
//void MasterClassFrame::Start()
//{
//  alice_vsd(fDataset->GetSelected());
//  UnmapWindow();
//}
//
//void TJpsiClass::RunExercise(Bool_t /*test*/)
//{
//  const TGUIEnglish& Translation = Utility::TranslationFromEnv<TJPsiLanguage>();
//  new MasterClassFrame();
//  auto* instructions =
//    new TRootHelpDialog(gClient->GetRoot(), Translation.MasterclassInstructionsTitle(), 700, 250);
//  instructions->SetText(Translation.MasterclassInstructionsText());
//  instructions->Popup();
//}
void TJpsiClass::RunExercise(Bool_t AllowAuto)
{
  (new JpsiDataset(AllowAuto))->Init();
}
} // namespace Jpsi
