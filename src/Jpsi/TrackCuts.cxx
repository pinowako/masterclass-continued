//
// Created by user on 7/4/19.
//

#include "TrackCuts.h"

#include <Utility/Instructions.h>
#include <TRootHelpDialog.h>
#include "EventDisplay.h"
#include "ParticleIdentification.h"

namespace Jpsi {

TrackCuts::TrackCuts(const TGWindow *p, UInt_t w, UInt_t h)
    : TGMainFrame(p, w, h, kFixedHeight | kHorizontalFrame)
    , fTranslation(Utility::TranslationFromEnv<TJPsiLanguage>())
    , fP1F(nullptr), fP2F(nullptr), fD1F(nullptr), fD2F(nullptr)
    , fApply(nullptr)
    , fIsEngaged(kFALSE)
{
  DontCallClose();

  auto* lh = new TGLayoutHints(kLHintsExpandX);

  // Make Left hand side for particles
  auto *gf = new TGGroupFrame(this, fTranslation.SelectorTitle(), kFixedWidth | kVerticalFrame);
  auto *vf = new TGVerticalFrame(gf);

  // Make header frames
  auto *hf = new TGHorizontalFrame(vf);

  auto *val1 = new TGLabel(hf, "    ");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  val1 = new TGLabel(hf, "min");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));

  val1 = new TGLabel(hf, "max");
  val1->SetTextJustify(kTextCenterX);
  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  vf->AddFrame(hf, lh);

  hf = new TGHorizontalFrame(vf);

  val1 = new TGLabel(hf, "p");
  fP1F = new TGNumberEntryField(hf);
  fP1F->SetNumber(0.);
  fP2F = new TGNumberEntryField(hf);
  fP2F->SetNumber(0.);

  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fP1F, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fP2F, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  vf->AddFrame(hf, lh);

  hf = new TGHorizontalFrame(vf);

  val1 = new TGLabel(hf, "dE/dx");
  fD1F = new TGNumberEntryField(hf);
  fD1F->SetNumber(0.);
  fD2F = new TGNumberEntryField(hf);
  fD2F->SetNumber(0.);

  hf->AddFrame(val1, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fD1F, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  hf->AddFrame(fD2F, new TGLayoutHints(kLHintsExpandX, 1, 1, 1, 1));
  vf->AddFrame(hf, lh);

  gf->AddFrame(vf, lh);
  gf->Resize(300, 150);
  AddFrame(gf, new TGLayoutHints(kLHintsLeft | kLHintsExpandY, 4, 2, 2, 4));

  lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 3, 3);

  // Make operations
  gf = new TGGroupFrame(this, "Operations", kVerticalFrame);

  // --- Instructions button ---------------------------------------
  auto* InstButton = Utility::MakeInstructionsButton(gf);
  InstButton->Connect("Clicked()", "Jpsi::TrackCuts", this, "Instructions()");

  auto* particleId = new TGTextButton(gf, fTranslation.DetectorInfoTitle());
  particleId->Connect("Clicked()", "Jpsi::TrackCuts", this, "ParticleId()");
  gf->AddFrame(particleId, new TGLayoutHints(kLHintsExpandX));

  fApply = new TGTextButton(gf, fTranslation.ButtonApplyCuts());
  fApply->AllowStayDown(kTRUE);
  fApply->Connect("Clicked()", "Jpsi::TrackCuts", this, "ToggleCuts()");
  gf->AddFrame(fApply, new TGLayoutHints(kLHintsExpandX | kLHintsBottom));

  AddFrame(gf, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 2, 4, 2, 4));

  MapSubwindows();
  TGDimension d = GetDefaultSize();
  Resize(d.fWidth, 400);
  MapWindow();
}

void TrackCuts::LockInputs(Bool_t lock)
{
  fP1F->SetEnabled(lock);
  fP2F->SetEnabled(lock);
  fD1F->SetEnabled(lock);
  fD2F->SetEnabled(lock);
}

void TrackCuts::ToggleCuts()
{
  fIsEngaged = !fIsEngaged;

  Float_t arr[4];

  arr[0] = arr[1] = arr[2] = arr[3] = 0;
  if(fIsEngaged) {
    arr[0] = Utility::Clamp(fP1F->GetNumber(), ParticleIdentification::minX, ParticleIdentification::maxX);
    arr[1] = Utility::Clamp(fP2F->GetNumber(), ParticleIdentification::minX, ParticleIdentification::maxX);
    arr[2] = Utility::Clamp(fD1F->GetNumber(), ParticleIdentification::minY, ParticleIdentification::maxY);
    arr[3] = Utility::Clamp(fD2F->GetNumber(), ParticleIdentification::minY, ParticleIdentification::maxY);

    fP1F->SetNumber(arr[0]);
    fP2F->SetNumber(arr[1]);
    fD1F->SetNumber(arr[2]);
    fD2F->SetNumber(arr[3]);
  }

  Long_t args[5];
  args[0] = fIsEngaged;
  args[1] = (Long_t)&arr[0];
  args[2] = (Long_t)&arr[1];
  args[3] = (Long_t)&arr[2];
  args[4] = (Long_t)&arr[3];

  Emit("CutsChanged(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*)", args);

  LockInputs(!fIsEngaged);
}

void TrackCuts::Instructions()
{
    auto* instructions =
      new TRootHelpDialog(gClient->GetRoot(), fTranslation.SelectorInstructionsTitle(), 700, 400);
    instructions->SetText(fTranslation.SelectorInstructionsText());

    instructions->Popup();
}

void TrackCuts::ParticleId()
{
    new Utility::Details("dEdx", "Jpsi", fTranslation.DetectorInfoTitle(), gClient->GetRoot(), 100, 100);
}

void TrackCuts::Reset()
{
  if(fIsEngaged)
  {
    ToggleCuts();
    fApply->SetState(kButtonUp, kTRUE);
  }

  fP1F->SetNumber(0);
  fP2F->SetNumber(0);
  fD1F->SetNumber(0);
  fD2F->SetNumber(0);
}

void TrackCuts::ToggleFrameEnabled()
{
  Bool_t current_state = fApply->IsEnabled();

  if(current_state)
  {
    LockInputs(kFALSE);
  } else {
    LockInputs(!fIsEngaged);
  }

  fApply->SetEnabled(!current_state);
  fApply->SetDown(fIsEngaged);
}
}
