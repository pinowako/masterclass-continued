#ifndef MASTERCLASSES_EVENTDISPLAY_H
#define MASTERCLASSES_EVENTDISPLAY_H

#include <Utility/EventDisplay.h>
#include <Utility/VSDReader.h>
#include "Jpsi/GUITranslation.h"
#include "Jpsi/TrackCuts.h"
#include "Jpsi/SignalExtraction.h"
#include "Jpsi/ParticleIdentification.h"
#include "Jpsi/InvariantMass.h"

namespace Jpsi {
    class EventDisplay : public Utility::EventDisplay {
    private:
        const TGUIEnglish& fTranslation;
        Bool_t fEnabledQuickAnalysis;
    public:
        TrackCuts* fTrackCuts;
        SignalExtraction* fSignalExtraction;
        ParticleIdentification fParticleIdentification;
        InvariantMass fInvariantMass;
        /// Default initialize all histograms and retrieve the translation.
        EventDisplay();

        /// Add our elements to the EVE browser
        /// @param browser Browser implementation to use
        /// @param cheat   True if cheats are on
        void Setup(TRootBrowser* browser, Bool_t /*unused*/) override;

        /// Check what kind of event we have
        /// @param evNo Event number in file
        /// @return Description of the event
        TString NewEvent(Int_t evNo) override;

        /// Get the current magnetic field, either 0.0T or 0.5T
        Double_t GetMagneticField() const override;

        std::unique_ptr<Utility::VSDReader> CreateReader() override;

        /// Publish the count to the histograms, and update calculations.
        void EventDone() override;

        /// Return a HTML styled string that explains what to do.
        const TString& Instructions() const override { return fTranslation.PathEventDisplay(); }

        void OnTrackCutChanged(Bool_t, Float_t*, Float_t*, Float_t*, Float_t*);
        void OnExtractSignal(Float_t*, Float_t*);

        void OnTrackLoaded(TEveTrack *track, Bool_t included);

        void ToggleQuickAnalysis();

        /**
 * Return the path to data relative to the top-level directory
 *
 * @return Path to data, relative to top-level directory
 */
        TString DataDir() const override { return Utility::ResourcePath("vsdData/Jpsi"); };

        void TracksCleared();
        void FinishedTracks();
        void ClearHisto();
        void FillEnergyLossHisto();
        void FillInvariantMassHistos();

    ClassDef(EventDisplay, 0);
    };
}


#endif //MASTERCLASSES_EVENTDISPLAY_H
