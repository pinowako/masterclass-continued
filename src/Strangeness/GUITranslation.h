#ifndef STRANGEGUITRANSLATION_C_JRWVEQB7
#define STRANGEGUITRANSLATION_C_JRWVEQB7

#include <iostream>
#include <memory>

#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"  // IWYUU pragma: keep
#include "Utility/Utilities.h" // IWYU pragma: keep

namespace Strangeness
{
struct TGUIEnglish : Utility::TLanguageProvider {
  TGUIEnglish(Utility::ESupportedLanguages lang = Utility::English)
    : TLanguageProvider(lang)
  {
    std::cerr << "Registering English StrangeClass GUI\n";
#include "Strangeness/keys_gui_trans.txt.en"

    RegisterText("DocDataSet",
#include "Strangeness/Dataset.en.html"
    );
    RegisterText("FileCalculator",
#include "Strangeness/Calculator.en.html"
    );
    RegisterText("FileEventDisplay",
#include "Strangeness/EventDisplay.en.html"
    );
  }

  LANG_KEY(All)
  LANG_KEY(Accept)
  LANG_KEY(Background)
  LANG_KEY(Calculator)
  LANG_KEY(Collected)
  LANG_KEY(Control)
  LANG_KEY(Count)
  LANG_KEY(Counts)
  LANG_KEY(Choice)
  LANG_KEY(Clear)
  LANG_KEY(DataSet)
  LANG_KEY(DemoSet)
  LANG_KEY(Entries)
  LANG_KEY(EventCharacteristics)
  LANG_KEY(Exit)
  LANG_KEY(Final)
  LANG_KEY(Fit)
  LANG_KEY(Fitter)
  LANG_KEY(FitRange)
  LANG_KEY(InvMass)
  LANG_KEY(Histograms)
  LANG_KEY(MassAWidth)
  LANG_KEY(Measurement)
  LANG_KEY(Measurements)
  LANG_KEY(Mean)
  LANG_KEY(Operations)
  LANG_KEY(Ratio2pp)
  LANG_KEY(RawMeasurements)
  LANG_KEY(SelectDataSet)
  LANG_KEY(SelectParticleType)
  LANG_KEY(Signal)
  LANG_KEY(SignalLimits)
  LANG_KEY(Start)
  LANG_KEY(StrangenessCollected)
  LANG_KEY(StrangenessEnhancement)
  LANG_KEY(StrangenessSuppression)
  LANG_KEY(Submit)
  LANG_KEY(Summary)
  LANG_KEY(Total)
  LANG_KEY(Particles)
  LANG_KEY(KeyYield)
  LANG_KEY(KeyWhich)

  LANG_KEY(DiagFailedOpenFileText)
  LANG_KEY(DiagFailedOpenFileTitle)
  LANG_KEY(DiagIncompleteInputText)
  LANG_KEY(DiagIncompleteInputTitle)
  LANG_KEY(DiagInvalidInputText)
  LANG_KEY(DiagInvalidInputTitle)
  LANG_KEY(DiagMoreDataText)
  LANG_KEY(DiagMoreDataTitle)

  LANG_KEY(DocDataSet)

  LANG_KEY(FileCalculator)
  LANG_KEY(FileEventDisplay)

  LANG_KEY(Undo)
  LANG_KEY(ClearHisto)
};

LANG_TRANSLATION(TGUI, German)
{
  std::cerr << "Registering German StrangeClass GUI\n";
#include "Strangeness/keys_gui_trans.txt.de"

  RegisterText("DocDataSet",
#include "Strangeness/Dataset.de.html"
  );
  RegisterText("FileCalculator",
#include "Strangeness/Calculator.de.html"
  );
  RegisterText("FileEventDisplay",
#include "Strangeness/EventDisplay.de.html"
  );
}

struct ClassLanguage {
  using ReturnType = TGUIEnglish;

  static std::unique_ptr<ReturnType> create(Utility::ESupportedLanguages lang)
  {
    switch (lang) {
      case Utility::English:
        return std_fix::make_unique<TGUIEnglish>();
      case Utility::German:
        return std_fix::make_unique<TGUIGerman>();
      case Utility::NLanguages:
        break;
    }
    throw std::runtime_error("Unsupported language requested for Strangeness!");
  }
};
} // namespace Strangeness

#endif /* end of include guard: STRANGEGUITRANSLATION_C_JRWVEQB7 */
