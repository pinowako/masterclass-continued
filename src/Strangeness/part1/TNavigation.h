#ifndef TNAVIGATION_H_CXEE3LYW
#define TNAVIGATION_H_CXEE3LYW

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>

#include "Utility/GUITranslation.h"
#include "Utility/INavigation.h"

class TBuffer;
class TClass;
class TGButton;
class TGTextButton;
class TMemberInspector;
namespace Utility
{
class EventDisplay;
class TEventAnalyseGUI;
class TEventVisualization;
class VSDReader;
} // namespace Utility

namespace Strangeness
{
namespace internal
{
struct TDisplay : TGGroupFrame {
  TGTextButton* fButtonTracks;
  TGHorizontalFrame* fFrameStrangeParticles;
  TGTextButton* fButtonV0;
  TGTextButton* fButtonCascade;
  TGHorizontalFrame* fFrameGeometry;
  TGTextButton* fToggleDetector;
  TGTextButton* fButtonAxes;
  TGTextButton* fButtonChangeBackground;

  TDisplay(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation);

  ClassDef(TDisplay, 0);
};

struct THelp : TGGroupFrame {
  TGTextButton* fButtonAliceInfo;
  TGTextButton* fButtonDecayPatterns;

  THelp(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation);
  ClassDef(THelp, 0);
};

struct TParticleInfo : TGGroupFrame {
  TParticleInfo(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation);
  ClassDef(TParticleInfo, 0);
};

} // namespace internal

class TNavigation : public Utility::INavigation
{
 private:
  // Same for Strangeness, Optional
  internal::TDisplay* fDisplayStrangeFrame;

  // Frame for help, differs between Raa and Strangeness, only one of two will be created
  internal::THelp* fHelpStrange;

  internal::TParticleInfo* fParticleInfo;

 protected:
  void InitialLoad() override;

 public:
  TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat);
  void SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI, Utility::TEventVisualization *EventVisualization,
                        Utility::VSDReader* Reader) override;

  /// Show the decay pattern information
  void PatternInfo();

  ClassDefOverride(TNavigation, 0);
};
} // namespace Strangeness

#endif /* end of include guard: TNAVIGATION_H_CXEE3LYW */
