/**
 * @file   StrangeCalculator.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar 15 19:38:40 2017
 *
 * @brief  Calculate the invariant mass
 *
 *
 * @ingroup alice_masterclass_str_part1
 */

#ifndef STRANGECALCULATOR_C
#define STRANGECALCULATOR_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TDatabasePDG.h>
#include <TEveTrack.h>
#include <TEveVector.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TMath.h>
#include <TParticlePDG.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part1/Histograms.h"
#include "Utility/Instructions.h"

class TBuffer;
class TClass;
class TEveTrack;
class TGComboBox;
class TGWindow;
class TMemberInspector;

namespace Strangeness
{
struct Histograms;
struct TGUIEnglish;

/**
 * Tools for calculating invariant mass
 *
 * @ingroup alice_masterclass_str_part1
 */
namespace Tools
{
Double_t E(Double_t m, Double_t px, Double_t py, Double_t pz);
Double_t E(Double_t m, const TEveVector& p);
Double_t M(Int_t pdg);
Double_t InvMass(Double_t m1, Double_t px1, Double_t py1, Double_t pz1, Double_t m2, Double_t px2,
                 Double_t py2, Double_t pz2, Double_t m3, Double_t px3, Double_t py3, Double_t pz3);
Double_t InvMass(Double_t m1, const TEveVector& p1, Double_t m2, const TEveVector& p2, Double_t m3,
                 const TEveVector& p3);
Double_t InvMass(Int_t i1, const TEveVector& p1, Int_t i2, const TEveVector& p2, Int_t i3,
                 const TEveVector& p3);
} // namespace Tools

/**
 * A row for a single particle
 *
 * @ingroup alice_masterclass_str_part1
 */
class Particle : public TGHorizontalFrame
{
 public:
  TGNumberEntryField* fPx;
  TGNumberEntryField* fPy;
  TGNumberEntryField* fPz;
  TGNumberEntryField* fM;

  Particle(TGCompositeFrame* p, const char* type, Pixel_t color);

  void Reset();
  void Set(Double_t px, Double_t py, Double_t pz, Double_t m);
  Bool_t IsEmpty() const { return (Px() + Py() + Pz()) == 0; };

  Double_t Px() const { return fPx->GetNumber(); }
  Double_t Py() const { return fPy->GetNumber(); }
  Double_t Pz() const { return fPz->GetNumber(); }
  Double_t M() const { return fM->GetNumber(); }
  ClassDef(Particle, 0);
};

/**
 * Calculate invariant mass
 * @ingroup alice_masterclass_str_part1
 */
class Calculator : public TGMainFrame
{
 public:
  Particle* fN;
  Particle* fP;
  Particle* fB;
  TGNumberEntryField* fI;
  TGComboBox* fT;
  Histograms* fH;
  TGTextButton* fUndo;
  TGTextButton* fSubmit;
  const TGUIEnglish& fTranslation;

  Calculator(Histograms* hists, const TGWindow* p, UInt_t w, UInt_t h);

  void Set(Particle* p, TEveTrack* t);
  void SetN(TEveTrack* t);
  void SetP(TEveTrack* t);
  void SetB(TEveTrack* t);
  void Calc();
  void Auto();
  void Take();
  void Undo();
  void ClearHisto();
  void Reset();
  void Instructions();
  void CheckSubmit();
  ClassDef(Calculator, 0);
};
} // namespace Strangeness
#endif
