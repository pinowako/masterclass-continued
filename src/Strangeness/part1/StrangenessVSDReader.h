#ifndef MASTERCLASSES_STRANGENESSVSDREADER_H
#define MASTERCLASSES_STRANGENESSVSDREADER_H

#include "Utility/VSDReader.h"

namespace Strangeness {
    class StrangenessVSDReader : public Utility::VSDReader
    {
    public:
        Bool_t handleTrack(TEveTrack*) override;
    };
}


#endif //MASTERCLASSES_STRANGENESSVSDREADER_H
