/**
 * @file   StrangeEventDisplay.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Wed Mar  8 10:22:41 2017
 *
 * @brief  Strangeness event display
 *
 * @ingroup alice_masterclass_str_part1
 */
#ifndef STRANGEEVENTDISPLAY_C
#define STRANGEEVENTDISPLAY_C

#include <RtypesCore.h>
#include <TDatabasePDG.h>
#include <TEveTrack.h>
#include <TEveVector.h>
#include <TGTab.h>
#include <TRootBrowser.h>
#include <TString.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part1/Calculator.h"
#include "Strangeness/part1/Histograms.h"
#include "Utility/EventDisplay.h"

class TEveTrack;
class TH1;
class TRootBrowser;

namespace Strangeness
{
class Calculator;
} // namespace Strangeness

namespace Strangeness
{
/**
 * This defines the interface for eventdisplays
 *
 * @image html Strangeness/doc/EventDisplay.png
 *
 * @ingroup alice_masterclass_str_part1
 */
class EventDisplay : public Utility::EventDisplay
{
 private:
  // The following variables are internals to cache tracks. Necessary
  // because Signal/Slot only allows single argument signals.
  // Signal/Slots are used to react to loaded data from VSDReader.
  TEveTrack* fV0Negative{ nullptr };
  TEveTrack* fV0Positive{ nullptr };

  TEveTrack* fCascadeNegative{ nullptr };
  TEveTrack* fCascadePositive{ nullptr };
  TEveTrack* fCascadeBachelor{ nullptr };

 public:
  Histograms fHistograms;
  Calculator* fCalculator{};
  TH1* fInvMassK;
  TH1* fInvMassXi;
  TH1* fInvMassLambda;
  TH1* fInvMassAntiLambda;
  Bool_t fIsDemo;
  Bool_t fAutoGuess;

  const TGUIEnglish& fTranslation;

  EventDisplay(Bool_t isDemo);

  /**
   * Set-up the elements needed by the exercise - e.g., GUI elements
   * or the like
   *
   * @param browser The browser we're using
   * @param cheat   Set-up for cheating
   */
  void Setup(TRootBrowser* browser, Bool_t cheat) override;

  /**
   * Called after reading an event.  Should reset what is needed.
   *
   * @param evNo The current event number
   *
   * @return Should return a text string describing the event
   */
  TString NewEvent(Int_t evNo) override;

  /**
   * Called when the student finish with an event.
   */
  void EventDone() override {}

  /// Clear the previous data when the V0s change.
  void ClearV0s();
  /// Callback when the negative V0 is loaded.
  void V0NegativeLoaded(TEveTrack* Track);
  /// Callback when the positive V0 is loaded.
  void V0PositiveLoaded(TEveTrack* Track);
  /// Callback to signal that the full V0 is loaded (Signal/Slot allows only
  /// one argument... FIXME Evaluate if callback would be better.
  /// Called when a v0 is loaded. We calculate the invariant mass and
  /// store in private histogram.  This histogram we can then add to
  /// the output histograms in case of auto-analysis.
  void V0Loaded();
  void FinishedV0s();

  /// Clear the Cascades analysis e.g. when the data changes.
  void ClearCascades() { fInvMassXi->Reset(); }
  /// Callback when negative cascade is loaded.
  void CascadeNegativeLoaded(TEveTrack* Track);
  /// Callback when positive cascade is loaded.
  void CascadePositiveLoaded(TEveTrack* Track);
  /// Callback when bachelor cascade is loaded.
  void CascadeBachelorLoaded(TEveTrack* Track);
  /// Callback when whole cascade is loaded. FIXME Evaluate classic callback
  /// Called when a cascade is loaded. We calculate the invariant mass
  /// and store in private histogram.  This histogram we can then add
  /// to the output histograms in case of auto-analysis.
  void CascadeLoaded();
  void FinishedCascades();

  /// Callback when an interesting track has been selected.
  void TrackSelected(TEveTrack* Track);

  /**
   * Toggle cheats
   * @param on if true, cheats are on
   */
  void ToggleCheat(Bool_t on) override { fAutoGuess = on; }

  /**
   * Automatically process an event
   */
  void AutoEvent() override;

  const TString& Instructions() const override { return fTranslation.FileEventDisplay(); }

  std::unique_ptr<Utility::VSDReader> CreateReader() override;

  /**
   * Stuff to initially load
   */
  UInt_t InitialLoad() const override
  {
    return static_cast<UInt_t>(Utility::ERenderables::kTracks) |
           static_cast<UInt_t>(Utility::ERenderables::kGeometry) |
           static_cast<UInt_t>(Utility::ERenderables::kCascades) |
           static_cast<UInt_t>(Utility::ERenderables::kV0s);
  }

  /**
   * We can save
   *
   * @return true
   */
  Bool_t CanPrint() const override { return true; }

  /**
   * We can save
   *
   * @return true
   */
  Bool_t CanSave() const override { return true; }

  /**
   * Export to PDF
   */
  void PrintPdf(const char* out) override { fHistograms.PrintPdf(out); }

  /// Export to ROOT
  void Export(const TString& out) override { fHistograms.Export(out); }

  TString DataDir() const override { return Utility::ResourcePath("vsdData/Strangeness"); }
};
} // namespace Strangeness
#endif
