#include "TNavigation.h"

#include <TGButton.h>
#include <TGClient.h>
#include <TGLayout.h>
#include <TString.h>
#include <TCanvas.h>
#include <vector>
#include <TRootEmbeddedCanvas.h>
#include <TLatex.h>

#include "Utility/Info.h"
#include "Utility/VSDReader.h"
#include "Utility/TEventVisualization.h"

namespace Utility
{
class EventDisplay;
class TEventAnalyseGUI;
class VSDReader;
} // namespace Utility

namespace Strangeness
{
namespace internal
{
TDisplay::TDisplay(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Display(), kVerticalFrame)
  , fButtonTracks(new TGTextButton(this, Translation.Tracks()))
  , fFrameStrangeParticles(new TGHorizontalFrame(this))
  , fButtonV0(new TGTextButton(fFrameStrangeParticles, "V0s"))
  , fButtonCascade(new TGTextButton(fFrameStrangeParticles, "Cascades"))
  , fFrameGeometry(new TGHorizontalFrame(this))
  , fToggleDetector(new TGTextButton(fFrameGeometry, Translation.ButtonGeometry()))
  , fButtonAxes(new TGTextButton(fFrameGeometry, Translation.ButtonAxes()))
  , fButtonChangeBackground(new TGTextButton(this, Translation.ButtonBackground()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX);

  Utility::SetupNavigationButton(fButtonTracks, Translation.TipToggleTracks(), /*Toggle=*/true);
  this->AddFrame(fButtonTracks, HintExpandX);

  Utility::SetupNavigationButton(fButtonV0, Translation.TipToggleV0(), /*Toggle=*/true);
  fFrameStrangeParticles->AddFrame(fButtonV0, HintExpandX);

  Utility::SetupNavigationButton(fButtonCascade, Translation.TipToggleCascades(), /*Toggle=*/true);
  fFrameStrangeParticles->AddFrame(fButtonCascade, HintExpandX);
  this->AddFrame(fFrameStrangeParticles, HintExpandX);

  Utility::SetupNavigationButton(fToggleDetector, Translation.TipToggleDetectorVolumes(),
                                 /*Toggle=*/true);
  fFrameGeometry->AddFrame(fToggleDetector, HintExpandX);

  Utility::SetupNavigationButton(fButtonAxes, Translation.TipToggleAxes());
  fFrameGeometry->AddFrame(fButtonAxes, HintExpandX);

  this->AddFrame(fFrameGeometry, HintExpandX);

  // --- Background
  Utility::SetupNavigationButton(fButtonChangeBackground, Translation.TipToggleBackground());
  this->AddFrame(fButtonChangeBackground, HintExpandX);
}

THelp::THelp(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Encyclopaedia(), kVerticalFrame)
  , fButtonAliceInfo(new TGTextButton(this, "ALICE"))
  , fButtonDecayPatterns(new TGTextButton(this, Translation.ButtonDecayPatterns()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX);
  fButtonAliceInfo->SetToolTipText(Translation.TipShowInformation());
  this->AddFrame(fButtonAliceInfo, HintExpandX);

  fButtonDecayPatterns->SetToolTipText(Translation.TipShowDecays());
  this->AddFrame(fButtonDecayPatterns, HintExpandX);
}

TParticleInfo::TParticleInfo(TGCompositeFrame *parent, const Utility::TGUIEnglish &Translation)
  : TGGroupFrame(parent, Translation.ParticleInfo(), kVerticalFrame)
{
    std::vector<Int_t> particleIDs{3312, 3122, 2212, 310, 211, 11};
    std::vector<TString> particleNames{"#Xi^{-}, #bar{#Xi}^{+}", "#Lambda, #bar{#Lambda}", "p, #bar{p}", "K^{0}_{S}", "#pi^{+}, #pi^{-}", "e^{-}, e^{+}"};

    TDatabasePDG* pdgDb = TDatabasePDG::Instance();

    Double_t fontSize = 0.8/particleIDs.size();

    TLatex latex;
    latex.SetTextFont(42);
    latex.SetTextAlign(kHAlignLeft +kVAlignBottom);

    auto *frame = new TGHorizontalFrame(this);

    auto *left = new TGLayoutHints(kLHintsLeft | kLHintsExpandX);
    auto *right = new TGLayoutHints(kLHintsRight);
    auto *expandX = new TGLayoutHints(kLHintsExpandX);

    auto *pName = new TRootEmbeddedCanvas("", frame, 110, 120, 0);
    auto *pMass = new TRootEmbeddedCanvas("", frame, 110, 120, 0);
    frame->AddFrame(pMass, right);
    frame->AddFrame(pName, left);
    AddFrame(frame, expandX);
    pName->GetCanvas()->SetFillColor(18);
    pMass->GetCanvas()->SetFillColor(18);

    for(int i = particleIDs.size()-1; i >= 0; --i)
    {
        pName->GetCanvas()->cd();
        latex.SetTextSize(fontSize*1.2);
        latex.DrawLatex(0, fontSize*(i+1), particleNames[i]);
        pMass->GetCanvas()->cd();
        latex.SetTextSize(fontSize);
        latex.DrawLatex(0, fontSize*(i+1), Form("%5.4f GeV/#it{c}^{2}", pdgDb->GetParticle(particleIDs[i])->Mass()));
    }

    pName->GetCanvas()->SetEditable(kFALSE);
    pMass->GetCanvas()->SetEditable(kFALSE);

    pName->GetCanvas()->Modified();
    pName->GetCanvas()->Update();
    pMass->GetCanvas()->Modified();
    pMass->GetCanvas()->Update();
}
} // namespace internal

void TNavigation::InitialLoad() {
  //fDisplayStrangeFrame->fButtonTracks->Toggle(kTRUE);
  //fDisplayStrangeFrame->fButtonTracks->SetState(kButtonEngaged);
  fDisplayStrangeFrame->fToggleDetector->Toggle(kTRUE);
  fDisplayStrangeFrame->fToggleDetector->SetState(kButtonEngaged);
}

TNavigation::TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat)
  : Utility::INavigation(Exercise, Cheat)
  , fDisplayStrangeFrame(new internal::TDisplay(this, fTranslation))
  , fHelpStrange(new internal::THelp(this, fTranslation))
  , fParticleInfo(new internal::TParticleInfo(this, fTranslation))
{
  auto* LayoutExpandX = new TGLayoutHints(kLHintsExpandX);
  this->AddFrame(fDisplayStrangeFrame, LayoutExpandX);

  fHelpStrange->fButtonAliceInfo->Connect("Clicked()", "Utility::INavigation", this,
                                          "DetectorInfo()");
  fHelpStrange->fButtonDecayPatterns->Connect("Clicked()", "Strangeness::TNavigation", this,
                                              "PatternInfo()");
  this->AddFrame(fHelpStrange, LayoutExpandX);
  this->AddFrame(fParticleInfo, LayoutExpandX);
  Resize();
}

void TNavigation::SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI, Utility::TEventVisualization *EventVisualization,
                                   Utility::VSDReader* Reader)
{
  INavigation::SetupSignalSlots(AnalysisGUI, EventVisualization, Reader);

  Reader->Connect("ClearingV0s()", "Strangeness::EventDisplay", fEventDisplay, "ClearV0s()");
  Reader->Connect("LoadedV0Negative(TEveTrack*)", "Strangeness::EventDisplay", fEventDisplay,
                   "V0NegativeLoaded(TEveTrack*)");
  Reader->Connect("LoadedV0Positive(TEveTrack*)", "Strangeness::EventDisplay", fEventDisplay,
                   "V0PositiveLoaded(TEveTrack*)");
  Reader->Connect("LoadedV0()", "Strangeness::EventDisplay", fEventDisplay, "V0Loaded()");
  // Reader->Connect("FinishedV0s()", "Strangeness::EventDisplay", fEventDisplay,
  // "FinishedV0s()");

  Reader->Connect("ClearingCascades()", "Strangeness::EventDisplay", fEventDisplay,
                   "ClearCascades()");
  Reader->Connect("LoadedCascadeNegative(TEveTrack*)", "Strangeness::EventDisplay",
                   fEventDisplay, "CascadeNegativeLoaded(TEveTrack*)");
  Reader->Connect("LoadedCascadePositive(TEveTrack*)", "Strangeness::EventDisplay",
                   fEventDisplay, "CascadePositiveLoaded(TEveTrack*)");
  Reader->Connect("LoadedCascadeBachelor(TEveTrack*)", "Strangeness::EventDisplay",
                   fEventDisplay, "CascadeBachelorLoaded(TEveTrack*)");
  Reader->Connect("LoadedCascade()", "Strangeness::EventDisplay", fEventDisplay,
                   "CascadeLoaded()");
  Reader->Connect("FinishedCascades()", "Strangeness::EventDisplay", fEventDisplay,
                   "FinishedCascades()");

  // GUI Selection must be passed to the analysis.
  EventVisualization->Connect("SelectedV0Track(TEveTrack*)", "Strangeness::EventDisplay",
                              fEventDisplay, "TrackSelected(TEveTrack*)");
  EventVisualization->Connect("SelectedV0PointingLine(TEveTrack*)", "Strangeness::EventDisplay",
                              fEventDisplay, "TrackSelected(TEveTrack*)");
  EventVisualization->Connect("SelectedCascadeTrack(TEveTrack*)", "Strangeness::EventDisplay",
                              fEventDisplay, "TrackSelected(TEveTrack*)");


  fDisplayStrangeFrame->fButtonAxes->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                             "ToggleRenderable(=Utility::ERenderables::kAxes)");
  fDisplayStrangeFrame->fButtonTracks->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                               AnalysisGUI,
                                               "ToggleRenderable(=Utility::ERenderables::kTracks)");
  fDisplayStrangeFrame->fButtonV0->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                           "ToggleRenderable(=Utility::ERenderables::kV0s)");
  fDisplayStrangeFrame->fButtonCascade->Connect(
    "Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
    "ToggleRenderable(=Utility::ERenderables::kCascades)");
  fDisplayStrangeFrame->fToggleDetector->Connect(
    "Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
    "ToggleRenderable(=Utility::ERenderables::kGeometry)");
  fDisplayStrangeFrame->fButtonChangeBackground->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                                         AnalysisGUI, "ChangeBackgroundColor()");

  InitialLoad();
}

/// Show the decay pattern information
void TNavigation::PatternInfo()
{
  std::vector<TString> names = { "kaon", "lambda", "antilambda", "xi" };
  new Utility::InfoBox("Decay patterns", names, gClient->GetRoot(), 100, 100);
}
} // namespace Strangeness
