#include "Part1.h"

#include <RtypesCore.h>
#include <TApplication.h>
#include <TEveManager.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLayout.h>
#include <TGMsgBox.h>
#include <TString.h>
#include <memory>
#include <utility>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part1/EventDisplay.h"
#include "Strangeness/part1/TNavigation.h"
#include "Utility/EventDisplay.h"
#include "Utility/INavigation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/StdFixes.h"
#include "Utility/TEventAnalyseGUI.h"
#include "Utility/Utilities.h"

class TGPicture;

namespace Strangeness
{
/** Data set selector
 * @image html Strangeness/doc/Part1Selector.png
 * @ingroup alice_masterclass_str_part1
 */
 StrangenessDataset::StrangenessDataset(Bool_t allowAuto)
      : StandardDataset(20, allowAuto, kTRUE)
      , fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
 {
   setInstructions(fTranslation.DocDataSet());
   setMainWindowTitle("Strangeness");
 }

 std::unique_ptr<Utility::EventDisplay> StrangenessDataset::createEventDisplay(Bool_t demo)
 {
   return std_fix::make_unique<Strangeness::EventDisplay>(demo);
 }

 std::unique_ptr<Utility::INavigation> StrangenessDataset::createNavigation(Utility::EventDisplay *exercise,
                                                                            Bool_t cheat)
 {
   return std_fix::make_unique<Strangeness::TNavigation>(exercise, cheat);
 }

 void TInspectPP::RunExercise(Bool_t AllowAuto) { (new StrangenessDataset(AllowAuto))->Init(); }

} // namespace Strangeness
