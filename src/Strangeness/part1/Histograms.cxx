#include "Histograms.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>
#include <TString.h>

#include "Strangeness/GUITranslation.h"
#include "Utility/LanguageProvider.h"

namespace Strangeness
{
TH1* MakeInvMass(const char* name, const char* title, Color_t color, Double_t min, Double_t max)
{
  TH1* h = new TH1D(name, title, 50, min, max);

  h->SetDirectory(nullptr);
  h->SetLineColor(color);
  h->SetFillColor(color);
  h->SetFillStyle(3001);
  h->SetMarkerColor(color);
  h->SetMarkerStyle(20);

  const TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();
  h->SetXTitle(Form("%s (GeV/c^{2})", Translation.InvMass().Data()));
  h->SetYTitle(Translation.Counts());

  return h;
}

Histograms::Histograms()
  : fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
  , fInvMassK(MakeInvMass("p0321", "Kaons", kRed + 2, 0.4, 0.6))
  , fInvMassXi(MakeInvMass("p3312", "Xis", kGreen + 2, 1.2, 1.4))
  , fInvMassLambda(MakeInvMass("p3122", "Lambas", kBlue + 2, 1.0, 1.2))
  , fInvMassAntiLambda(MakeInvMass("a3122", "Anti-Lambdas", kMagenta + 2, 1.0, 1.2))
  , fInvMass(MakeInvMass(fTranslation.All(), fTranslation.Final(), kBlack, 0.0, 2.0))
  , fPreviousHisto(nullptr)
{
}

void Histograms::Setup()
{
  fCanvas = new TCanvas();
  fCanvas->Divide(2, 2);
  Update();
}

void Histograms::Fill(Int_t pdg, Double_t m)
{
  TH1* h = nullptr;
  switch (pdg) {
    case 310:
      h = fInvMassK;
      break;
    case 3122:
      h = fInvMassLambda;
      break;
    case -3122:
      h = fInvMassAntiLambda;
      break;
    case 3312:
      h = fInvMassXi;
      break;
  }
  if (h == nullptr) {
    return;
  }
  delete fPreviousHisto;
  fPreviousHisto = static_cast<TH1*>(h->Clone());
  fPreviousParticle = pdg;
  h->Fill(m);
  Update();
}

void Histograms::Update()
{
  fCanvas->cd();
  fCanvas->cd(1);
  fInvMassK->Draw();
  fCanvas->cd(2);
  fInvMassLambda->Draw();
  fCanvas->cd(3);
  fInvMassAntiLambda->Draw();
  fCanvas->cd(4);
  fInvMassXi->Draw();
  fCanvas->Modified();
  fCanvas->Update();
  fCanvas->cd();
}

void Histograms::Export(const char* name)
{
  TFile* out = TFile::Open(name, "RECREATE");
  out->cd();
  fInvMassK->Write();
  fInvMassXi->Write();
  fInvMassLambda->Write();
  fInvMassAntiLambda->Write();
  out->Write();
  out->Close();
}

void Histograms::Clear()
{
  fInvMassK->Reset();
  fInvMassLambda->Reset();
  fInvMassAntiLambda->Reset();
  fInvMassXi->Reset();
  delete fPreviousHisto;
  fPreviousHisto = nullptr;
  Update();
}

void Histograms::Undo()
{
  if(fPreviousHisto)
  {
    switch(fPreviousParticle)
    {
      case 310:
        delete fInvMassK;
        fInvMassK = fPreviousHisto;
        break;
      case 3122:
        delete fInvMassLambda;
        fInvMassLambda = fPreviousHisto;
        break;
      case -3122:
        delete fInvMassAntiLambda;
        fInvMassAntiLambda = fPreviousHisto;
        break;
      case 3312:
        delete fInvMassXi;
        fInvMassXi = fPreviousHisto;
    }
    fPreviousHisto = nullptr;
  }
}

} // namespace Strangeness
