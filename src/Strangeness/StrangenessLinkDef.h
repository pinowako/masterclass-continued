#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ struct Strangeness::TGUIEnglish+;
#pragma link C++ struct Strangeness::TGUIGerman+;
#pragma link C++ struct Strangeness::ClassLanguage+;

#pragma link C++ struct Strangeness::Obs+;

#pragma link C++ class Strangeness::Particle+;
#pragma link C++ class Strangeness::Calculator+;
#pragma link C++ class Strangeness::EventDisplay+;
#pragma link C++ class Strangeness::Exercise+;
#pragma link C++ struct Strangeness::Fitter+;
#pragma link C++ struct Strangeness::Histograms+;
#pragma link C++ struct Strangeness::Picker+;
#pragma link C++ struct Strangeness::Range+;

#pragma link C++ class Strangeness::TNavigation+;
#pragma link C++ struct Strangeness::internal::TDisplay+;
#pragma link C++ struct Strangeness::internal::THelp+;
#pragma link C++ struct Strangeness::internal::TParticleInfo+;

#endif
