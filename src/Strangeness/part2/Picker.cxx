#include "Picker.h"

#include <RtypesCore.h>
#include <TClass.h>
#include <TCollection.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TGCanvas.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TGListTree.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TObject.h>
#include <TString.h>

#include "Strangeness/GUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/LogoButtons.h"

namespace Utility
{
class Exercise;
} // namespace Utility

namespace Strangeness
{
Picker::Picker(TFile* file, Utility::Exercise* e, Bool_t cheat)
  : TGMainFrame(gClient->GetRoot(), 0, 0, kVerticalFrame)
  , fList(nullptr)
  , fTop(nullptr)
  , fPic(nullptr)
  , fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
{
  auto* lc = new TGCanvas(this, GetWidth(), 400);
  fList = new TGListTree(lc, kHorizontalFrame);

  // Get histogram picture
  fPic = gClient->GetPicture("h1_t.xpm");
  // Loop over directories
  TIter nextT(file->GetListOfKeys());
  TKey* keyT = nullptr;
  fList->AddRoot(fTranslation.Histograms());
  fTop = fList->GetFirstItem();
  fTop->SetCheckBox(true);
  fTop->CheckItem(false);
  while ((keyT = dynamic_cast<TKey*>(nextT())) != nullptr) {
    TObject* o = keyT->ReadObj();
    if (!o->IsA()->InheritsFrom(TDirectory::Class())) {
      continue;
    }
    auto* sub = dynamic_cast<TDirectory*>(o);
    auto* subI = new TGListTreeItemStd(gClient, sub->GetName(), nullptr, nullptr, true);
    // subI->CheckItem(false);
    fList->AddItem(fTop, subI);

    // Loop over histograms
    TIter nextH(sub->GetListOfKeys());
    TKey* keyH = nullptr;
    while ((keyH = dynamic_cast<TKey*>(nextH())) != nullptr) {
      o = keyH->ReadObj();
      if (!o->IsA()->InheritsFrom(TH1::Class())) {
        Printf("Not a histogram, but %s", o->ClassName());
        continue;
      }
      auto* h = dynamic_cast<TH1*>(o);
      Int_t c1 = h->GetBinContent(0);
      Int_t c2 = h->GetBinContent(h->GetNbinsX() + 1);
      if (c1 == 0 && c2 == 100) {
        continue; // Ignore Pb-Pb MB
      }
      if (h->GetEntries() < 100) {
        continue; // Ignore if low-stat.
      }
      TString tit(h->GetTitle());
      tit.ReplaceAll("#minus", "-");
      auto* hI = new TGListTreeItemStd(gClient, tit.Data(), fPic, fPic, true);
      fList->AddItem(subI, hI);
      hI->CheckItem(false);
      hI->SetUserData(h);
    }
  }
  AddFrame(lc, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  fList->Connect("Clicked(TGListTreeItem*,Int_t)", "Strangeness::Exercise", e,
                 "HistSelect(TGListTreeItem*,Int_t)");
  fList->Connect("Checked(TObject*,Bool_t)", "Strangeness::Picker", this,
                 "UndoCheck(TObject*,Bool_t)");

  // Add the logo and button
  new Utility::LogoButtons(this, e, cheat);
  Layout();
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

void Picker::UndoCheck(TObject* user, Bool_t /*unused*/)
{
  TGListTreeItem* item = fList->FindItemByObj(fTop, user);
  if (item == nullptr) {
    Warning("UndoCheck", "No item found");
    return;
  }
  item->Toggle();
}
} // namespace Strangeness
