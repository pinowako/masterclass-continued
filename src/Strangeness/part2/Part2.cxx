#include "Part2.h"

#include <TBrowser.h>

#include "Strangeness/part2/Exercise.h"
#include "Utility/Browser.h"

namespace Strangeness
{
void TPeakBackground::RunExercise(Bool_t AllowAuto)
{
  Utility::Browser* b = new Utility::Browser("Strangeness, 2nd exercise");
  b->Setup(new Exercise, AllowAuto);
  if (AllowAuto)
    b->Auto();
}
} // namespace Strangeness
