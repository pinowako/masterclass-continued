#include "Fitter.h"

#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TH1.h>
#include <TList.h>
#include <TString.h>

#include "Strangeness/GUITranslation.h"
#include "Strangeness/part2/Range.h"
#include "Utility/LanguageProvider.h"

namespace Strangeness
{
Fitter::Fitter(Exercise* b)
  : TGMainFrame(gClient->GetRoot(), 0, 60, kHorizontalFrame)
  , fTranslation(Utility::TranslationFromEnv<ClassLanguage>())
  , fMotherFrame(new TGVerticalFrame(this))
  , fSignal(new Range(fTranslation.SignalLimits(), 0, 2, fMotherFrame, 300, 30))
  , fBackground(new Range(fTranslation.FitRange(), 0, 2, fMotherFrame, 300, 30))
{
  auto* lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2);

  fMotherFrame->AddFrame(fSignal, lh);
  fMotherFrame->AddFrame(fBackground, lh);
  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  lh = new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 2);
  fMotherFrame = new TGVerticalFrame(this);
  fMotherFrame->AddFrame(fFit = new TGTextButton(fMotherFrame, fTranslation.Fit()), lh);
  fMotherFrame->AddFrame(fAccept = new TGTextButton(fMotherFrame, fTranslation.Accept()), lh);
  this->AddFrame(fMotherFrame, new TGLayoutHints(kLHintsLeft, 2, 2, 2, 2));

  fFit->Connect("Clicked()", "Strangeness::Exercise", b, "Fit()");
  fAccept->Connect("Clicked()", "Strangeness::Exercise", b, "Accept()");
  fFit->SetEnabled(false);
  fAccept->SetEnabled(false);

  Layout();
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

void Fitter::SetHistogram(TH1* h, Bool_t autoRange)
{
  if (h == nullptr) {
    fFit->SetEnabled(false);
    fAccept->SetEnabled(false);
    return;
  }
  if ((h->GetListOfFunctions() != nullptr) &&
      (h->GetListOfFunctions()->FindObject("bg") != nullptr)) {
    fFit->SetEnabled(false);
    fAccept->SetEnabled();
    fAccept->SetText("Clear");
    return;
  }
  fFit->SetEnabled(true);
  fAccept->SetText("Accept");
  fAccept->SetEnabled(false);
  if (!autoRange) {
    return;
  }
  Double_t sl = 0, sh = 2, bl = 0, bh = 2;
  switch (h->GetUniqueID()) {
    case 1:
      sl = 0.48;
      sh = 0.52;
      bl = .4;
      bh = .6;
      break;
    case 2:
      if (h->GetBinContent(0) == 70) {
        sl = 1.1;
        sh = 1.13;
        bl = 1.085;
        bh = 1.3;
        break;
      }
      // Fall through
    case 3:
      sl = 1.11;
      sh = 1.13;
      bl = 1.085;
      bh = 1.3;
      break;
  }
  Double_t ds = 0; // gRandom->Uniform(0,(sh-sl)/5);
  Double_t db = 0; // gRandom->Uniform(0,(bh-bl)/5);
  fSignal->Set(sl - ds, sh + ds);
  fBackground->Set(bl - db, bh + db);
}
} // namespace Strangeness
