Changes with respect to previous version
========================================

Part1:
------

- General code clean-up in event display 
- Counter tool is now embedded in the main GUI.  This allows the user
  to full-screen that window. 
- Counter automatically resets on next event
- *Event analysed* button automatically go to the next event
- Export directly to ROOT file 
- Added script to do merging across groups 

Part2:
-----

- Do not use TEveBrowser but custom thing. 
- Invariant mass spectra read from ROOT file (created by MakeFile.C)
- Selection tree browsers through the spectra 
- Values of measurements, yields, and ratio to pp automatically
  graphed. 
- Export to ROOT (or CSV) file directly 

  
