#include "Collect.h"

#include <RtypesCore.h>
#include <TCanvas.h>
#include <TCanvasImp.h>
#include <TClass.h>
#include <TDirectory.h>
#include <TError.h>
#include <TFile.h>
#include <TGClient.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TH1.h>
#include <TObject.h>
#include <TRootCanvas.h>
#include <TString.h>
#include <TVirtualPad.h>
#include <TRootBrowser.h>
#include <TGTab.h>

#include "Strangeness/GUITranslation.h"
#include "Utility/LanguageProvider.h"

class TGWindow;

namespace Strangeness
{
namespace
{
void SetHistogram(TCanvas* c, TH1* Histogram, Int_t Idx)
{
  TVirtualPad* p = c->cd(Idx);
  Histogram->Draw();
  p->SetTicks();
  p->Modified();
}
} // namespace

TFile* TCollectResults::CollectOpen(TCanvas* c)
{
  const char* types[] = { "ROOT File", "*.root", nullptr, nullptr };
  TGFileInfo info;
  info.fFileTypes = types;
  info.fMultipleSelection = false;
  new TGFileDialog(gClient->GetRoot(), fBrowser, kFDOpen, &info);
  if (info.fFilename == nullptr) {
    return nullptr;
  }

  TString out = info.fFilename;
  Int_t ei = info.fFileTypeIdx;
  const char* ext = &(types[ei + 1][1]);
  if (!out.EndsWith(ext)) {
    out.Append(ext);
  }

  TFile* file = TFile::Open(out, "READ");
  if (file == nullptr) {
    const TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();
    new TGMsgBox(gClient->GetRoot(), fBrowser, Translation.DiagFailedOpenFileTitle(),
                 Form("%s \"%s\"", Translation.DiagFailedOpenFileText().Data(), out.Data()),
                 kMBIconExclamation);
    return nullptr;
  }
  return file;
}

TH1* TCollectResults::GetH1(TDirectory* d, const char* name)
{
  if (d == nullptr) {
    return nullptr;
  }
  TObject* o = d->Get(name);
  if (o == nullptr) {
    Warning("GetH1", "Failed to get object %s from %s", name, d->GetName());
    return nullptr;
  }
  if (!o->IsA()->InheritsFrom(TH1::Class())) {
    Warning("GetH1", "Object %s from %s is not a TH1, but a %s", o->GetName(), d->GetName(),
            o->ClassName());
    return nullptr;
  }
  return dynamic_cast<TH1*>(o);
}

TH1* TCollectResults::CollectAdd(TH1* sum, TH1* h)
{
  if (sum == nullptr) {
    sum = dynamic_cast<TH1*>(h->Clone());
    sum->SetDirectory(nullptr);
    return sum;
  }
  Printf("sum is %s, h is %s", sum->GetName(), h->GetName());
  sum->Print();
  sum->Add(h);
  return sum;
}

Bool_t TCollectResults::CollectMore(TCanvas* c)
{
  Printf("Check if we need more input");
  Int_t ret = 0;
  TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();
  new TGMsgBox(gClient->GetRoot(), fBrowser, Translation.DiagMoreDataTitle(),
               Translation.DiagMoreDataText(), kMBIconQuestion, kMBYes | kMBNo, &ret);
  return ret == kMBYes;
}

void TCollectResults::RunExercise(Bool_t /*cheat*/)
{
  const TGUIEnglish& Translation = Utility::TranslationFromEnv<ClassLanguage>();

  fBrowser = new TRootBrowser(nullptr, "ALICE", 900, 700, "", false);
  fBrowser->DontCallClose();
  fBrowser->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");

  fBrowser->StartEmbedding(TRootBrowser::kRight);
  auto* c = new TCanvas(Translation.Collected(), Translation.StrangenessCollected(), 800, 800);
  c->SetTopMargin(0.01);
  c->SetRightMargin(0.01);
  c->Divide(2, 2);
  fBrowser->StopEmbedding(Translation.Summary());
  fBrowser->GetTabRight()->GetTabTab(Translation.Summary())->ShowClose(kFALSE);

  //Hide left and bottom tabs of the browser
  auto *fV1 = (TGCompositeFrame*)(fBrowser->GetTabLeft()->GetParent());
  auto *fHf = (TGCompositeFrame*)(fV1->GetParent());
  auto *fH2 = (TGCompositeFrame*)(fBrowser->GetTabBottom()->GetParent());
  auto *fV2 = (TGCompositeFrame*)(fH2->GetParent());
  fHf->HideFrame(fV1);
  fV2->HideFrame(fH2);

  fBrowser->MapWindow();

  TH1* sp0321 = nullptr;
  TH1* sp3312 = nullptr;
  TH1* sp3122 = nullptr;
  TH1* sa3122 = nullptr;

  do {
    TFile* file = CollectOpen(c);
    if (file == nullptr) {
      continue;
    }

    TH1* p0321 = GetH1(file, "p0321");
    TH1* p3312 = GetH1(file, "p3312"); // xi
    TH1* p3122 = GetH1(file, "p3122"); // Lambda
    TH1* a3122 = GetH1(file, "a3122"); // Anti-lambda

    if ((p0321 == nullptr) || (p3312 == nullptr) || (p3122 == nullptr) || (a3122 == nullptr)) {
      new TGMsgBox(gClient->GetRoot(), fBrowser, Translation.DiagIncompleteInputTitle(),
                   Form("%s %s", Translation.DiagIncompleteInputText().Data(), file->GetName()),
                   kMBIconExclamation);
      file->Close();
      continue;
    }

    sp0321 = CollectAdd(sp0321, p0321);
    sp3312 = CollectAdd(sp3312, p3312);
    sp3122 = CollectAdd(sp3122, p3122);
    sa3122 = CollectAdd(sa3122, a3122);

    SetHistogram(c, sp0321, 1);
    SetHistogram(c, sp3122, 2);
    SetHistogram(c, sa3122, 3);
    SetHistogram(c, sp3312, 4);

    c->Modified();
    c->Update();
    c->cd();

    file->Close();
  } while (CollectMore(c));
}
} // namespace Strangeness
