/**
 * @file   Collect.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar  9 20:36:02 2017
 *
 * @brief A GUI to collect the inclusive (Part 1) RAA measurements of
 * the students.
 *
 * This is to be run by the exercise instructor.  Either the
 * instructor can enter the numbers, or the students can enter them
 * themselves.
 * @ingroup alice_masterclass_raa_part1
 */
#ifndef RAACOLLECT_H_ZUSMHC5P
#define RAACOLLECT_H_ZUSMHC5P

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TGNumberEntry.h>
#include <TGraph.h>
#include <TH1.h>
#include <TLatex.h>
#include <TMultiGraph.h>
#include <TRandom.h>
#include <TString.h>

#include "Raa/ContentGUITranslation.h"
#include "Utility/AbstractMasterClassContent.h"
#include "Utility/ContentTranslation.h"

class TBuffer;
class TCanvas;
class TClass;
class TGNumberEntryField;
class TGraph;
class TMemberInspector;
class TMultiGraph;
class TVirtualPad;
class TRootBrowser;

namespace Raa
{
struct TGUIEnglish;
/**
 * A value from the students.
 *
 * @ingroup alice_masterclass_raa_part1
 */
struct StudentValue {
  TGraph* fP;
  TGraph* fPMinPt;
  TGraph* fS;
  TGraph* fSMinPt;
  TGraph* fC;
  TGraph* fCMinPt;
  Bool_t fOK;
  Int_t fId;
  const TGUIEnglish& fTranslation;

  StudentValue(Int_t id);

  TGraph* MakeGraph(const char* prefix, Int_t id);

  void Set(Double_t p, Double_t pMin, Double_t s, Double_t sMin, Double_t c, Double_t cMin);
};

/**
 * Input the student values.
 *
 * @ingroup alice_masterclass_raa_part1
 */
struct ValuesInput : public TGTransientFrame {
  StudentValue* fSet{ nullptr };
  TGNumberEntryField* fP;
  TGNumberEntryField* fPMinPt;
  TGNumberEntryField* fS;
  TGNumberEntryField* fSMinPt;
  TGNumberEntryField* fC;
  TGNumberEntryField* fCMinPt;

  const TGUIEnglish& fTranslation;

  ValuesInput();

  void Popup(StudentValue* set, Bool_t rndm = false);
  void Take();

  ClassDef(ValuesInput, 0);
};

class TCollectResults : public Utility::TAbstractExercise
{
 TRootBrowser* fBrowser;
 public:
  TCollectResults()
    : TAbstractExercise("collect results")
  {
  }

/**
 * Draw a Multi-graph.
 *
 * @param mg  Multi-graph
 * @param p   Pad
 * @param sub Sub-pad number
 * @param txt Text to draw
 *
 * @ingroup alice_masterclass_raa_part1
 */
void CollectDrawMG(TMultiGraph* mg, TVirtualPad* p, Int_t sub, const char* txt);

/**
 * Add a graph to a multi graph, making sure to get a new histogram.
 *
 * @param mg Multi-graph
 * @param g  Graph to add
 *
 * @ingroup alice_masterclass_raa_part1
 */
void CollectAdd(TMultiGraph* mg, TGraph* g);

/**
 * Check if we need more data.
 *
 * @param c Canvas to draw on top of
 *
 * @return true if we need more
 *
 * @ingroup alice_masterclass_raa_part1
 */
Bool_t CollectMore(TCanvas* c);

  /**
   * Entry point.
   * @param test Test mode
   * @ingroup alice_masterclass_raa_part1
   */
  void RunExercise(Bool_t test = false) override;
};
} // namespace Raa

#endif /* end of include guard: RAACOLLECT_H_ZUSMHC5P */
