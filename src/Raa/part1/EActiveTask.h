#ifndef EACTIVETASK_H_TAWLMBX9
#define EACTIVETASK_H_TAWLMBX9

namespace Raa
{
enum class EActiveTask {
  kCountPrimaries,   ///< Count only primary tracks
  kCountCharged,     ///< Count primary tracks that are positivly charged
  kCountSecondaries, ///< Count and find secondary tracks.
  kFindHighestPT,    ///< Find the primary track with the highest Pt 
};
} // namespace Raa

#endif /* end of include guard: EACTIVETASK_H_TAWLMBX9 */
