#ifndef ECOLLISIONSYSTEM_H_IEZCRRKS
#define ECOLLISIONSYSTEM_H_IEZCRRKS

#include <RtypesCore.h>

namespace Raa
{
enum class ECollisionSystem {
  kpp7TeV = 1,      // 1, pp 7TeV, B=0T
  kpp2_76TeV,       // 2, pp 2.76TeV, B=0.5T
  kPbPbPeripheral,  // 3, PbPb, (B=0.5T?)
  kPbPbSemiCentral, // 4, PbPb, (B=0.5T?)
  kPbPbCentral,     // 5, PbPb, (B=0.5T?)

};

inline Bool_t IsProtonProton(ECollisionSystem C)
{
  return C == ECollisionSystem::kpp2_76TeV || C == ECollisionSystem::kpp7TeV;
}

inline Bool_t IsLeadLead(ECollisionSystem C) { return !IsProtonProton(C); }
} // namespace Raa

#endif /* end of include guard: ECOLLISIONSYSTEM_H_IEZCRRKS */
