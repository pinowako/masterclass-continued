#include "TNavigation.h"

#include "Raa/part1/EActiveTask.h"

#include <TGButton.h>
#include <TGLayout.h>
#include <TString.h>

#include "Utility/VSDReader.h"
#include "Utility/TEventVisualization.h"

namespace Utility
{
class EventDisplay;
class TEventAnalyseGUI;
} // namespace Utility

namespace Raa
{
namespace internal
{
TDisplay::TDisplay(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Display(), kVerticalFrame)
  , fButtonIP(new TGTextButton(this, Translation.IP()))
  , fButtonClusters(new TGTextButton(this, Translation.Clusters()))
  , fButtonTracks(new TGTextButton(this, Translation.Tracks()))
  , fShowPrimaries(new TGTextButton(this, Translation.ButtonShowPrimary()))
  , fFrameGeometry(new TGHorizontalFrame(this))
  , fToggleDetector(new TGTextButton(fFrameGeometry, Translation.ButtonGeometry()))
  , fButtonAxes(new TGTextButton(fFrameGeometry, Translation.ButtonAxes()))
  , fButtonChangeBackground(new TGTextButton(this, Translation.ButtonBackground()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX);

  Utility::SetupNavigationButton(fButtonIP, Translation.TipTogglePrimary(), /*Toggle=*/true);
  this->AddFrame(fButtonIP, HintExpandX);

  Utility::SetupNavigationButton(fButtonClusters, Translation.TipToggleClusters());
  this->AddFrame(fButtonClusters, HintExpandX);

  Utility::SetupNavigationButton(fButtonTracks, Translation.TipToggleTracks(), /*Toggle=*/true);
  this->AddFrame(fButtonTracks, HintExpandX);

  Utility::SetupNavigationButton(fShowPrimaries, Translation.TipTogglePrimaryOnly());
  this->AddFrame(fShowPrimaries, HintExpandX);

  Utility::SetupNavigationButton(fToggleDetector, Translation.TipToggleDetectorVolumes(),
                                 /*Toggle=*/true);
  fFrameGeometry->AddFrame(fToggleDetector, HintExpandX);

  Utility::SetupNavigationButton(fButtonAxes, Translation.TipToggleAxes());
  fFrameGeometry->AddFrame(fButtonAxes, HintExpandX);

  this->AddFrame(fFrameGeometry, HintExpandX);

  // --- Background
  Utility::SetupNavigationButton(fButtonChangeBackground, Translation.TipToggleBackground());
  this->AddFrame(fButtonChangeBackground, HintExpandX);
}

THelp::THelp(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Encyclopaedia(), kVerticalFrame)
  , fButtonAliceInfo(new TGTextButton(this, "ALICE"))
{
  fButtonAliceInfo->SetToolTipText(Translation.TipShowInformation());
  this->AddFrame(fButtonAliceInfo, new TGLayoutHints(kLHintsExpandX));
}

TTasks::TTasks(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation)
  : TGGroupFrame(parent, Translation.Tasks(), kVerticalFrame)
  , fTaskRow1(new TGHorizontalFrame(this, 30, 15, kFixedWidth))
  , fTaskRow2(new TGHorizontalFrame(this, 30, 15, kFixedWidth))
  , fTaskPrimariesButton(new TGTextButton(fTaskRow1, Translation.Task1()))
  , fTaskChargedButton(new TGTextButton(fTaskRow1, Translation.Task2()))
  , fTaskSecondariesButton(new TGTextButton(fTaskRow2, Translation.Task3()))
  , fTaskPrimariesHighPtButton(new TGTextButton(fTaskRow2, Translation.Task4()))
{
  auto* HintExpandX = new TGLayoutHints(kLHintsExpandX);

  fTaskRow1->AddFrame(fTaskPrimariesButton, HintExpandX);
  fTaskRow1->AddFrame(fTaskChargedButton, HintExpandX);
  fTaskRow2->AddFrame(fTaskSecondariesButton, HintExpandX);
  fTaskRow2->AddFrame(fTaskPrimariesHighPtButton, HintExpandX);

  this->AddFrame(fTaskRow1, HintExpandX);
  this->AddFrame(fTaskRow2, HintExpandX);

  fTaskPrimariesButton->AllowStayDown(true);
  fTaskChargedButton->AllowStayDown(true);
  fTaskSecondariesButton->AllowStayDown(true);
  fTaskPrimariesHighPtButton->AllowStayDown(true);
}

void TTasks::ChangeTask(Int_t NewTask)
{
  auto EnumNewTask = gsl::narrow_cast<EActiveTask>(NewTask);
  ToggleButtons(EnumNewTask);
  // NOTE Necessary because Signal/Slot does not work with 'enum class' types.
  Emit("SwitchedTask(Int_t)", NewTask);
}

void TTasks::ToggleButtons(EActiveTask ChoosenTask)
{
  fTaskPrimariesButton->SetState(ChoosenTask == EActiveTask::kCountPrimaries ? kButtonDown
                                                                             : kButtonUp);
  fTaskChargedButton->SetState(ChoosenTask == EActiveTask::kCountCharged ? kButtonDown : kButtonUp);
  fTaskSecondariesButton->SetState(ChoosenTask == EActiveTask::kCountSecondaries ? kButtonDown
                                                                                 : kButtonUp);
  fTaskPrimariesHighPtButton->SetState(ChoosenTask == EActiveTask::kFindHighestPT ? kButtonDown
                                                                                  : kButtonUp);
}

} // namespace internal

void TNavigation::InitialLoad() {
  //fDisplayRaaFrame->fButtonIP->Toggle(kTRUE);
  //fDisplayRaaFrame->fButtonIP->SetState(kButtonEngaged);
  //fDisplayRaaFrame->fButtonTracks->Toggle(kTRUE);
  //fDisplayRaaFrame->fButtonTracks->SetState(kButtonEngaged);
  fDisplayRaaFrame->fToggleDetector->Toggle(kTRUE);
  fDisplayRaaFrame->fToggleDetector->SetState(kButtonEngaged);
  fTasksRaa->ChangeTask(static_cast<Int_t>(EActiveTask::kCountPrimaries));
}

TNavigation::TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat)
  : Utility::INavigation(Exercise, Cheat)
  , fDisplayRaaFrame(new internal::TDisplay(this, fTranslation))
  , fHelpRaa(new internal::THelp(this, fTranslation))
  , fTasksRaa(new internal::TTasks(this, fTranslation))
{
  auto* LayoutExpandX = new TGLayoutHints(kLHintsExpandX);
  this->AddFrame(fDisplayRaaFrame, LayoutExpandX);

  fHelpRaa->fButtonAliceInfo->Connect("Clicked()", "Utility::INavigation", this, "DetectorInfo()");
  this->AddFrame(fHelpRaa, LayoutExpandX);
  this->AddFrame(fTasksRaa, LayoutExpandX);

  Resize();
}

void TNavigation::SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI, Utility::TEventVisualization *EventVisualization,
                                   Utility::VSDReader* Reader)
{
  INavigation::SetupSignalSlots(AnalysisGUI, EventVisualization, Reader);

  Reader->Connect("LoadedTrack(TEveTrack*,Bool_t)", "Raa::EventDisplay", fEventDisplay,
                   "TrackLoaded(TEveTrack*,Bool_t)");
  Reader->Connect("FinishedTracks()", "Raa::EventDisplay", fEventDisplay, "FinishedTracks()");

  // GUI Selection must be passed to the analysis.
  EventVisualization->Connect("SelectedESDTrack(TEveTrack*)", "Raa::EventDisplay", fEventDisplay,
                              "TrackSelected(TEveTrack*)");

  EventVisualization->Connect("SelectedClustersITS()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                              "ShowDetectorDetails(=\"ITS\")");
  EventVisualization->Connect("SelectedClustersTPC()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                              "ShowDetectorDetails(=\"TPC\")");
  EventVisualization->Connect("SelectedClustersTRDTOF()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                              "ShowDetectorDetails(=\"TRD+TOF\")");


  fDisplayRaaFrame->fButtonIP->Connect(
    "Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
    "ToggleRenderable(=Utility::ERenderables::kIntersectionPoint)");
  fDisplayRaaFrame->fButtonAxes->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                         "ToggleRenderable(=Utility::ERenderables::kAxes)");
  fDisplayRaaFrame->fButtonClusters->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                             "ToggleRenderable(=Utility::ERenderables::kClusters)");
  fDisplayRaaFrame->fButtonTracks->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                           "ToggleRenderable(=Utility::ERenderables::kTracks)");
  fDisplayRaaFrame->fShowPrimaries->Connect(
    "Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
    "ToggleRenderable(=Utility::ERenderables::kPrimaries()");

  fDisplayRaaFrame->fToggleDetector->Connect("Clicked()", "Utility::TEventAnalyseGUI", AnalysisGUI,
                                             "ToggleRenderable(=Utility::ERenderables::kGeometry)");
  fDisplayRaaFrame->fButtonChangeBackground->Connect("Clicked()", "Utility::TEventAnalyseGUI",
                                                     AnalysisGUI, "ChangeBackgroundColor()");

  // NOTE I see the ugliness, Signal/Slot does not work with 'enum class'.
  // 'gsl::narrow_cast' is used to show a 'downcast' in the correspdonding
  // Slots.
  fTasksRaa->fTaskPrimariesButton->Connect(
    "Clicked()", "Raa::internal::TTasks", fTasksRaa,
    Form("ChangeTask(=%d)", static_cast<Int_t>(EActiveTask::kCountPrimaries)));
  fTasksRaa->fTaskChargedButton->Connect(
    "Clicked()", "Raa::internal::TTasks", fTasksRaa,
    Form("ChangeTask(=%d)", static_cast<Int_t>(EActiveTask::kCountCharged)));
  fTasksRaa->fTaskSecondariesButton->Connect(
    "Clicked()", "Raa::internal::TTasks", fTasksRaa,
    Form("ChangeTask(=%d)", static_cast<Int_t>(EActiveTask::kCountSecondaries)));
  fTasksRaa->fTaskPrimariesHighPtButton->Connect(
    "Clicked()", "Raa::internal::TTasks", fTasksRaa,
    Form("ChangeTask(=%d)", static_cast<Int_t>(EActiveTask::kFindHighestPT)));
  fTasksRaa->Connect("SwitchedTask(Int_t)", "Utility::TEventAnalyseGUI", AnalysisGUI,
                     "DispatchTaskChoice(Int_t)");

  InitialLoad();
}

void TNavigation::SetEventNumber(Int_t Num, Int_t Max)
{
  INavigation::SetEventNumber(Num, Max);

  if (Num > 31) {
    SetTask((Int_t)EActiveTask::kCountPrimaries);
    fTasksRaa->fTaskPrimariesButton->SetEnabled(false);
    fTasksRaa->fTaskChargedButton->SetEnabled(false);
    fTasksRaa->fTaskSecondariesButton->SetEnabled(false);
    fTasksRaa->fTaskPrimariesHighPtButton->SetEnabled(false);
  } else {
    fTasksRaa->fTaskPrimariesButton->SetEnabled(true);
    fTasksRaa->fTaskChargedButton->SetEnabled(true);
    fTasksRaa->fTaskSecondariesButton->SetEnabled(true);
    fTasksRaa->fTaskPrimariesHighPtButton->SetEnabled(true);
  }
}

} // namespace Raa
