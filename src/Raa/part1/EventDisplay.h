/**
 * @file   EventDisplay.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar 16 23:30:50 2017
 *
 * @brief  The event display for the RAA exercise
 * @ingroup alice_masterclass_raa_part1
 */
#ifndef RAAEVENTDISPLAY_C
#define RAAEVENTDISPLAY_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TCanvas.h>
#include <TEveTrack.h>
#include <TFile.h>
#include <TMath.h>
#include <TRootBrowser.h>
#include <TRootHelpDialog.h>
#include <TString.h>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part1/EActiveTask.h"
#include "Raa/part1/ECollisionSystem.h"
#include "Raa/part1/TAnalysisWidget.h"
#include "Raa/part1/TRaaStatistics.h"
#include "Raa/part1/TStatisticsWidget.h"
#include "Raa/part1/TTrackCounter.h"
#include "Raa/part1/TTrackInfoWidget.h"
#include "Raa/part1/TrackClassification.h"
#include "Utility/EventDisplay.h"

class TBuffer;
class TClass;
class TEveElement;
class TEveTrack;
class TMemberInspector;
class TRootBrowser;

namespace Raa
{
/**
 * Manager of the nuclear modification factor event display.
 * @image html Raa/doc/EventDisplay.png
 * @ingroup alice_masterclass_raa_part1
 */
class EventDisplay : public Utility::EventDisplay
{
 private:
  /// Store which task is currently selected by the user to determine what
  /// is supposed to be counted and things like that.
  EActiveTask fCurrentTask{ EActiveTask::kCountPrimaries };
  /// Automatically count all loaded tracks for the event according to their classification.
  TTrackCounter fAutomaticCounting;
  /// The statistics over the whole dataset for the automatic counting.
  TRaaStatistics fAutomaticPPStatistics;

  /// Count the user selections according to choosen Task.
  TTrackCounter fManualCounting;
  /// The statistics over the whole dataset for the manual counting.
  TRaaStatistics fManualPPStatistics;

  /// TODO Refactor the triple of correctionfactor and RaaResult into on small
  /// helper object.

  const Double_t kCorrectionPeripheral = 6.32;
  /// Raa-factor for peripheral PbPb to PP with uncertainty {Raa, dRaa} from automatic analysis.
  RaaResult fRaaPeripheralAuto;
  /// Raa-factor for peripheral PbPb to PP with uncertainty {Raa, dRaa} from manual analysis.
  RaaResult fRaaPeripheralManual;

  const Double_t kCorrectionSemiCentral = 438.80;
  /// Raa-factor for semi-central PbPb to PP with uncertainty {Raa, dRaa} from automatic analysis.
  RaaResult fRaaSemiCentralAuto;
  /// Raa-factor for semi-central PbPb to PP with uncertainty {Raa, dRaa} from manual analysis.
  RaaResult fRaaSemiCentralManual;

  const Double_t kCorrectionCentral = 1686.87;
  /// Raa-factor for central PbPb to PP with uncertainty {Raa, dRaa} from automatic analysis.
  RaaResult fRaaCentralAuto;
  /// Raa-factor for central PbPb to PP with uncertainty {Raa, dRaa} from manual analysis.
  RaaResult fRaaCentralManual;

  /// This widget displays basic information about the currently selected track
  /// to get a more detailed image of its properties.
  TTrackInfoWidget* fTrackInfoWidget = nullptr;

  /// This widget displays the analysis results and plots some histograms to
  /// see the difference between PP and PbPb.
  /// It is attached to the third tab of the GUI.
  TAnalysisWidget* fAnalysisWidget = nullptr;

  /// This widget displays the histograms containg the dataset statistics
  /// containing distributions, multiplicities and the like.
  TStatisticsWidget* fStatisticsWidget = nullptr;

  /// The Pt distribution for the peripheral PbPb Event.
  TPtDistribution fPtDistPbPbPeripheral;
  /// The Pt distribution for the semi-central PbPb Event.
  TPtDistribution fPtDistPbPbSemiCentral;
  /// The Pt distribution for the central PbPb Event.
  TPtDistribution fPtDistPbPbCentral;

  /// Information about the kind of the current Event, e.g. PbPb or PP
  ECollisionSystem fCollSys;

  const TGUIEnglish& fTranslation;

  /// This method fills the counters for the manual counting from the automatic
  /// counter.
  /// This is necessary for example, if the task is to count the number of
  /// secondaries. To do the calculations for Raa you still need information
  /// about the primaries. These will be added here.
  void FillMissingManualCountsDependingOnTask();

  /// Helper method to calculate Raa for automatic and manual analysis.
  /// Assigns the result directly to the result-pairs.
  void RaaCalculator(RaaResult& TargetAuto, RaaResult& TargetManual,
                     Double_t CorrectionFactor) noexcept;

 public:
  /// Default initialize all histograms and retrieve the translation.
  EventDisplay();

  /// Add our elements to the EVE browser
  /// @param browser Browser implementation to use
  /// @param cheat   True if cheats are on
  void Setup(TRootBrowser* browser, Bool_t /*unused*/) override;

  /// This operation is called when the user changes the task.
  /// It might fail, if the user already selected tracks in the currently loaded
  /// event. In that case the method returns \c false and the task choice
  /// has been rejected. The caller *MUST* ensure that the failure is
  /// back-propagated!
  Bool_t TryChooseTask(Int_t NTask) override;

  Int_t GetCurrentTask() const noexcept override { return (Int_t)fCurrentTask; }

  /// Check what kind of event we have
  /// @param evNo Event number in file
  /// @return Description of the event
  TString NewEvent(Int_t evNo) override;

  /// Get the current magnetic field, either 0.0T or 0.5T
  Double_t GetMagneticField() const override;

  /// Publish the count to the histograms, and update calculations.
  void EventDone() override;

  /// Callback for every track that is loaded.
  void TrackLoaded(TEveTrack* Track, Bool_t included);
  /// Callback after all Tracks of the Event are loaded.
  void FinishedTracks();

  /// Callback if a track has been selected.
  /// Check if the track has been counted or if it is not a primary.
  /// Otherwise, count the track if we're on a pp event.
  void TrackSelected(TEveTrack* Track);

  /// Return string containing the instructions for the class.
  const TString& Instructions() const override { return fTranslation.PathEventDisplay(); }

  /// This masterclass can be printed, \returns true
  Bool_t CanPrint() const override { return true; }

  /// This masterclass can be saved, \returns true
  Bool_t CanSave() const override { return true; }

  /// Print the results to a pdf with filename \p out.
  void PrintPdf(const char* /*out*/) override
  { /* fHistograms.Canvas().SaveAs(out); */
  }

  /// Export the results to a root-file with filename \p out.
  void Export(const TString& out) override;

  /// Returns the path to the data -> TODO must change somewhat
  TString DataDir() const override { return Utility::ResourcePath("vsdData/Raa"); }

  ClassDef(EventDisplay, 0);
};
} // namespace Raa
#endif
