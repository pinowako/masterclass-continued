#ifndef TRACKCLASSIFICATION_CXX_XRSVCXFU
#define TRACKCLASSIFICATION_CXX_XRSVCXFU

#include <gsl/gsl>
#include "Utility/HelperPrimaryTracks.h"

class TEveTrack;
namespace Raa
{
/// Currently suboptimal -> This logic should be only in Raa, right now
/// calls the 'Utility::IsPrimary' because VSDReader needs to know about
/// primary tracks already. Will be changed in future refactorings
/// (when VSDReader is properly subclassed for each MasterClass).
inline Bool_t IsPrimary(const TEveTrack* T) { return Utility::IsPrimary(T); }

/// Check if the Track has a positive charge assuming it is a primary track.
Bool_t IsPrimaryPositive(const TEveTrack* T) noexcept;

/// Calculate Pt for the Track, result in GeV
Double_t GetPt(const TEveTrack* T) noexcept;

/// Check if the Track is a secondary track (meaning it is not an Primary).
inline Bool_t IsSecondary(const TEveTrack* T) noexcept { return !IsPrimary(T); }
} // namespace Raa

#endif /* end of include guard: TRACKCLASSIFICATION_CXX_XRSVCXFU */
