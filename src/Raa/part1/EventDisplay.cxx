#include "EventDisplay.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TEveElement.h>
#include <TEveTrack.h>
#include <TEveVector.h>
#include <TFile.h>
#include <TGClient.h>
#include <TMath.h>
#include <TRootBrowser.h>
#include <TRootHelpDialog.h>
#include <TString.h>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part1/TrackClassification.h"
#include "Utility/ERenderElements.h"
#include "Utility/HelperPrimaryTracks.h"
#include "Utility/LanguageProvider.h"

namespace
{
void PopupWarning(const TString& Message)
{
  auto* startMessage = new TRootHelpDialog(gClient->GetRoot(), "Warning", 250, 100);
  startMessage->SetText(Message);
  startMessage->Popup();
}
} // namespace

namespace Raa
{
void EventDisplay::FillMissingManualCountsDependingOnTask()
{
  switch (fCurrentTask) {
    case EActiveTask::kCountPrimaries:
      fManualCounting.CountPrimariesPositive(fAutomaticCounting.PrimariesPositive());
      fManualCounting.CountSecondaries(fAutomaticCounting.Secondaries());
      break;
    case EActiveTask::kCountCharged:
      fManualCounting.CountPrimaries(fAutomaticCounting.Primaries());
      fManualCounting.CountSecondaries(fAutomaticCounting.Secondaries());
      break;
    case EActiveTask::kCountSecondaries:
      fManualCounting.CountPrimaries(fAutomaticCounting.Primaries());
      fManualCounting.CountPrimariesPositive(fAutomaticCounting.PrimariesPositive());
      break;
    case EActiveTask::kFindHighestPT:
      fManualCounting.CountPrimaries(fAutomaticCounting.Primaries());
      fManualCounting.CountPrimariesPositive(fAutomaticCounting.PrimariesPositive());
      fManualCounting.CountSecondaries(fAutomaticCounting.Secondaries());
      break;
  }
}

namespace
{
std::vector<TEveTrack*> FilterTracks(const gsl::span<TEveTrack* const> Primaries,
                                     Double_t Threshold)
{
  for (auto* t : Primaries)
    t->IncDenyDestroy();

  std::vector<TEveTrack*> FilteredTracks(Primaries.begin(), Primaries.end());
  FilteredTracks.erase(
    std::remove_if(std::begin(FilteredTracks), std::end(FilteredTracks),
                   [Threshold](TEveTrack* const t) { return GetPt(t) < Threshold; }),
    FilteredTracks.end());

  return FilteredTracks;
}
} // namespace

void EventDisplay::RaaCalculator(RaaResult& TargetAuto, RaaResult& TargetManual,
                                 Double_t CorrectionFactor) noexcept
{
  const Int_t NPrimariesLeadLead = fAutomaticCounting.NPrimaries();
  // This case can occur when the tracks are deactivated.
  if (NPrimariesLeadLead == 0)
    return;

  // Automatic counting did count tracks. The opposite might be the case
  // when loading tracks is deactivated for the *whole* data set, which is
  // a pathological case.
  if (fAutomaticPPStatistics.HistMultiplicity().GetMean() > 0.) {
    const auto r =
      RaaFor(NPrimariesLeadLead, fAutomaticPPStatistics.HistMultiplicity(), CorrectionFactor);
    TargetAuto = { r.first, r.second };
  }

  // Do the manual calculation only if at least one primary track has been
  // selected. The 'empty' case can occur when the user just clicks through
  // the events without doing the task 'Select Primaries'.
  if (fManualPPStatistics.HistMultiplicity().GetMean() > 0.) {
    const auto r =
      RaaFor(NPrimariesLeadLead, fManualPPStatistics.HistMultiplicity(), CorrectionFactor);
    TargetManual = { r.first, r.second };
  }

  /// !!!!!!!!!!!!!!!!!
  /// This section is to check if the Raa factors make sense.
  /// Changing the 'MinPt' must happen here *AND* in
  /// 'TRaaStatistics::AddEventStatistics::PtThreshold'.
  /// !!!!!!!!!!!!!!!!!
  std::cout << "**** Experimental - Calculation for High PT > 2GeV\n";
  const std::vector<TEveTrack*> HighPtLead = FilterTracks(fAutomaticCounting.Primaries(), 1.0);
  if (!HighPtLead.empty() && fAutomaticPPStatistics.HistMultiplicityMinPt().GetMean() > 0.) {
    auto AutoHighPt =
      RaaFor(HighPtLead.size(), fAutomaticPPStatistics.HistMultiplicityMinPt(), CorrectionFactor);
    std::cout << "HighPt Raa: " << AutoHighPt.first << " - dRaa: " << AutoHighPt.second << "\n";
  }
}

EventDisplay::EventDisplay()
  : fAutomaticPPStatistics("AutomaticPP")
  , fManualPPStatistics("ManualPP")
  , fPtDistPbPbPeripheral("PbPbPeripheral")
  , fPtDistPbPbSemiCentral("PbPbSemiCentral")
  , fPtDistPbPbCentral("PbPbCentral")
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
}

/**
 * Add our elements to the EVE browser
 *
 * @param browser Browser implementation to use
 * @param cheat   True if cheats are on
 */
void EventDisplay::Setup(TRootBrowser* browser, Bool_t /*unused*/)
{
  // Set-up the counter interface
  browser->StartEmbedding(TRootBrowser::kBottom);
  fTrackInfoWidget = new TTrackInfoWidget(gClient->GetRoot());
  browser->StopEmbedding(fTranslation.Counter().Data());

  // Create histogram tab
  browser->StartEmbedding(TRootBrowser::kRight);
  fStatisticsWidget =
    new TStatisticsWidget(gClient->GetRoot(), fAutomaticPPStatistics, fManualPPStatistics);
  browser->StopEmbedding(fTranslation.EventCharacteristics().Data());
  browser->GetTabRight()->GetTabTab(fTranslation.EventCharacteristics())->ShowClose(kFALSE);

  // Create Tab displaying the Analysis Results, mainly Raa factors for the
  // different cases.
  browser->StartEmbedding(TRootBrowser::kRight);
  fAnalysisWidget =
    new TAnalysisWidget(gClient->GetRoot(), fAutomaticPPStatistics, fPtDistPbPbPeripheral,
                        fPtDistPbPbSemiCentral, fPtDistPbPbCentral);
  browser->StopEmbedding(fTranslation.Analysis().Data());
  browser->GetTabRight()->GetTabTab(fTranslation.Analysis())->ShowClose(kFALSE);
}

Bool_t EventDisplay::TryChooseTask(Int_t NTask)
{
  // There has been a selection already, that means this event has to be
  // finished first!
  if (fManualCounting.NPrimaries() > 0 || fManualCounting.NSecondaries() > 0 ||
      fManualCounting.NPrimariesPositive() > 0) {
    std::cerr << "!! Prevent Task Choice because of already started task in this event\n";
    PopupWarning("Can not change task in between work of Event!");
    return kFALSE;
  }

  // Ok to switch the task for this event.
  fCurrentTask = (EActiveTask)NTask;
  return kTRUE;
}

TString EventDisplay::NewEvent(Int_t evNo)
{
  Expects(evNo >= 0 && evNo <= 33);
  TString type;
  std::cerr << "Displaying Raa Event #" << evNo << "\n";
  if (evNo == 31) {
    type = Form("PbPb, %s", fTranslation.Peripheral().Data());
    fCollSys = ECollisionSystem::kPbPbPeripheral;
  } else if (evNo == 32) {
    type = Form("PbPb, %s", fTranslation.SemiCentral().Data());
    fCollSys = ECollisionSystem::kPbPbSemiCentral;
  } else if (evNo == 33) {
    type = Form("PbPb, %s", fTranslation.Central().Data());
    fCollSys = ECollisionSystem::kPbPbCentral;
  } else if (evNo == 0) {
    type = "pp, 7 TeV, B=0 T";
    fCollSys = ECollisionSystem::kpp7TeV;
  } else {
    type = "pp, 2.76 TeV, B=0.5 T";
    fCollSys = ECollisionSystem::kpp2_76TeV;
  }
  std::cerr << "Decided on CollSysType = " << type << "\n";

  // Clear the counters for the local event but *NOT* the statistics because
  // they are for the full dataset!
  fAutomaticCounting.Clear();
  fManualCounting.Clear();

  return type;
}

Double_t EventDisplay::GetMagneticField() const
{
  return fCollSys == ECollisionSystem::kpp7TeV ? 0. : .5;
}

void EventDisplay::EventDone()
{
  /// Reset the track selection info, because no track can be selected anymore.
  fTrackInfoWidget->ResetInformation();

  // Fill the Track counts as a first step. These are then used to fill
  // the distributions accordingly.
  if (fCollSys == ECollisionSystem::kpp2_76TeV) {
    // Ensure that only the part the user counts manually is used from the
    // manual counting, but the rest of the data that is still required is
    // added from automatic counting. This is result of the different Tasks.
    FillMissingManualCountsDependingOnTask();

    // For the automatically counting facility ...
    fAutomaticPPStatistics.AddEventStatistics(fAutomaticCounting.Primaries());
    fAutomaticPPStatistics.AddSecondaryMultiplicity(fAutomaticCounting.NSecondaries());

    // ... and the manual counting. At the end you compare ;)
    fManualPPStatistics.AddEventStatistics(fManualCounting.Primaries());
    fManualPPStatistics.AddSecondaryMultiplicity(fManualCounting.NSecondaries());
  }

  // Fill the Pt Distributions accordingly to be able to calculate the Raa
  // at the end.
  switch (fCollSys) {
    case ECollisionSystem::kpp7TeV:
    case ECollisionSystem::kpp2_76TeV:
      // Finishing a proton-proton-event does not give information on
      // calculating Raa alone. The calculations only happen for the
      // lead-lead-events.
      break;
    case ECollisionSystem::kPbPbPeripheral: {
      std::cerr << "Peripheral Lead Lead - filling automatic PtDist\n";
      fPtDistPbPbPeripheral.AddEventStatistics(fAutomaticCounting.Primaries());

      RaaCalculator(fRaaPeripheralAuto, fRaaPeripheralManual, kCorrectionPeripheral);
      std::cout << "Raa Peripheral: " << fRaaPeripheralAuto.Raa
                << " - dRaa: " << fRaaPeripheralAuto.dRaa << "\n";
      fAnalysisWidget->UpdateAutoRaaPeripheral(fRaaPeripheralAuto);
      fAnalysisWidget->UpdateManualRaaPeripheral(fRaaPeripheralManual);
    } break;
    case ECollisionSystem::kPbPbSemiCentral: {
      std::cerr << "Semi-Central Lead Lead - filling automatic PtDist\n";
      fPtDistPbPbSemiCentral.AddEventStatistics(fAutomaticCounting.Primaries());

      RaaCalculator(fRaaSemiCentralAuto, fRaaSemiCentralManual, kCorrectionSemiCentral);
      std::cout << "Raa Semi Central: " << fRaaSemiCentralAuto.Raa
                << " - dRaa: " << fRaaSemiCentralAuto.dRaa << "\n";
      fAnalysisWidget->UpdateAutoRaaSemiCentral(fRaaSemiCentralAuto);
      fAnalysisWidget->UpdateManualRaaSemiCentral(fRaaSemiCentralManual);
    } break;
    case ECollisionSystem::kPbPbCentral: {
      std::cerr << "Central Lead Lead - filling automatic PtDist\n";
      fPtDistPbPbCentral.AddEventStatistics(fAutomaticCounting.Primaries());

      RaaCalculator(fRaaCentralAuto, fRaaCentralManual, kCorrectionCentral);
      std::cout << "Raa Central: " << fRaaCentralAuto.Raa << " - dRaa: " << fRaaCentralAuto.dRaa
                << "\n";
      fAnalysisWidget->UpdateAutoRaaCentral(fRaaCentralAuto);
      fAnalysisWidget->UpdateManualRaaCentral(fRaaCentralManual);
    } break;
  }
}

void EventDisplay::TrackLoaded(TEveTrack* Track, Bool_t /*included*/)
{
  Expects(Track != nullptr);

  if (IsPrimary(Track)) {
    fAutomaticCounting.CountPrimary(Track);

    if (IsPrimaryPositive(Track))
      fAutomaticCounting.CountPrimaryPositive(Track);
  } else {
    fAutomaticCounting.CountSecondary(Track);
  }
}

void EventDisplay::FinishedTracks()
{
  fStatisticsWidget->RedrawAll();
  fAnalysisWidget->UpdatePtPlots();
}

void EventDisplay::TrackSelected(TEveTrack* Track)
{
  Expects(Utility::StringToRenderElement(Track->GetElementName()) ==
          Utility::ERenderElements::kESDTrack);

  /// Display information about the track so the student can judge better
  /// what he selected and find out characteristics of the tracks.
  fTrackInfoWidget->UpdateInformation(Track);

  // Only the 'normal' PP events are counted manually.
  if (fCollSys != ECollisionSystem::kpp2_76TeV)
    return;

  // Bailout early if the track has already been selected.
  if (Utility::IsSelected(Track)) {
    PopupWarning(fTranslation.DiagAlreadyCounted());
    return;
  }

  // Bailout early, if it is clear the track selected is not a primary and
  // the task requires primaries only.
  if (fCurrentTask != EActiveTask::kCountSecondaries && !IsPrimary(Track)) {
    PopupWarning(fTranslation.DiagNotPrimary());
    return;
  }

  switch (fCurrentTask) {
    case EActiveTask::kCountPrimaries:
      if (!Utility::IsPrimary(Track)) {
        PopupWarning("This is not a primary track you have selected!");
        return;
      }
      Utility::MarkSelected(Track);
      fManualCounting.CountPrimary(Track);
      break;
    case EActiveTask::kCountSecondaries: {
      if (Utility::IsPrimary(Track)) {
        PopupWarning("This is a primary track you have selected!");
        return;
      }
      Utility::MarkSelected(Track);
      fManualCounting.CountSecondary(Track);
      // Secondary tracks do not get counted for the calculation of Raa.
      // Therefor leaving the function early.
      return;
    }
    case EActiveTask::kCountCharged: {
      // FIXME This should change. Task is to count positive charged particles,
      // which removes half the logic. It will change the Tasks GUI as well.
      if (Track->GetCharge() < 0) {
        PopupWarning("Wrong charge, Please select positive particles!");
        return;
      }
      Utility::MarkSelected(Track);
      fManualCounting.CountPrimaryPositive(Track);
      break;
    }
    case EActiveTask::kFindHighestPT: {
      if (fAutomaticCounting.PrimaryHighestPt() != Track)
        PopupWarning("You did not select the track with Highest Pt!");
      else
        Utility::MarkSelected(Track, kGreen + 2);
      break;
    }
  }
}

void EventDisplay::Export(const TString& out)
{
  TFile* file = TFile::Open(out, "RECREATE");
  fManualPPStatistics.Export(file);

  // FIXME Extract the whole export of the Raa and Multiplicity stuff into
  // single private method.

  /// Extracted from original RaaCalculator.C
  TH1* exportMultiplicty = new TH1D("exportMultiplicity", "", 4, 0, 4);
  exportMultiplicty->GetXaxis()->SetBinLabel(1, "pp");
  exportMultiplicty->GetXaxis()->SetBinLabel(2, "Peripheral");
  exportMultiplicty->GetXaxis()->SetBinLabel(3, "Semi-central");
  exportMultiplicty->GetXaxis()->SetBinLabel(4, "Central");
  exportMultiplicty->SetYTitle("#LTM#GT");

  TH1* exportMultiplicityMinPt =
    static_cast<TH1*>(exportMultiplicty->Clone("explortMultiplicityMinPt"));
  exportMultiplicityMinPt->SetYTitle("#LTM#GT|_{#it{p}_{T}>1GeV/c}");

  TH1* exportRaa = new TH1D("raa", "", 3, 0, 3);
  exportRaa->GetXaxis()->SetBinLabel(1, "Peripheral");
  exportRaa->GetXaxis()->SetBinLabel(2, "Semi-central");
  exportRaa->GetXaxis()->SetBinLabel(3, "Central");
  exportRaa->SetYTitle("#it{R}_{AA}");

  TH1* exportRaaMinPt = static_cast<TH1*>(exportRaa->Clone("raaMinPt"));
  exportRaaMinPt->SetYTitle("#it{R}_{AA}|_{#it{p}_{T}>1GeV/c}");

  exportMultiplicty->SetBinContent(1, fManualPPStatistics.HistMultiplicity().GetMean());
  exportMultiplicty->SetBinContent(2, fPtDistPbPbPeripheral.GetHistogram().GetEntries());
  exportMultiplicty->SetBinContent(3, fPtDistPbPbSemiCentral.GetHistogram().GetEntries());
  exportMultiplicty->SetBinContent(4, fPtDistPbPbCentral.GetHistogram().GetEntries());
  exportRaa->SetBinContent(1, fRaaPeripheralManual.Raa);
  exportRaa->SetBinContent(2, fRaaSemiCentralAuto.Raa);
  exportRaa->SetBinContent(3, fRaaCentralManual.Raa);

  // FIXME The MinPt calculation is not specified because it was not done before.
  exportMultiplicityMinPt->SetBinContent(1, fManualPPStatistics.HistMultiplicityMinPt().GetMean());
  exportMultiplicityMinPt->SetBinContent(2, 0. /*fMMinPt[i]->GetNumber()*/);
  exportMultiplicityMinPt->SetBinContent(3, 0. /*fMMinPt[i]->GetNumber()*/);
  exportMultiplicityMinPt->SetBinContent(4, 0. /*fMMinPt[i]->GetNumber()*/);
  exportRaaMinPt->SetBinContent(1, 0. /*fRAAMin[i]->GetNumber()*/);
  exportRaaMinPt->SetBinContent(2, 0. /*fRAAMin[i]->GetNumber()*/);
  exportRaaMinPt->SetBinContent(3, 0. /*fRAAMin[i]->GetNumber()*/);

  file->cd();
  exportMultiplicty->Write();
  exportMultiplicityMinPt->Write();
  exportRaa->Write();
  exportRaaMinPt->Write();

  file->Write();
  file->Close();
}
} // namespace Raa
