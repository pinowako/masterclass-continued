#ifndef TNAVIGATION_H_CXEE3LYW
#define TNAVIGATION_H_CXEE3LYW

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGFrame.h>

#include "Raa/part1/EActiveTask.h"
#include "Utility/GUITranslation.h"
#include "Utility/INavigation.h"
#include "Utility/NavigationElements.h"

class TBuffer;
class TClass;
class TGButton;
class TGTextButton;
class TMemberInspector;

namespace Utility
{
class EventDisplay;
class TEventAnalyseGUI;
class TEventVisualization;
class VSDReader;
} // namespace Utility

namespace Raa
{
namespace internal
{
struct TDisplay : TGGroupFrame {
  TGTextButton* fButtonIP;
  TGTextButton* fButtonClusters;
  TGTextButton* fButtonTracks;
  TGButton* fShowPrimaries;
  TGHorizontalFrame* fFrameGeometry;
  TGTextButton* fToggleDetector;
  TGTextButton* fButtonAxes;
  TGTextButton* fButtonChangeBackground;

  TDisplay(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation);
  ClassDef(TDisplay, 0);
};

struct THelp : TGGroupFrame {
  TGTextButton* fButtonAliceInfo;

  THelp(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation);

  ClassDef(THelp, 0);
};

struct TTasks : TGGroupFrame {
  TGHorizontalFrame* fTaskRow1;
  TGHorizontalFrame* fTaskRow2;
  TGTextButton* fTaskPrimariesButton;
  TGTextButton* fTaskChargedButton;
  TGTextButton* fTaskSecondariesButton;
  TGTextButton* fTaskPrimariesHighPtButton;

  TTasks(TGCompositeFrame* parent, const Utility::TGUIEnglish& Translation);

  /// @{
  /// @name Task control choices

  /// This slot will change the GUI to the new task and emit the signal
  /// \c SwitchedTask. That signal will be handled by the EventDisplay to check
  /// if the switch was acutally allowed. If not, a warning will be showed and
  /// the GUI will be reset with this method.
  void ChangeTask(Int_t NewTask);

  /// Signal that is emitted after the GUI changed the Task, must be handled
  /// with the constraints of \c ChangeTask() in order to work properly.
  void SwitchedTask(Int_t NewTask); //*SIGNAL*

  /// Toggle the buttons depending on the choosen task.
  void ToggleButtons(EActiveTask ChoosenTask);

  /// @}

 private:
  ClassDef(TTasks, 0);
};
} // namespace internal

class TNavigation : public Utility::INavigation
{
 private:
  // Control the display settings for the Event Display for the Raa class, Optional
  internal::TDisplay* fDisplayRaaFrame;

  // Frame for help, differs between Raa and Strangeness, only one of two will be created
  internal::THelp* fHelpRaa;

  // Frame that control the current Task in Raa
  // Optionally created.
  internal::TTasks* fTasksRaa;

 protected:
  /// Load the initial data to display for the user, beeing tracks, detector
  /// geometry and the intersection point.
  void InitialLoad() override;

 public:
  TNavigation(Utility::EventDisplay* Exercise, Bool_t Cheat);

  void SetupSignalSlots(Utility::TEventAnalyseGUI* AnalysisGUI, Utility::TEventVisualization *EventVisualization,
                        Utility::VSDReader* Reader) override;

  void SetEventNumber(Int_t Num, Int_t Max) override;

  void SetTask(Int_t NewTask) override { fTasksRaa->ToggleButtons((EActiveTask)NewTask); }

  ClassDefOverride(TNavigation, 0);
};
} // namespace Raa

#endif /* end of include guard: TNAVIGATION_H_CXEE3LYW */
