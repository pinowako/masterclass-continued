#include "TRaaStatistics.h"

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TDirectory.h>
#include <TH1.h>
#include <TMath.h>
#include <TObject.h>
#include <TString.h>
#include <TVirtualPad.h>

#include "Raa/ContentGUITranslation.h"
#include "Raa/part1/TrackClassification.h"
#include "Utility/LanguageProvider.h"

namespace Raa
{
TPtDistribution::TPtDistribution(const TString& PostfixHistogram)
  : fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
  , fPtHist(fTranslation.StatisticsPT() + " - " + PostfixHistogram,
            Form("p_{T} %s", fTranslation.Distribution().Data()), 50, 0., 6.)
{
  fPtHist.SetLineColor(3);
  fPtHist.GetXaxis()->SetTitle("p_{T} (GeV/c)");
  fPtHist.GetYaxis()->SetTitle(fTranslation.Counts());
  fPtHist.SetFillColor(0);
}

void TPtDistribution::AddEventStatistics(const gsl::span<TEveTrack* const> Primaries)
{
  for (const auto* T : Primaries)
    fPtHist.Fill(GetPt(T));
}

TRaaStatistics::TRaaStatistics(const TString& PostfixHistogram)
  : fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
  , fMultiplicityHist(fTranslation.StatisticsMult() + " - " + PostfixHistogram,
                      fTranslation.MultiplicityDist(), 10, 0., 50.)
  , fMultHistMinPt(
      Form("%s pt > 1.0 - %s", fTranslation.StatisticsMult().Data(), PostfixHistogram.Data()),
      Form("%s pt > 1.0 GeV/c - %s", fTranslation.StatisticsMult().Data(), PostfixHistogram.Data()),
      10, 0., 50.)
  , fMultHistSec("Statistics Sec - " + PostfixHistogram, "Multiplicity of Secondaries", 20, 0., 20.)
  , fPtHist(fTranslation.StatisticsPT(), Form("p_{T} %s", fTranslation.Distribution().Data()), 50,
            0., 20.)
  , fChargeHist(fTranslation.StatisticsCharge() + " - " + PostfixHistogram,
                fTranslation.ChargeDistribution(), 5, -2.5, 2.5)
  , fPhiDist("Phi projection - " + PostfixHistogram, "Normalized #phi projection", 72, -TMath::Pi(),
             TMath::Pi())
{
  fMultiplicityHist.SetLineColor(2);
  fMultiplicityHist.GetXaxis()->SetTitle(
    Form("%s (# TPC %s)", fTranslation.Multiplicity().Data(), fTranslation.Tracks().Data()));
  fMultiplicityHist.GetYaxis()->SetTitle(fTranslation.Counts());
  fMultiplicityHist.SetFillColor(0);

  fMultHistSec.SetLineColor(kGreen + 2);
  fMultHistSec.GetXaxis()->SetTitle(" #TPC tracks");
  fMultHistSec.GetYaxis()->SetTitle("Counts");
  fMultHistSec.SetFillColor(0);

  fPtHist.SetLineColor(3);
  fPtHist.GetXaxis()->SetTitle("p_{T} (GeV/c)");
  fPtHist.GetYaxis()->SetTitle(fTranslation.Counts());
  fPtHist.SetFillColor(0);

  fChargeHist.SetLineColor(4);
  fChargeHist.GetXaxis()->SetTitle(fTranslation.Charge());
  fChargeHist.GetYaxis()->SetTitle(fTranslation.Counts());
  fChargeHist.SetFillColor(0);

  fPhiDist.GetXaxis()->SetTitle("#phi");
  fPhiDist.GetYaxis()->SetTitle("Counts");
  fPhiDist.SetLineColor(kRed);
  fPhiDist.SetFillColor(0);
}

void TRaaStatistics::AddEventStatistics(const gsl::span<TEveTrack* const> Primaries)
{
  fMultiplicityHist.Fill(Primaries.size());

  // Ensure that the data can be hold properly when converting between the
  // types of counting and filling the histogram.
  static_assert(std::numeric_limits<Double_t>::max() >
                  std::numeric_limits<gsl::span<TEveTrack* const>::difference_type>::max(),
                "No dataloss when filling histogram");
  // Constant defining the threshold for primary tracks to count as high pt.
  // Can be a parameter in the future.
  constexpr Double_t PtThreshold = 1.;
  const Double_t NHighPt =
    std::count_if(std::begin(Primaries), std::end(Primaries),
                  [&](const TEveTrack* const T) { return GetPt(T) > PtThreshold; });
  fMultHistMinPt.Fill(NHighPt);

  for (const auto* T : Primaries) {
    fPtHist.Fill(GetPt(T));
    fChargeHist.Fill(T->GetCharge());
    fPhiDist.Fill(T->GetMomentum().Phi());
  }
}

void TRaaStatistics::AddSecondaryMultiplicity(Int_t NumberSecondaries)
{
  fMultHistSec.Fill(NumberSecondaries);
}

void TRaaStatistics::ClearStatistics()
{
  fMultiplicityHist.Reset();
  fMultHistMinPt.Reset();
  fMultHistSec.Reset();

  fChargeHist.Reset();
  fPhiDist.Reset();
}

void TRaaStatistics::Export(gsl::not_null<TFile*> f)
{
  f->cd();
  fMultiplicityHist.Write();
  fMultHistMinPt.Write();
  fPtHist.Write();
  fChargeHist.Write();
  fMultHistSec.Write();
  fPhiDist.Write();
}

std::pair<Double_t, Double_t> CalcRaa(Double_t MeanPbPb, Double_t MeanPP, Double_t Correction,
                                      Double_t PbPbRMS, Double_t PPRMS)
{
  Expects(std::isnormal(MeanPbPb));
  Expects(std::isnormal(MeanPP));
  Expects(std::isnormal(Correction));
  Expects(std::isnormal(PbPbRMS));
  Expects(std::isnormal(PPRMS));

  const auto Raa = MeanPbPb / (MeanPP * Correction);

  Ensures(std::isnormal(Raa));
#if 0
  Ensures(Raa < 1.2);
  Ensures(Raa > 0.0);
#endif

  const auto rel_a = PPRMS / MeanPP;
  Expects(std::isnormal(rel_a));
  const auto rel_b = PbPbRMS / MeanPbPb;
  Expects(std::isnormal(rel_b));
  const auto Unc = Raa * sqrt(rel_a * rel_a + rel_b * rel_b);
  Ensures(std::isnormal(Unc));

  return std::make_pair(Raa, Unc);
}

std::pair<Double_t, Double_t> RaaFor(const Double_t LeadMultiplicity,
                                     const TH1D& ProtonMultiplicity,
                                     const Double_t CorrectionFactor) noexcept
{
  return CalcRaa(LeadMultiplicity, ProtonMultiplicity.GetMean(), CorrectionFactor,
                 std::sqrt(LeadMultiplicity), ProtonMultiplicity.GetMean());
}

} // namespace Raa
