#ifndef UTILITIES_CXX_1SWTTCNU
#define UTILITIES_CXX_1SWTTCNU

#include "Utilities.h"

#include <RtypesCore.h>
#include <TArrayD.h>
#include <TAxis.h>
#include <TClass.h>
#include <TCollection.h>
#include <TDirectory.h>
#include <TError.h>
#include <TFile.h>
#include <TGComboBox.h>
#include <TGaxis.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TLegend.h>
#include <TLine.h>
#include <TList.h>
#include <TMathBase.h>
#include <TObject.h>
#include <TString.h>
#include <TStyle.h>
#include <TTree.h>
#include <TVirtualPad.h>
#include <iostream>

#include "Rtypes.h"
#include "Utility/Utilities.h"

namespace Raa
{
void SetupBinComboBox(TGComboBox& Box)
{
  Box.AddEntry("0-5%", 1);
  Box.AddEntry("5-10%", 2);
  Box.AddEntry("10-20%", 3);
  Box.AddEntry("20-30%", 4);
  Box.AddEntry("30-40%", 5);
  Box.AddEntry("40-50%", 6);
  Box.AddEntry("50-60%", 7);
  Box.AddEntry("60-70%", 8);
  Box.AddEntry("70-80%", 9);
  Box.Resize(150, 20);
}

Color_t ChooseColor(int ID)
{
  Color_t c = kBlack;
  switch (ID % 7) {
    case 0:
      c = kPink + 2;
      break;
    case 1:
      c = kRed + 2;
      break;
    case 2:
      c = kOrange + 2;
      break;
    case 3:
      c = kYellow + 2;
      break;
    case 4:
      c = kGreen + 2;
      break;
    case 5:
      c = kBlue + 2;
      break;
    case 6:
      c = kViolet + 2;
      break;
  }
  return c;
}

void StyleSettings()
{
  // gStyle->SetOptTitle(kFALSE);
  gStyle->SetOptDate(0); // show day and time
  gStyle->SetOptStat(0); // show statistic
  gStyle->SetPalette(1, nullptr);
  gStyle->SetFrameBorderMode(0);
  gStyle->SetFrameFillColor(0);
  gStyle->SetTitleFillColor(0);
  gStyle->SetTextSize(0.5);
  gStyle->SetLabelSize(0.03, "xyz");
  gStyle->SetLabelOffset(0.002, "xyz");
  gStyle->SetTitleFontSize(0.04);
  gStyle->SetTitleOffset(1.7, "y");
  gStyle->SetTitleOffset(0.7, "x");
  gStyle->SetCanvasColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetLineWidth(1);
  gStyle->SetPadTopMargin(0.06);
  gStyle->SetPadBottomMargin(0.09);
  gStyle->SetPadRightMargin(0.04);
  gStyle->SetPadLeftMargin(0.1);
  gStyle->SetOptStat(1);

  TGaxis::SetMaxDigits(3);
}

void SetAttributes(TH1* histo1, Style_t markerStyle, Size_t markerSize, Color_t markerColor,
                   Color_t lineColor)
{
  histo1->SetMarkerColor(markerColor);
  histo1->SetMarkerSize(markerSize);
  histo1->SetMarkerStyle(markerStyle);

  histo1->SetLineColor(lineColor);
}

TFile* OpenInput(const char* filename)
{
  TString fname(filename);
  if (fname.IsNull()) {
    fname = Utility::ResourcePath("data/Raa/MasterClassesTree_LHC10h_Run139036.root");
  }
  TFile* file = TFile::Open(fname, "READ");
  if (file == nullptr) {
    Error("OpenInput", "Couldn't open the file %s", fname.Data());
    return nullptr;
  }
  return file;
}

/**
 * Open the output file
 *
 * @param read     If true, open for reading
 * @param filename File name
 *
 * @return Pointer to the file, or null
 */
TFile* OpenOutput(Bool_t read, const char* filename)
{
  TFile* file = TFile::Open(filename, read ? "READ" : "UPDATE");
  if (file == nullptr) {
    Error("OpenOutput", "Couldn't open the file %s", filename);
    return nullptr;
  }
  return file;
}

/**
 * Open the pp file
 *
 * @param filename File name
 *
 * @return
 */
TFile* OpenPP(const char* filename)
{
  TString fname(filename);
  if (fname.IsNull()) {
    fname = Utility::ResourcePath("data/Raa/PP_2760GeV_BaseLine.root");
  }
  TFile* file = Utility::File("", fname);
  if (file == nullptr) {
    Error("OpenPP", "Couldn't open the file %s", fname.Data());
    return nullptr;
  }
  return file;
}

/**
 * Open the output file
 *
 * @param read     If true, open for reading
 * @param filename File name
 *
 * @return Pointer to the file, or null
 */
TFile* OpenResult(Bool_t read, const char* filename)
{
  TFile* file = TFile::Open(filename, read ? "READ" : "UPDATE");
  if (file == nullptr) {
    Error("OpenOutput", "Couldn't open the file %s", filename);
    return nullptr;
  }
  return file;
}

/**
 * Get a pointer to an object from a directory.  Optionally, if @a cls
 * is non-null, we check that the object has the expected type.
 *
 * @param dir   Directory to read from
 * @param name  Name of the object
 * @param cls   Option class to check against
 *
 * @return Pointer to the object if found and (optionally) is of the
 * right type.
 */
TObject* GetObject(TDirectory* dir, const char* name, TClass* cls)
{
  if (dir == nullptr) {
    Error("GetObject", "No directory passed");
    return nullptr;
  }
  if ((name == nullptr) || name[0] == '\0') {
    Error("GetObject", "No name given");
    return nullptr;
  }
  TObject* o = dir->Get(name);
  if (o == nullptr) {
    Error("GetObjet", "Object %s not found in %s", name, dir->GetName());
    return nullptr;
  }
  if (cls != nullptr) {
    if (!o->IsA()->InheritsFrom(cls)) {
      Error("GetObject", "Object %s read from %s is not a %s but a %s", o->GetName(),
            dir->GetName(), cls->GetName(), o->ClassName());
      return nullptr;
    }
  }
  return o;
}

TTree* GetEventTree(TDirectory* dir, Float_t* centrality, Int_t* mult)
{
  auto* t = Get<TTree>(dir, "Event");
  if (t == nullptr) {
    return nullptr;
  }

  t->SetBranchAddress("nTPCAccepted", mult);
  t->SetBranchAddress("centrality", centrality);
  return t;
}

TTree* GetTrackTree(TDirectory* dir, Float_t* centrality, Double_t* pt)
{
  auto* t = Get<TTree>(dir, "Track");
  if (t == nullptr) {
    return nullptr;
  }

  t->SetBranchAddress("trackpt", pt);
  t->SetBranchAddress("centrality", centrality);
  return t;
}

/**
 * Get the maximum number of entries to analyse
 *
 * @param t    Tree
 * @param test If true, restrict to 1000 entries
 *
 * @return The number of entries to analyse
 */
ULong64_t MaxEntries(TTree* t, Bool_t test)
{
  ULong64_t n = t->GetEntries();
  if (!test) {
    Info("MaxEntries", "A total of %llu entries in tree %s", n, t->GetName());
    return n;
  }
  ULong64_t m = TMath::Min(n, 1000ULL);
  Info("MaxEntries",
       "For testing we will analyse %llu events (out of %llu) "
       "in tree %s",
       m, n, t->GetName());
  return m;
}

/**
 * Format a histogram name
 *
 * @param prefix        Prefix
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Temporary string
 */
TString HistogramName(const char* prefix, Int_t minCentrality, Int_t maxCentrality)
{
  return Form("%s_%03d_%03d", prefix, minCentrality, maxCentrality);
}

/**
 * Format a histogram name
 *
 * @param prefix        Prefix
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 * @param postfix       Possible postfix
 *
 * @return Temporary string
 */
TString HistogramTitle(const char* prefix, Int_t minCentrality, Int_t maxCentrality,
                       const char* postfix)
{
  return Form("%s%3d-%3d%%%s", prefix, minCentrality, maxCentrality, postfix);
}

/**
 * Get the colour corresponding to a centrality range
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return
 */
Color_t CentralityColor(Int_t minCentrality, Int_t maxCentrality)
{
  Int_t c = (maxCentrality + minCentrality) / 2;
  if (c < 5) {
    return kBlue + 4;
  }
  if (c < 10) {
    return kBlue;
  }
  if (c < 20) {
    return kBlue - 4;
  }
  if (c < 30) {
    return kBlue - 9;
  }
  if (c < 40) {
    return kGreen + 4;
  }
  if (c < 50) {
    return kGreen - 2;
  }
  if (c < 60) {
    return kGreen;
  }
  if (c < 70) {
    return kGreen + 9;
  }
  if (c < 80) {
    return kYellow + 1;
  }
  return kRed + 1;
}
/**
 * Get an array of pT bins
 *
 * @return Array of pT bins
 */
TArrayD PtBins()
{
  Double_t borders[55] = { 0.00, 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50,
                           0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.00, 1.10,
                           1.20, 1.30, 1.40, 1.50, 1.60, 1.70, 1.80, 1.90, 2.00, 2.20, 2.40,
                           2.60, 2.80, 3.00, 3.20, 3.40, 3.60, 3.80, 4.00, 4.50, 5.00, 5.50,
                           6.00, 6.50, 7.00, 8.00, 9.00, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0 };
  return TArrayD(sizeof(borders), borders);
}
/**
 * Find a bin number corresponding to a pT value
 *
 * @param pt pT value
 *
 * @return bin number
 */
Int_t PtBin(Double_t pt)
{
  TArrayD bins = PtBins();
  for (Int_t i = 1; i < bins.GetSize(); i++) {
    if (pt >= bins[i - 1] && pt < bins[i]) {
      return i;
    }
  }
  return bins.GetSize();
}

/**
 * Deduce the pT style
 *
 * @param i pT bin number
 *
 * @return Marker style, alternating between filled and hollow
 */
Style_t PtStyle(Int_t i)
{
  Style_t s = 20;
  switch ((i / 2) % 7) {
    case 0:
    case 1:
    case 2:
      s = 20 + ((i / 2) % 7);
      break;
    case 3:
      s = 23;
      break;
    case 4:
      s = 29;
      break;
    case 5:
      s = 33;
      break;
    case 6:
      s = 34;
      break;
  }
  if (i % 2 == 1) {
    // Flip to hollow style
    switch (s) {
      case 20:
      case 21:
      case 22:
        s += 4;
        break;
      case 23:
        s += 9;
        break;
      case 29:
        s += 1;
        break;
      case 33:
      case 34:
        s -= 6;
        break;
      default:
        s = 24;
        break;
    }
  }
  return s;
}

/**
 * Make a pT distribution histogram
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to newly allocated histogram
 */
TH1* MakePtHistogram(Int_t minCentrality, Int_t maxCentrality)
{
  TArrayD borders = PtBins();
  TH1* h = new TH1D(HistogramName("pt", minCentrality, maxCentrality),
                    HistogramTitle("", minCentrality, maxCentrality), borders.GetSize() - 1,
                    borders.GetArray());

  h->SetXTitle("#it{p}_{T} (GeV/c)");
  h->SetYTitle("d#it{N}/d#it{p}_{T} (c/GeV)");

  h->SetDirectory(nullptr);

  Color_t color = CentralityColor(minCentrality, maxCentrality);
  SetAttributes(h, 20, 1, color, color);
  h->Sumw2();

  return h;
}

/**
 * Get pT histogram from a directory.  If @a minCentrality is smaller
 * than @a maxCentrality, assume we're getting a pp distribution.
 *
 * @param dir           Directory
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to the histogram, or null
 */
TH1* GetPtHistogram(TDirectory* dir, Int_t minCentrality, Int_t maxCentrality)
{
  if (minCentrality > maxCentrality) {
    auto* h = Get<TH1>(dir, "trackPt_pp");
    if (h == nullptr) {
      return nullptr;
    }
    h->SetName("pt_pp");
    h->SetTitle("pp min. bias");
    SetAttributes(h, 24, 1.2, kRed + 1, kRed + 1);
    return h;
  }
  return Get<TH1>(dir, HistogramName("pt", minCentrality, maxCentrality));
}

/**
 * Make a multiplicity histogram
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to newly created histogram
 */
TH1* MakeMultHistogram(Int_t minCentrality, Int_t maxCentrality)
{
  TH1* h = new TH1D(HistogramName("mult", minCentrality, maxCentrality),
                    HistogramTitle("", minCentrality, maxCentrality), 200, 0, 2000);
  h->SetXTitle("#it{N}_{tracks}");
  h->SetYTitle("P(#it{N}_{tracks})");

  h->SetDirectory(nullptr);
  h->Sumw2();

  Color_t color = CentralityColor(minCentrality, maxCentrality);
  SetAttributes(h, 20, 1, color, color);
  return h;
}

/**
 * Get multiplicity histogram from a directory. If @a minCentrality is
 * smaller than @a maxCentrality, assume we're getting a pp
 * distribution.
 *
 * @param dir           Directory
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to the histogram, or null
 */
TH1* GetMultHistogram(TDirectory* dir, Int_t minCentrality, Int_t maxCentrality)
{
  if (minCentrality > maxCentrality) {
    auto* h = Get<TH1>(dir, "nTracksTPC_pp");
    if (h == nullptr) {
      return nullptr;
    }

    h->SetName("mult_pp");
    h->SetTitle("pp min. bias");
    SetAttributes(h, 24, 1.2, kRed + 1, kRed + 1);
    return h;
  }
  return Get<TH1>(dir, HistogramName("mult", minCentrality, maxCentrality));
}

/**
 * Make the multiplicity versus centrality correlation histogram
 *
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to newly allocated histogram
 */
TH2* MakeMultCentHistogram(Int_t minCentrality, Int_t maxCentrality)
{
  TH2* h = new TH2D(HistogramName("multVsCent", minCentrality, maxCentrality),
                    "#it{N}_{TPC tracks} vs Centrality", 200, 0, 2000, 100, 0, 100);
  h->SetXTitle("#it{N}_{tracks}");
  h->SetYTitle("Centrality (%)");
  h->SetZTitle("P(#it{N}_{tracks})");

  Color_t color = CentralityColor(minCentrality, maxCentrality);
  SetAttributes(h, 20, 1, color, color);
  return h;
}

/**
 * Get multiplicity histogram from a directory
 *
 * @param dir           Directory
 * @param minCentrality Least centrality
 * @param maxCentrality Largest centrality
 *
 * @return Pointer to the histogram, or null
 */
TH2* GetMultCentHistogram(TDirectory* dir, Int_t minCentrality, Int_t maxCentrality)
{
  return Get<TH2>(dir, HistogramName("multVsCent", minCentrality, maxCentrality));
}

/**
 * Replace all special charaters in passed string
 *
 * @param who String to escape
 *
 * @return The escaped string
 */
TString EscapeWho(const char* who)
{
  TString w;
  w = who;
  TString rpl(" /-@:;.,");
  for (Int_t i = 0; i < rpl.Length(); i++) {
    w.ReplaceAll(rpl(i, 1), "_");
  }
  return w;
}

/**
 * Encode pT specific centrality histogram name
 *
 * @param pt     pT value
 * @param prefix Possible prefix to use
 *
 * @return The name
 */
TString EncodePtHistName(Double_t pt, const char* prefix)
{
  TString tmp;
  tmp.Form("%s_%03dd%03d", prefix, Int_t(pt), Int_t(pt * 1000) % 1000);
  return tmp;
}
/**
 * Encode pT specific centrality histogram name
 *
 * @param ptBin  pT bin
 * @return The name
 */
TString EncodePtHistName(Int_t ptBin)
{
  Double_t pt = PtBins()[ptBin - 1];
  return EncodePtHistName(pt);
}

/**
 * Make a centrality histogram
 *
 * @param color Color to use
 * @param pt    pT bin
 * @param who   Who did this
 *
 * @return Pointer to the histogram
 */
TH1* MakeCentHistogram(Color_t color, Double_t pt, const char* who)
{
  Style_t style = PtStyle(PtBin(pt));
  TString esc = EscapeWho(who);
  TString nme = EncodePtHistName(pt);
  Double_t bins[] = { 0, 5, 10, 20, 30, 40, 50, 60, 70, 80 };

  TH1* hist = new TH1D(nme, who, 9, bins);
  hist->SetXTitle("Centrality (%)");
  hist->SetYTitle("#it{R}_{AA}(#it{p}_{T})");

  hist->SetMarkerColor(color);
  hist->SetMarkerSize(2);
  hist->SetMarkerStyle(style);

  hist->SetLineColor(color);

  hist->SetDirectory(nullptr);
  hist->SetBinContent(0, pt); // Store pt in underflow

  return hist;
}

/**
 * Read an entry from a tree
 *
 * @param t     Tree to read from
 * @param entry Entry offset
 * @param max   Maximum number of entries to get
 *
 * @return true on success, false otherwise
 */
Bool_t ReadEntry(TTree* t, ULong64_t entry, ULong64_t max)
{
  if (t == nullptr) {
    Warning("ReadEntry", "No tree to read from");
    return false;
  }
  Int_t bytes = t->GetEntry(entry);
  if (bytes <= 0) {
    Warning("ReadEntry", "Short read %llu (expected %llu) from tree %s", entry + 1, max,
            t->GetName());
    return false;
  }
  return true;
}

/**
 * Check if the centrality is within (@a min, @a max)
 *
 * @param centrality Read centrality
 * @param min        Least centrality
 * @param max        Largest centrality
 *
 * @return true if centrality is within the specified range
 */
Bool_t CheckCentrality(Float_t centrality, Int_t min, Int_t max)
{
  return (centrality > min && centrality < max);
}

/**
 * Scale a histogram by number of binary collisions
 *
 * @param h
 * @param scale
 *
 * @return
 */
TH1* Scale(TH1* h, Double_t scale)
{
  h->Scale(1. / scale);
  h->SetYTitle(Form("1/#LT#it{N}_{coll}#GT %s", h->GetYaxis()->GetTitle()));
  return h;
}

/**
 * Make a copy of @a in, remove the last element (presumably pp or
 * most peripheral event class), and divide all histograms in the copy
 * by the removed element.
 *
 * @param in   Input stack of histograms
 * @param name Name for new stack
 * @param rcp  If true, mark as @f$ R_{\mathrm{CP}}@f$
 *
 * @return Newly allocated stack
 */
THStack* Ratio(THStack* in, const char* name, Bool_t rcp)
{
  auto* out = dynamic_cast<THStack*>(in->Clone(name));
  auto* dev = dynamic_cast<TH1*>(out->GetHists()->Last());
  out->GetHists()->Remove(dev);

  if (rcp) {
    // Remove one more - the peripheral event class
    dev = dynamic_cast<TH1*>(out->GetHists()->Last());
    out->GetHists()->Remove(dev);
  }
  Info("Ratio", "Dividing all histograms of %s with %s", in->GetName(), dev->GetName());

  TIter next(out->GetHists());
  TH1* num = nullptr;
  while ((num = dynamic_cast<TH1*>(next())) != nullptr) {
    num->Divide(dev);
    num->SetYTitle(rcp ? "#it{R}_{CP}" : "#it{R}_{AA}");
  }
  dev = dynamic_cast<TH1*>(out->GetHists()->Last());

  auto* l = new TLine(0, 1, 15, 1);
  l->SetLineColor(kGray + 2);
  l->SetLineStyle(2);

  dev->GetListOfFunctions()->Add(l);

  return out;
}

namespace
{
void ConfigureHistogram(TH1* h, double tbase, TVirtualPad* p)
{
  h->GetXaxis()->SetNdivisions(210);
  h->GetYaxis()->SetNdivisions(210);

  h->GetXaxis()->SetLabelSize(tbase / p->GetHNDC());
  h->GetYaxis()->SetLabelSize(tbase / p->GetHNDC());

  h->GetXaxis()->SetTitleSize(tbase / p->GetHNDC());
  h->GetYaxis()->SetTitleSize(tbase / p->GetHNDC());

  h->GetXaxis()->SetTitleOffset(1.1);
  h->GetYaxis()->SetTitleOffset(1.1);
}

} // namespace

/**
 * Draw a stack of histograms
 *
 * @param stack  Stack to draw
 * @param p      Pad to draw in
 * @param logy   If true, use logarithmic Y scale
 * @param leg    If true, make a legend
 */
void DrawStack(THStack* stack, TVirtualPad* p, Bool_t logy, Int_t leg)
{
  auto* first = dynamic_cast<TH1*>(stack->GetHists()->First());

  p->SetTicks();
  p->SetRightMargin(0.01);

  if (logy) {
    p->SetLogy();
  }

  stack->Draw("nostack");
  Double_t tbase = 0.02;

  TH1* h = stack->GetHistogram();
  h->SetXTitle(first->GetXaxis()->GetTitle());
  h->SetYTitle(first->GetYaxis()->GetTitle());
  ConfigureHistogram(h, tbase, p);

  if (leg > 0) {
    TLegend* l = p->BuildLegend(1 - p->GetRightMargin() - .5, 1 - p->GetTopMargin() - .5,
                                1 - p->GetRightMargin() - .02, 1 - p->GetTopMargin() - .02);
    l->SetBorderSize(0);
    l->SetFillStyle(0);
    l->SetNColumns(leg);
  }
  p->Modified();
}

/**
 * Draw a 2D histogram
 *
 * @param h     Histogram
 * @param p     Pad
 * @param logy  If true, use logarithmic Z scale
 * @param tbase Base size for text
 */
void DrawH1(TH1* h, TVirtualPad* p, Bool_t logy, Double_t tbase)
{
  p->SetTicks();
  if (logy) {
    p->SetLogy();
  }

  h->GetXaxis()->SetNoExponent();
  ConfigureHistogram(h, tbase, p);

  h->Draw("colz");
  p->Modified();
}

/**
 * Draw a 2D histogram
 *
 * @param h     Histogram
 * @param p     Pad
 * @param logz  If true, use logarithmic Z scale
 * @param tbase Base size for text
 */
void DrawH2(TH2* h, TVirtualPad* p, Bool_t logz, Double_t tbase)
{
  p->SetTicks();
  if (logz) {
    p->SetLogz();
  }

  h->GetXaxis()->SetNoExponent();

  h->GetXaxis()->SetNdivisions(210);
  h->GetYaxis()->SetNdivisions(210);
  h->GetZaxis()->SetNdivisions(210);

  h->GetXaxis()->SetLabelSize(tbase / p->GetHNDC());
  h->GetYaxis()->SetLabelSize(tbase / p->GetHNDC());
  h->GetZaxis()->SetLabelSize(tbase / p->GetHNDC());

  h->GetXaxis()->SetTitleSize(tbase / p->GetHNDC());
  h->GetYaxis()->SetTitleSize(tbase / p->GetHNDC());
  h->GetZaxis()->SetTitleSize(tbase / p->GetHNDC());

  h->GetXaxis()->SetTitleOffset(1.1);
  h->GetYaxis()->SetTitleOffset(1.1);
  h->GetZaxis()->SetTitleOffset(1.1);

  h->Draw("colz");
  p->Modified();
}

/**
 * Print progress
 *
 * @param cur  Current entry
 * @param freq Frequency of update
 */
void Progress(Int_t cur, Int_t freq)
{
  if (((cur + 1) % freq) == 0) {
    std::cout << '.' << std::flush;
  }
}
} // namespace Raa

#endif /* end of include guard: UTILITIES_CXX_1SWTTCNU */
