/**
 * @file   Control.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 14:02:24 2017
 *
 * @brief  Control panel for second part of the exercise
 *
 * @ingroup alice_masterclass_raa_part2
 */
#ifndef RAACONTROL_C
#define RAACONTROL_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGFrame.h>

#include "Raa/ContentGUITranslation.h"
#include "Utility/Exercise.h"
#include "Utility/LogoButtons.h"

class TBuffer;
class TClass;
class TGButton;
class TGComboBox;
class TMemberInspector;

namespace Utility
{
class Exercise;
} // namespace Utility

namespace Raa
{
struct TGUIEnglish;
/**
 * Control for second part of the exercise.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct Control : public TGMainFrame {
  TGComboBox* fBin;
  TGButton* fGo;
  TGButton* fRead;
  Int_t fMode;
  Int_t fCent;
  const TGUIEnglish& fTranslation;

  Control(Utility::Exercise* e, Bool_t cheat);

  void Mode(Int_t mode);
  void Cent(Int_t cent);

  ClassDef(Control, 0);
};

} // namespace Raa
#endif
