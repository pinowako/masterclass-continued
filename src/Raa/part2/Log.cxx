#include "Log.h"

#include <TGClient.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TGTextView.h>
#include <TSystem.h>
#include <cstdio>

#include <TString.h>

namespace Raa
{
Log::Log()
  : TGMainFrame(gClient->GetRoot(), 0, 0)
  , fFile("XXXXXX")
{
  FILE* file = gSystem->TempFileName(fFile);
  fclose(file);

  fView = new TGTextView(this, 10, 10, 1);
  AddFrame(fView, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  Layout();
  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}

Guard::Guard(Log* log)
  : fLog(log)
{
  gSystem->RedirectOutput(fLog->fFile);
}
Guard::~Guard()
{
  gSystem->RedirectOutput(nullptr);
  if (fLog->fView == nullptr) {
    return;
  }
  fLog->fView->LoadFile(fLog->fFile);
  fLog->fView->ShowBottom();
}
} // namespace Raa
