#include <RtypesCore.h>
#include <TGButton.h>
#include <TGClient.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGLayout.h>
#include <TString.h>

#include "Raa/part2/Control.h"
#include "Raa/ContentGUITranslation.h"
#include "Utility/LanguageProvider.h"
#include "Utility/LogoButtons.h"

namespace Utility
{
class Exercise;
} // namespace Utility

namespace Raa
{
Control::Control(Utility::Exercise* e, Bool_t cheat)
  : TGMainFrame(gClient->GetRoot(), 0, 0, kVerticalFrame)
  , fBin(nullptr)
  , fGo(nullptr)
  , fRead(nullptr)
  , fMode(0)
  , fCent(100)
  , fTranslation(Utility::TranslationFromEnv<ContentLanguage>())
{
  auto* gf = new TGGroupFrame(this, fTranslation.ModeAndCentrality().Data(), kVerticalFrame);
  auto* cb = new TGComboBox(gf);
  cb->AddEntry(fTranslation.SelectMode().Data(), 0);
  cb->AddEntry(fTranslation.AnalyseCentralityBin().Data(), 1);
  cb->AddEntry(fTranslation.CalculateRAA().Data(), 2);
  cb->Resize(150, 20);
  cb->Select(0, false);
  cb->Connect("Selected(Int_t)", "Raa::Control", this, "Mode(Int_t)");
  gf->AddFrame(cb, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 2));

  fBin = new TGComboBox(gf);
  fBin->AddEntry(fTranslation.SelectBin().Data(), 100);
  fBin->AddEntry("0-5%", 0);
  fBin->AddEntry("5-10%", 1);
  fBin->AddEntry("10-20%", 2);
  fBin->AddEntry("20-30%", 3);
  fBin->AddEntry("30-40%", 4);
  fBin->AddEntry("40-50%", 5);
  fBin->AddEntry("50-60%", 6);
  fBin->AddEntry("60-70%", 7);
  fBin->AddEntry("70-80%", 8);
  fBin->AddEntry(fTranslation.All().Data(), 9);
  fBin->Resize(150, 20);
  fBin->Connect("Selected(Int_t)", "Raa::Control", this, "Cent(Int_t)");
  fBin->Select(100, kFALSE);
  fBin->SetEnabled(false);
  gf->AddFrame(fBin, new TGLayoutHints(kLHintsExpandX, 5, 5, 2, 5));
  AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 2, 2, 5, 2));

  gf = new TGGroupFrame(this, fTranslation.Operations(), kVerticalFrame);
  fGo = new TGTextButton(gf, fTranslation.ButtonStart().Data());
  fGo->Connect("Clicked()", "Raa::Exercise", e, "Go()");
  fGo->SetEnabled(false);
  gf->AddFrame(fGo, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 2));

  fRead = new TGTextButton(gf, fTranslation.ButtonReadValues());
  fRead->Connect("Clicked()", "Raa::Exercise", e, "Values()");
  fRead->SetEnabled(false);
  gf->AddFrame(fRead, new TGLayoutHints(kLHintsExpandX, 5, 5, 2, 2));

  AddFrame(gf, new TGLayoutHints(kLHintsExpandX, 2, 2, 2, 5));

  new Utility::LogoButtons(this, e, cheat);

  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();
}
void Control::Mode(Int_t mode)
{
  fMode = mode;
  fBin->SetEnabled(mode == 1);
  fGo->SetEnabled(mode == 2);
  fRead->SetEnabled(mode == 2);
}
void Control::Cent(Int_t cent)
{
  fCent = cent;
  fGo->SetEnabled(cent >= 0 && cent <= 10);
}
} // namespace Raa
