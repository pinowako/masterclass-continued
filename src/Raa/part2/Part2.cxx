#include "Part2.h"

#include <RtypesCore.h>
#include <TBrowser.h>

#include "Raa/part2/Exercise.h"
#include "Utility/Browser.h"

namespace Raa
{
void TPeakBackground::RunExercise(Bool_t AllowAuto)
{
  Utility::Browser* b = new Utility::Browser("RAA, 2nd exercise");
  b->Setup(new Exercise, AllowAuto);
  new TBrowser("RAA2", "", b);
}
} // namespace Raa
