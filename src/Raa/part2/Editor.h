/**
 * @file   Editor.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Fri Mar 17 13:08:15 2017
 *
 * @brief  Editor used in RAA 2nd exercise
 *
 * @ingroup alice_masterclass_raa_part2
 */

#ifndef RAAEDITOR_C
#define RAAEDITOR_C

#include <Rtypes.h>
#include <RtypesCore.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TGMenu.h>
#include <TGMsgBox.h>
#include <TGStatusBar.h>
#include <TGTextEdit.h>
#include <TGTextEditDialogs.h>
#include <TGToolBar.h>
#include <TString.h>
#include <TSystem.h>

#include "Raa/ContentGUITranslation.h"

class TBuffer;
class TClass;
class TGPopupMenu;
class TGStatusBar;
class TGTextEdit;
class TGToolBar;
class TMemberInspector;

namespace Raa
{
struct TGUIEnglish;
//====================================================================
/**
 * Script editor.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct Editor : public TGMainFrame {
  enum {
    kSave = 1,
    kCut,
    kCopy,
    kPaste,
    kDelete,
    kFind,
    kFindNext,
    kGoto,
    kCompile,
    kExecute,
    kInterrupt
  };

  TGPopupMenu* fFileMenu;
  TGPopupMenu* fEditMenu;
  TGPopupMenu* fSearchMenu;
  TGPopupMenu* fToolMenu;
  TGToolBar* fToolBar;
  TGTextEdit* fEdit;
  TGStatusBar* fStatus;
  TString fFileName;
  const TGUIEnglish& fTranslation;

  Editor(const char* filename, TGStatusBar* status);

  /**
   * Load the file
   */
  void Load(const char* file = nullptr, Bool_t cheat = false);

  /**
   * Remove lines from the file.  These lines the student should put
   * back in.
   */
  void EditForStudent();

  /**
   * Handle menu item stuff
   *
   * @param id Menu item
   */
  void Menu(Int_t id);

  /**
   * Compile the script
   */
  void Compile();

  /**
   * Run the script
   */
  void Run();

  /**
   * Interrupt execution
   */
  void Interrupt();

  /**
   * Search the text
   *
   * @param again If true, repeate last search
   */
  void Search(Bool_t again);

  /**
   * Called when text is changed
   */
  void Changed();

  /**
   * Check if the file is saved
   * @return true if it is saved
   */
  Bool_t IsSaved();

  /**
   * Save the file.  Note, we always save to the working directory.
   */
  void Save();

  /**
   * Go to a specific line
   */
  void Goto();

  /**
   * Calld when text is marked
   *
   * @param on True if region is on
   */
  void Marked(Bool_t on);

  /**
   * Show current position
   */
  void ShowPos();

  ClassDef(Editor, 0);
};
} // namespace Raa
#endif
