#ifndef READ_H_5JZGMYTL
#define READ_H_5JZGMYTL

/**
 * @file   Read.C
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Mar  9 20:29:19 2017
 *
 * @brief This code defines a GUI to read the values of RAA in
 * specific pT and centrality bins.
 *
 * This is to be run by the exercise instructor before the remove
 * Vidyo conference.
 *
 * @ingroup alice_masterclass_raa_part2
 */
#include <Rtypes.h>
#include <RtypesCore.h>
#include <TCanvas.h>
#include <TGColorSelect.h>
#include <TGComboBox.h>
#include <TGFrame.h>
#include <TGHtml.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TGNumberEntry.h>
#include <TList.h>

#include "Raa/ContentGUITranslation.h"
#include "Raa/Utilities.h"

class TBuffer;
class TClass;
class TGColorSelect;
class TGComboBox;
class TGNumberEntryField;
class TGTextEntry;
class THStack;
class TMemberInspector;

namespace Raa
{
struct TGUIEnglish;
/**
 * A single row.
 *
 * @ingroup alice_masterclass_raa_part2
 */
struct Row : public TGGroupFrame {
  THStack* fStack;
  TGComboBox* fCBin;
  TGComboBox* fPtBin;
  TGNumberEntryField* fValue;
  TGNumberEntryField* fError;
  Bool_t fOK;
  Double_t fPt;
  const TGUIEnglish& fTranslation;

  Row(TGCompositeFrame* p, THStack* stack);

  void Fetch();
  ClassDef(Row, 1);
};

/**
 * Main GUI.
 *
 * @ingroup alice_masterclass_raa_part2
 */
class Values : public TGMainFrame
{
 public:
  TGTextEntry* fWho;
  TGColorSelect* fColor;
  TList fRows;
  const TGUIEnglish& fTranslation;

  Values(THStack* stack);

  /**
   * Saves read values to a file
   */
  void Save();

  ClassDef(Values, 0);
};

/**
 * Instructions canvas.
 *
 * @ingroup alice_masterclass_raa_part2
 */
class InstructionsGUI : public TGMainFrame
{
 private:
  const TGUIEnglish& fTranslation;

 public:
  InstructionsGUI();
  ClassDef(InstructionsGUI, 0)
};

/**
 * Entry point.
 *
 * @ingroup alice_masterclass_raa_part2
 */
void Read();
} // namespace Raa

#endif /* end of include guard: READ_H_5JZGMYTL */
