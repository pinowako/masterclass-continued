How to add another Language
===========================

The mechanism to provide translations has been reworked since the version of
Christian.
The new version relies on multiple compile time constructs to provide better
diagnostics on missing parts.
You should create a new branch for the translation first before you start to
work. This ensures that you do not pollute another working state. At the end,
the changes can be incorporated with a merge request!

Register the language
---------------------

Registration for a new language must be done in
`src/Utility/LanguageProvider.{h,cxx}`.

1. Add the english name for the new language to the
  `enum ESupportedLanguages` __BEFORE__ `NLanguages`.
1. Extend the functions `GetLanguageName`,
  `GetLanguageNameShort`, `GetLanguageFromShort` accordingly.
  Please note, that the `case NLanguages` branch exists to utilize the
  compile time warnings for missing code paths. Do not remove that branch!
1. Please extend the unit-tests in `test/test_translation.cpp`
  - add the language in `TestAllGUILanguages`
  - add the language in `TestAllContentLanguages`

If you recompile the program you should receive new warnings that a value of
an `enum` is not covered in some `switch` statements.
Theses places actually create the correct translations for each language
which must be provided next. The unittests will give you hints what to do
as well!

Provide translation classes for all parts
-----------------------------------------

The code utilizes inheritance to provide the text snippets for every language.
English is the base class in every case to have it as an fallback
language.

The code provides macros to reduce the amount of boilerplate code. These are
explained briefly. See `src/Raa/ContentGUITranslation.h` as an example.

- `LANG_KEY(<KEY>)` creates a new method with name `<KEY>` and query the
  snippet storage for `<KEY>`.
- `LANG_TRANSLATION(<CLASS_PREFIX>, <LANG>)` creates a new class with the
  name `<CLASS_PREFIX><LANG>` that derives from `<CLASS_PREFIX>English`. It
  automatically contains all the method created with `LANG_KEY`.
  All you have to provide for `LANG_TRANSLATION` is an include of all
  registered keys and the corresponding translated words. These files are
  discussed afterwards.

Provide the `LANG_TRANSLATION` with the same pattern as the existing
translations (e.g. in `src/Raa/ContentGUITranslation.h`) for:

- `src/Raa/ClassContent.h`
- `src/Raa/ContentGUITranslation.h`
- `src/Strangeness/ClassContent.h`
- `src/Strangeness/GUITranslation.h`
- `src/Jpsi/ClassContent.h`
- `src/Jpsi/GUITranslation.h`

Please add the missing codepath for the `switch` in the `create`-method at
the end of these files. The compiler will warn if you don't.

For bigger documents use the `#include` mechanism as all other translations do.
This ensures, that the translation is shipped **WITH THE EXECUTABLE** and does
not require a runtime loading which is error prone. This allows some static
analysis as well.

Do the actual translation
-------------------------

1. Copy all html-based documentation files that shall be translated with
   `./utility/copy_html_docs.bash <LANG_SHORTCUT>` (e.g. `LANG_SHORTCUT = de`
   for german)
1. (Optional) If you use the automated translation script please add
   replacements for special character of your language in
   `utility/translate.py` in the function `ascii_transformer()`.
   ROOT-GUI does only support ASCII letters.
1. Create the Google Translate version for the translation snippets with
   `./utility/translate_snippets.bash <LANG_SHORTCUT>`. Please note that this
   script use [PhantomJs](http://phantomjs.org/) to query Google Translate.
   This process will take a little while. It will use `parallel` if the
   program is detected on you machine. Otherwise `xargs`.
1. Provide the actual translations for all files. Do not forget to outcomment
   the snippets, they are included directly into C++ code.

It is of course OK to use Google Translate (or another translation service)
as a starting point, but please check the translation and correct weird
translations. It does not hurt to run the MasterClasses and check if the
translations make sense in context :)

Integrating into upstream
-------------------------

Because you made a new branch, this is super easy. Push the branch and make an
merge request. Other forms are not accepted ;)
Instructions on that can be found in the
[GitLab Documentation](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
