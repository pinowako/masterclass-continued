#!/bin/bash

# This script run CopyPasteDetector (cpd) on the codebase.
# The locations depend on my personal install of it!
~/opt/cpd/pmd-bin-6.5.0/bin/run.sh cpd --files src/EntryPoint/ src/Raa/ src/Utility/ src/Jpsi/ src/Strangeness/ --language cxx --minimum-tokens 60

exit 0
