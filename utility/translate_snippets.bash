#!/bin/bash

trap_ctrlC() {
  echo "Press CTRL-C again to kill. Restarting in 2 second"
  sleep 2 || exit 1
}

trap trap_ctrlC SIGINT SIGTERM

if [ $# -ne 1 ]; then
  echo "Provide the language shortcut for the target language."
  echo "E.g. 'de' for german"
  exit 1
fi

LANGUAGE_KEY="$1"

# Check if the script is executed from the correct directory. Existence of
# `.git` directory is used as heuristic.
if [ ! -d .git ]; then
  echo "You did not executed the script from the repository base-directory!"
  exit 1
fi

if [ $(command -v parallel) ]; then
  EXECUTOR=parallel
else
  EXECUTOR="xargs -I {}"
fi

echo "Information:"
echo "============"
echo "Using ${EXECUTOR} to start the translation scripts."
echo "Using the language \'${LANGUAGE_KEY}\' to translate to."
echo
echo

(echo "translation/EntryPoint/keys_gui_trans.txt.en";
echo "translation/Utility/keys_gui_trans.txt.en";
echo "translation/Jpsi/keys_class_trans.txt.en";
echo "translation/Jpsi/keys_gui_trans.txt.en";
echo "translation/Raa/keys_class_trans.txt.en";
echo "translation/Raa/keys_gui_trans.txt.en";
echo "translation/Strangeness/keys_class_trans.txt.en";
echo "translation/Strangeness/keys_gui_trans.txt.en") | $EXECUTOR python3 utility/translate.py -i {} -s en -t "$LANGUAGE_KEY"
