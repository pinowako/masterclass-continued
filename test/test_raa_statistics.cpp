#include "catch2/catch.hpp"

#include <TFile.h>
#include <TSystem.h>
#include <iomanip>
#include <iostream>
#include <random>

#include "Raa/part1/TRaaStatistics.h"
#include "Raa/part1/TTrackCounter.h"
#include "util_track_creation.h"

gsl::span<const Double_t> GetHistSpan(const TH1D& H)
{
  const Double_t* UnderlyingArray = H.GetArray();
  const Int_t NBins = H.GetSize();

  return gsl::span<const Double_t>(UnderlyingArray, NBins);
}

Bool_t HistAllZero(const TH1D& H)
{
  const auto S = GetHistSpan(H);
  return std::all_of(std::begin(S), std::end(S), [](const Double_t V) { return V == 0.; });
}

void BinPrint(const TH1D& Hist)
{
  std::cout << "Bins for '" << Hist.GetName() << "'\n";
  const auto Range = GetHistSpan(Hist);
  for (std::size_t i = 0; i < Range.size(); ++i) {
    const Double_t low = Hist.GetBinLowEdge(i);
    const Double_t high = low + Hist.GetBinWidth(i);

    std::cout << std::fixed << std::setprecision(4) << std::setw(7) << low << " - " << std::setw(7)
              << high << ":= " << Range[i] << "\n";
  }
}

TEST_CASE("Tracks are correctly registered", "")
{
  constexpr int NumPrimaries = 5;
  constexpr int NumPrimariesPositive = 3;
  constexpr int NumPrimariesHighPt = 7;
  constexpr int NumSecondaries = 8;

  SECTION("Do some counting of the tracks, simulate selections")
  {
    Raa::TTrackCounter C;
    TEveTrack t_primary = internal::CreatePrimary();
    TEveTrack t_primary_pos = internal::CreatePrimaryCharged();
    TEveTrack t_primary_high_pt = internal::CreatePrimaryHighPt();
    TEveTrack t_sec = internal::CreateSecondary();

    for (int i = 0; i < NumPrimaries; ++i)
      C.CountPrimary(&t_primary);

    for (int i = 0; i < NumPrimariesHighPt; ++i)
      C.CountPrimary(&t_primary_high_pt);

    for (int i = 0; i < NumPrimariesPositive; ++i) {
      C.CountPrimary(&t_primary_pos);
      C.CountPrimaryPositive(&t_primary_pos);
    }

    for (int i = 0; i < NumSecondaries; ++i)
      C.CountSecondary(&t_sec);

    REQUIRE(C.NPrimaries() > 0);
    REQUIRE(C.NSecondaries() > 0);

    AND_THEN("Check that the Statistics did count somewhat correctly")
    {
      Raa::TRaaStatistics A("test_raa_stats");

      A.AddEventStatistics(C.Primaries());
      A.AddSecondaryMultiplicity(C.NSecondaries());

      REQUIRE(!HistAllZero(A.HistMultiplicity()));
      REQUIRE(!HistAllZero(A.HistMultiplicityMinPt()));
      REQUIRE(!HistAllZero(A.HistMultiplicitySecondaries()));

      REQUIRE(A.HistMultiplicity().GetBinContent((A.HistMultiplicity().FindFixBin(
                NumPrimaries + NumPrimariesPositive + NumPrimariesHighPt))) == 1);
      REQUIRE(A.HistMultiplicityMinPt().GetBinContent(
                (A.HistMultiplicityMinPt().FindFixBin(NumPrimariesHighPt))) == 1);
      REQUIRE(A.HistMultiplicitySecondaries().GetBinContent(
                (A.HistMultiplicitySecondaries().FindFixBin(NumSecondaries))) == 1);

      BinPrint(A.HistMultiplicity());
      BinPrint(A.HistMultiplicityMinPt());
      BinPrint(A.HistMultiplicitySecondaries());

      REQUIRE(!HistAllZero(A.HistCharge()));
      REQUIRE(!HistAllZero(A.HistPhi()));

      REQUIRE(A.HistCharge().GetBinContent(A.HistCharge().FindFixBin(0.)) ==
              NumPrimaries + NumPrimariesHighPt);
      REQUIRE(A.HistCharge().GetBinContent(A.HistCharge().FindFixBin(1.)) == NumPrimariesPositive);
      REQUIRE(A.HistPhi().GetBinContent(A.HistPhi().FindFixBin(0.)) ==
              NumPrimaries + NumPrimariesPositive + NumPrimariesHighPt);

      BinPrint(A.HistCharge());
      BinPrint(A.HistPhi());

      AND_THEN("Clear and ensure everything is empty again")
      {
        A.ClearStatistics();
        REQUIRE(HistAllZero(A.HistMultiplicity()));
        REQUIRE(HistAllZero(A.HistMultiplicityMinPt()));
        REQUIRE(HistAllZero(A.HistMultiplicitySecondaries()));

        REQUIRE(HistAllZero(A.HistCharge()));
        REQUIRE(HistAllZero(A.HistPhi()));
      }
    }

    AND_THEN("Check that the distributions are counted properly")
    {
      Raa::TPtDistribution PPDist("ProtonProton");

      PPDist.AddEventStatistics(C.Primaries());

      BinPrint(PPDist.GetHistogram());

      REQUIRE(!HistAllZero(PPDist.GetHistogram()));
      REQUIRE(PPDist.GetHistogram().GetBinContent(1) == NumPrimaries + NumPrimariesPositive);
      REQUIRE(PPDist.GetHistogram().GetBinContent(17) == NumPrimariesHighPt);

      AND_THEN("Clear the data")
      {
        PPDist.ClearStatistics();
        REQUIRE(HistAllZero(PPDist.GetHistogram()));
      }
    }
  }
}

TEST_CASE("Calculate Raa Function", "")
{
  using Raa::CalcRaa;
  using Raa::RaaFor;

  const Double_t MultiplicityPbPb = 500.;
  const Double_t MultiplicityPP = 10.;
  const Double_t Correction = 50.;
  const Double_t ExpectedUncertainty = std::sqrt(1. / 250000. + 1. / 100.);

  WHEN("CalcRaa is called with all expected parameters")
  {
    const auto Result = CalcRaa(MultiplicityPbPb, MultiplicityPP, Correction, 1., 1.);

    REQUIRE(Result.first == Approx(1.));
    REQUIRE(Result.second == Approx(Result.first * ExpectedUncertainty));
  }

  WHEN("RaaFor some PP statistics is called")
  {
    TH1D PPStats("Raa for PP Test", "Title for the test histogram", 10, 0., 50.);

    // 100 Events where multiplicity of primaries is 'MultiplicityPP'. Fakes
    // a dataset with the relevant data.
    for (int i = 0; i < 100; ++i)
      PPStats.Fill(MultiplicityPP);

    const auto Result = RaaFor(MultiplicityPbPb, PPStats, Correction);

    REQUIRE(Result.first == Approx(1.));
    const Double_t ExpectedUncertainty = 1.0010;
    REQUIRE(Result.second == Approx(Result.first * ExpectedUncertainty));
  }
}

TEST_CASE("Export Raa Statistics", "")
{
  TFile tmp_file("tmp_statistics.root", "RECREATE");
  Raa::TRaaStatistics s("test_export");
  s.Export(&tmp_file);

  REQUIRE(true);

  tmp_file.Write();
  tmp_file.Close();
}
