#include "catch2/catch.hpp"

#include "Raa/part1/EventDisplay.h"
#include "util_track_creation.h"

TEST_CASE("EventDisplay count while loading", "")
{
  Raa::EventDisplay E;

  TEveTrack t_primary = internal::CreatePrimary();
  TEveTrack t_primary_pos = internal::CreatePrimaryCharged();
  TEveTrack t_sec = internal::CreateSecondary();

  E.TrackLoaded(&t_primary);
  E.TrackLoaded(&t_primary_pos);
  E.TrackLoaded(&t_sec);
}
