#include "catch2/catch.hpp"

#include <TEveTrack.h>

#include "Raa/part1/TrackClassification.h"

TEST_CASE("Primary Classifier", "[classify_tracks]")
{
  WHEN("Track is marked as a primary track from VSDReader")
  {
    TEveTrack t_primary;
    Utility::MarkPrimary(&t_primary);
    REQUIRE(Raa::IsPrimary(&t_primary));

    AND_THEN("The track is marked as not primary")
    {
      Utility::ClearPrimary(&t_primary);
      REQUIRE(!Raa::IsPrimary(&t_primary));
    }
  }

  WHEN("Track has not been marked as primary track")
  {
    TEveTrack t_non_primary;
    REQUIRE(!Raa::IsPrimary(&t_non_primary));
  }
}

TEST_CASE("Primary Positive Classifier", "[classify_tracks]")
{
  TEveTrack t_primary;
  Utility::MarkPrimary(&t_primary);
  WHEN("Charge is positive")
  {
    t_primary.SetCharge(1);
    REQUIRE(Raa::IsPrimaryPositive(&t_primary));
  }

  WHEN("Charge is negative")
  {
    t_primary.SetCharge(-1);
    REQUIRE(!Raa::IsPrimaryPositive(&t_primary));
  }
}

TEST_CASE("Helper function to calculate Pt", "")
{
  TEveRecTrackD underlying_track;
  const TEveVectorD Exactly1GeV(1., 0., 0.);

  WHEN("Pt == 0.5")
  {
    underlying_track.fP = 0.5 * Exactly1GeV;
    TEveTrack t_primary(&underlying_track);

    REQUIRE(Raa::GetPt(&t_primary) == Approx(0.5));
  }

  WHEN("Track has different direction")
  {
    underlying_track.fP = -0.5 * Exactly1GeV;
    TEveTrack t_primary(&underlying_track);

    REQUIRE(Raa::GetPt(&t_primary) == Approx(0.5));
  }

  WHEN("Track has no momentum")
  {
    underlying_track.fP = 0.0 * Exactly1GeV;
    TEveTrack t_primary(&underlying_track);

    REQUIRE(Raa::GetPt(&t_primary) == Approx(0.0));
  }

  WHEN("fX == fY == 0, but fZ > 1.") {
    TEveRecTrackD underlying_bad_track;
    underlying_bad_track.fP = {0., 0., 1.5};
    TEveTrack t_non_primary(&underlying_bad_track);

    REQUIRE(Raa::GetPt(&t_non_primary) == 0.);
  }
}

TEST_CASE("Track is a secondary", "[classify_tracks]")
{
  TEveTrack t;

  WHEN("Track is marked as primary")
  {
    Utility::MarkPrimary(&t);
    REQUIRE(!Raa::IsSecondary(&t));
  }

  AND_THEN("The tracks get primary revoked")
  {
    Utility::ClearPrimary(&t);
    REQUIRE(Raa::IsSecondary(&t));
  }
}
