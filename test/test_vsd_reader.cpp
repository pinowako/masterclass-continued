#include "catch2/catch.hpp"

#include <TApplication.h>
#include <TEveManager.h>
#include <Utility/VSDReader.h>

#include "base_path.h"

namespace
{
TFile* OpenRaaVSD()
{
  return TFile::Open(TString(ProjectSource) + "/vsdData/Raa/AliVSD_MasterClass_test.root", "READ");
}
#if 0
TFile* OpenStrangeVSD()
{
  return TFile::Open(TString(ProjectSource) + "/vsdData/Strangeness/AliVSD_MasterClass_1.root",
                     "READ");
}
TFile* OpenJpsiVSD()
{
  return TFile::Open(TString(ProjectSource) + "/vsdData/Raa/AliVSD_Masterclass_1.root", "READ");
}
#endif
} // namespace

TEST_CASE("VSDReader Construction", "[vsd_reader]") { Utility::VSDReader R; }

TEST_CASE("VSDReader Setup", "[vsd_reader]")
{
  REQUIRE(ProjectSource != nullptr);
  std::cout << ProjectSource << std::endl;

  Utility::VSDReader R;
  R.Setup(OpenRaaVSD(), false);
}

#if 0
// FIXME Segfaults when setting attributes for the EveElements
TEST CASE("VSDReader Load Tracks", "[vsd_reader][load_tracks]")
{
  int i = 0;
  // const char* exec_name = "test_driver.x";
  // char** argv = { const_cast<char**>(const_cast<char*>(&exec_name)) };

  TApplication app("TestDriver", &i, nullptr);
  TEveManager::Create(kTRUE, "FV");
  new TEveEventManager;

  WHEN("Only primary tracks are loaded")
  {
    Utility::VSDReader R;
    R.Setup(OpenRaaVSD());
    R.LoadEvent(1);
    R.LoadTracks(0.0, true);
  }
  WHEN("Only all tracks are loaded")
  {
    Utility::VSDReader R;
    R.Setup(OpenRaaVSD());
    R.LoadEvent(1);
    R.LoadTracks(0.0, false);
  }
}

TEST CASE("VSDReader Load V0s", "[vsd_reader][load_v0s]")
{
  // TEveManager::Create(kTRUE, "FV");
  WHEN("Load all V0s")
  {
    Utility::VSDReader R;
    R.Setup(OpenStrangeVSD());
    R.LoadEvent(1);
    R.LoadV0s(1.0);
  }
}

TEST CASE("VSDReader Load Cascades", "[vsd_reader][load_cascades]")
{
  // TEveManager::Create(kTRUE, "FV");
  WHEN("Load all V0s")
  {
    Utility::VSDReader R;
    R.Setup(OpenStrangeVSD());
    R.LoadEvent(1);
    R.LoadCascades(1.0);
  }
}

#endif
