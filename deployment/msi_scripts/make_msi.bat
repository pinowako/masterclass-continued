SET "PATH=%PATH%;C:\Program Files\WiX Toolset v3.11\bin"

heat dir ".\bin" -gg -sfrag -srd -cg BinariesGroup -dr BINARYDIR -var var.projectdir -template fragment -out bin.wxs
heat dir ".\headers" -gg -sfrag -srd -cg HeadersGroup -dr HEADERDIR -var var.projectdir -template fragment -out headers.wxs
heat dir ".\share" -gg -sfrag -srd -cg ShareGroup -dr SHAREDIR -var var.projectdir -template fragment -out share.wxs
heat dir ".\root" -gg -sfrag -srd -cg RootGroup -dr ROOTDIR -var var.projectdir -template fragment -out root.wxs

candle bin.wxs -dprojectdir=bin
candle headers.wxs -dprojectdir=headers
candle share.wxs -dprojectdir=share
candle root.wxs -dprojectdir=root
candle MasterClassSetup.wxs
light -ext WixUIExtension bin.wixobj headers.wixobj share.wixobj root.wixobj MasterClassSetup.wixobj -o ..\MasterClassSetup.msi