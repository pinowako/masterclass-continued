#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This file automates the deployment of MasterClasses.
Supported modes: MSI,AppImage,VMInstall
"""

import argparse
import multiprocessing
import os
import stat
import subprocess
import tarfile
import zipfile
import shutil
import sys
import urllib.request


def create_parser():
    parser = argparse.ArgumentParser()
    # parser.add_argument(
    #     "-k",
    #     "--keep-files",
    #     help="Keep generated temporary files (e.g. build-dir)",
    #     action='store_true')
    parser.add_argument(
        "-o",
        "--out-directory",
        help="Name of the directory to create (e.g. for versioning)",
        default="deploy",
        type=str)
    parser.add_argument(
        "-sh",
        "--hash",
        help="Eight-character hash (typically from SHA of the commit) used as build number",
        default="00000000"
    )
    parser.add_argument(
        "-c",
        "--clear",
        help="Clear cache folder before building",
        action="store_true"
    )
    parser.add_argument(
        "-r",
        "--root-version",
        help="Version of ROOT",
        default="6.22.02"
    )
    parser.add_argument(
        "-p",
        "--platform",
        help="Target platform (if UNIX)",
        default="Linux-ubuntu18-x86_64-gcc7.5"
    )
    parser.add_argument(
        "-s",
        "--skip-packing",
        help="Don't package but only build.",
        action="store_true")
    parser.add_argument(
        "-t",
        "--type",
        help="Type of the package to build",
        required=True,
        choices=("appimage", "vminstall", "msi"))
    return parser


class MasterClassBuilder:
    UNIX, WINDOWS = range(2)

    def __init__(self, args, current_system):
        self.args = args

        if current_system == "unix":
            self.system = MasterClassBuilder.UNIX
        else:
            self.system = MasterClassBuilder.WINDOWS

        if (self.system == MasterClassBuilder.UNIX and self.args.type == "msi") or \
            (self.system == MasterClassBuilder.WINDOWS and (self.args.type == "appimage" or self.args.type == "vminstall")):
            print("Incompatibile operating system and target combination!")
            sys.exit(1)

        self.repo_path = self.find_repo() + os.path.sep

        if self.args.type == "appimage":
            self.target_name = "{}.AppDir".format(args.out_directory)
        elif self.args.type == "vminstall":
            self.target_name = "{}".format(args.out_directory)
        elif self.args.type == "msi":
            self.target_name = "{}".format(args.out_directory)

        self.cache_path = os.path.join(self.repo_path, "deployment" + os.path.sep + "cache" + os.path.sep)
        self.install_path = os.path.join(self.repo_path, self.cache_path + self.target_name)
        self.build_path = os.path.join(self.cache_path, "build" + os.path.sep)

        #if self.args.clear:
        #    print("Clearing cache folder...")
        #    shutil.rmtree(self.cache_path, ignore_errors=True)

        os.makedirs(self.cache_path, exist_ok=True)
        os.makedirs(self.install_path, exist_ok=True)
        os.makedirs(self.build_path, exist_ok=True)

        self.root_version = self.args.root_version

        if self.system == MasterClassBuilder.UNIX:
            self.root_archive_format = ".tar.gz"
            self.root_platform = self.args.platform
        else:
            self.root_archive_format = ".zip"
            self.root_platform = "win32.vc16"

        self.root_archive_filename = os.path.join(self.cache_path, "ROOT" + self.root_archive_format)
        self.root_directory = None

    def find_repo(self):
        """
        Find the absolute path to the repository of the MasterClasses and return it.
        """
        old_path = os.getcwd()
        repo_path = ""
        while True:
            current_path = os.getcwd()
            if not os.path.exists(os.path.join(current_path, ".git")):
                os.chdir(os.path.abspath(".."))
            else:
                # found a git repository, is it the MasterClass repo?
                if os.path.exists(
                        os.path.join(current_path, "AliceMasterClass.desktop")):
                    repo_path = current_path
                    break
                else:
                    print(
                        "Found git-repository in '{}', but does not seem to be MasterClass repo. Bailing out!".
                        format(current_path),
                        file=sys.stderr)
                    sys.exit(1)

            # we are in the root directory, stop iterating
            if current_path == os.path.abspath(os.sep):
                print(
                    "Could not find git-repository, stop at root directory",
                    file=sys.stderr)
                sys.exit(1)

        os.chdir(old_path)
        return repo_path

    def download_root(self):
        """
        Download the ROOT binaries from the official site.

        :returns: directory name of the the extract ROOT archive
        """
        url = "https://root.cern.ch/download/root_v{version}.{platform}{archive_type}".format(version=self.root_version, platform=self.root_platform, archive_type=self.root_archive_format)
        out_dir = self.root_archive_filename.rstrip(self.root_archive_format)

        if not os.path.exists(out_dir):
            with urllib.request.urlopen(url) as response, open(self.root_archive_filename,'wb') as out_f:
                shutil.copyfileobj(response, out_f)

            if self.root_archive_format == ".tar.gz":
                root_archive = tarfile.open(self.root_archive_filename)
            else:
                root_archive = zipfile.ZipFile(self.root_archive_filename)

            root_archive.extractall(out_dir)
            root_archive.close()

        self.root_directory = os.path.abspath(os.path.join(out_dir, "root/"))

    def write_build_header(self):
        file_content = f'''#ifndef CMAKE_BUILD_NUMBER_HEADER
#define CMAKE_BUILD_NUMBER_HEADER

#define BUILD_NUMBER "{self.args.hash}"

#endif
'''
        build_header_path = os.path.join(self.repo_path, "src" + os.path.sep + "compilation_number.h")
        with open(build_header_path, "w") as out_f:
            out_f.write(file_content)

    def configure_cmake(self):
        """
        Call CMake in :directory: with the necessary flags to build the
        project
        """

        if self.args.type == "vminstall":
            data_prefix = "/home/masterclass/masterclass/share"
        else:
            data_prefix = "share"

        cmd = f"cmake -S {self.repo_path} -B {self.build_path} -DROOTSYS={self.root_directory} " \
            f"-DCMAKE_INSTALL_PREFIX={self.install_path} -DMASTERCLASS_DIR={data_prefix} -DTESTING=OFF -DCMAKE_BUILD_TYPE=Release"

        try:
            subprocess.run(cmd, shell=True)
        except Exception as e:
            raise e

    def compile_code(self, ncores = -1):
        """
        Call make in the specified directory
        :ncores: number of cores to build, -1 means all available cores.
        """

        if ncores == -1:
            ncores = multiprocessing.cpu_count()

        assert ncores > 0

        cmd = f"cmake --build {self.build_path} --parallel {ncores} --config Release"

        try:
            subprocess.run(cmd, shell=True)
        except Exception as e:
            raise e

    def install_code(self):
        """
        Call 'make install' with the appropriate destination in build_dir
        """

        cmd = f"cmake --build {self.build_path} --target install --config Release"

        try:
            subprocess.run(cmd, shell=True)
        except Exception as e:
            raise e

    def copy_root(self):
        if not os.path.exists(os.path.join(self.install_path, "root")):
            shutil.copytree(self.root_directory, os.path.join(self.install_path, "root"))

    def copy_file(self, src_path, dst_path, filename):
        FileSrc = os.path.join(src_path, filename)
        FileDst = os.path.join(dst_path, filename)
        shutil.copyfile(FileSrc, FileDst)

    def create_appimage(self):
        # Make AppRun executable
        AppRunPath = os.path.join(self.install_path, "AppRun")
        os.chmod(AppRunPath, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)

        # download the AppImage tool
        AppImageURL = "https://github.com/probonopd/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
        AppImageToolName = "appimagetool-x86_64.AppImage"
        AppImageToolPath = os.path.join(self.cache_path, AppImageToolName)
        AppImageExtractedToolPath = os.path.join(self.cache_path, "squashfs-root" + os.path.sep + "AppRun")
        PackagePath = os.path.join(self.cache_path, "ALICE_MasterClasses-x86_64.AppImage")

        if not os.path.exists(AppImageToolName):
            with urllib.request.urlopen(AppImageURL) as response, open(AppImageToolPath, 'wb') as out_f:
                shutil.copyfileobj(response, out_f)
            os.chmod(AppImageToolPath, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
            cmd = f"{AppImageToolPath} --appimage-extract"

            old_dir = os.getcwd()
            os.chdir(self.cache_path)
            subprocess.run(cmd, shell=True)
            os.chdir(old_dir)

            os.chmod(AppImageExtractedToolPath, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)

            cmd = f"{AppImageExtractedToolPath} {self.install_path} {PackagePath}"
            subprocess.run(cmd, shell=True)

    def create_msi(self):
        scripts_path = os.path.join(self.repo_path, "deployment" + os.path.sep + "msi_scripts" + os.path.sep)
        
        self.copy_file(scripts_path, self.install_path, "MasterClass.exe")
        self.copy_file(scripts_path, self.install_path, "make_msi.bat")
        self.copy_file(scripts_path, self.install_path, "License.rtf")
        self.copy_file(scripts_path, self.install_path, "MasterClassSetup.wxs")

        # TEMP FIX FOR ROOT 6.18.04
        # temp_fix_path = os.path.join(scripts_path, "temp_fix" + os.path.sep)
        # bin_path = os.path.join(self.install_path, "bin" + os.path.sep)
        # self.copy_file(temp_fix_path, bin_path, "libEve.dll")
        # self.copy_file(temp_fix_path, bin_path, "libEve1.rootmap")
        # self.copy_file(temp_fix_path, bin_path, "libEve1_rdict.pcm")
        # self.copy_file(temp_fix_path, bin_path, "libEve2.rootmap")
        # self.copy_file(temp_fix_path, bin_path, "libEve2_rdict.pcm")
        
        old_dir = os.getcwd()
        os.chdir(self.install_path)
        subprocess.run("call make_msi.bat", shell=True)
        os.chdir(old_dir)

    def start(self):
        print("Setting build number...")
        self.write_build_header()
        print(f"Downloading ROOT {self.root_version} for {self.root_platform}...")
        self.download_root()
        print("Configuring CMake...")
        try:
            self.configure_cmake()
        except subprocess.CalledProcessError:
                print(
                    "Could not configure directory '{}' with cmake!".format(self.build_path),
                    file=sys.stderr)
                sys.exit(1)
        print("Compilling...")
        try:
            self.compile_code()
        except subprocess.CalledProcessError:
            print(
                "Could not compile code in directory '{}'!".format(self.build_path),
                file=sys.stderr)
            sys.exit(1)

        print("Installing...")
        try:
            self.install_code()
        except subprocess.CalledProcessError:
            print(
                "Could not install code in directory '{}' to destination '{}'!".
                format(self.build_path, self.install_path),
                file=sys.stderr)
            sys.exit(1)

        if self.args.skip_packing:
            return

        print("Copying ROOT...")
        self.copy_root()

        if self.args.type == "appimage":
            print("Creating AppImage...")
            self.create_appimage()
        elif self.args.type == "msi":
            print("Creating MSI Installer...")
            self.create_msi()
        elif self.args.type == "vminstall":
            pass

# def main():
#     """
#     Implements the program logic of compiling, installing and archiving
#     the masterclass to the requested format.
#     """
#     args = create_parser().parse_args()
#
#     current_system = "windows" if os.name == "nt" else "linux"
#
#     if args.type == "appimage":
#         target_name = "{}.AppDir".format(args.out_directory)
#     elif args.type == "vminstall":
#         target_name = "{}".format(args.out_directory)
#     elif args.type == "msi":
#         target_name = "{}.msi".format(args.out_directory)
#     else:
#         print("Wrong type supplied!", file=sys.stderr)
#         sys.exit(1)
#
#     root_ver = "6.18.04"
#     archive_format = ".zip" if current_system == "windows" else ".tar.gz"
#
#     cache_path = os.path.join(find_repo(), "deployment/download_cache/")
#     root_ver_f = os.path.join(cache_path, "ROOT6" + archive_format)
#     install_dir = os.path.abspath(os.path.join(os.getcwd(), target_name))
#
#     if current_system == "windows":
#         target = "win32.vc16"
#     else:
#         target = "Linux-ubuntu18-x86_64-gcc7.4"
#
#     # Download root, will use the cached version if existing
#     ROOT_dir = download_root(root_ver, target, archive_format, root_ver_f)
#
#     # Compile the code in release mode and install into the install_dir
#     build_dir = build_code("release", ROOT_dir, install_dir,
#                            args.force, args.type)
#     if not args.keep_files:
#         shutil.rmtree(build_dir)
#
#     # TODO: fix the paths!!!!
#
#     # subprocess.run(['rm', '-rf', os.path.join(install_dir, 'headers')])
#     # shutil.copytree(find_repo() + '/src', os.path.join(install_dir, 'headers'))
#     # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.cxx', '-type', 'f', '-delete'])
#     # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.txt', '-type', 'f', '-delete'])
#     # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.cmake', '-type', 'f', '-delete'])
#     # subprocess.run(["find", os.path.join(install_dir, 'headers'), '-name', '*.md', '-type', 'f', '-delete'])
#     # subprocess.run(['cp', '-r', find_repo() + '/translation/.', os.path.join(install_dir, 'headers/')])
#     # subprocess.run(['cp', '-r', find_repo() + '/lib/gsl-lite/include/.', os.path.join(install_dir, 'headers/')])
#
#     if args.type == "msi":
#         pass
#     elif args.type == "vminstall":
#         pass
#
#         #subprocess.run(['rm', os.path.join(install_dir, 'AppRun')])
#
#         #subprocess.run(['sudo', 'rsync', '-av', install_dir + '/', '/mnt/vdi/home/masterclass/masterclass'])
#         #subprocess.run(['sudo', 'cp', '-r', os.path.join(install_dir, '.'), '/mnt/vdi/home/masterclass/masterclass'])
#         #subprocess.run(['sudo', 'cp', '-r', '/home/user/masterclass/deployment/startMasterClass.sh', '/mnt/vdi/home/masterclass/masterclass/'])
#         #subprocess.run(['sudo', 'mv', '-n', '/mnt/vdi/home/masterclass/masterclass/AliceMasterClass.desktop', '/mnt/vdi/home/masterclass/Desktop/'])
#         #subprocess.run(['sudo', 'chmod', 'o+x', '/mnt/vdi/home/masterclass/Desktop/AliceMasterClass.desktop'])
#     else:
#         # make AppRun executable
#         apprun_path = os.path.join(install_dir, "AppRun")
#         subprocess.run(["chmod", "+x", "{}".format(apprun_path)])
#
#         # copy in ROOT for convenience
#         if not os.path.exists(os.path.join(install_dir, "root")):
#             shutil.copytree(ROOT_dir, os.path.join(install_dir, "root"))
#
#     if args.skip_packing:
#         return
#
#     # create tar.gz if making an normal archive
#     if args.type == "archive":
#         archive_name = os.path.abspath(install_dir + ".tar.gz")
#         if os.path.exists(archive_name) and args.force:
#             os.remove(archive_name)
#         elif os.path.exists(archive_name):
#             print(
#                 "Target archive '{}' already exists!".format(archive_name),
#                 file=sys.stderr)
#             sys.exit(1)
#
#         print("Packing archive {}".format(archive_name))
#         with tarfile.open(archive_name, "w:gz") as archive:
#             print(install_dir)
#             archive.add(install_dir, arcname=os.path.basename(install_dir))
#
#     elif args.type == "appimage":
#         # download the AppImage tool
#         AppImageURL = "https://github.com/probonopd/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
#         AppImageToolName = "appimagetool-x86_64.AppImage"
#         AppImageToolPath = os.path.join(
#             find_repo(),
#             "deployment/download_cache/{}".format(AppImageToolName))
#
#         if not os.path.exists(AppImageToolName):
#             with urllib.request.urlopen(AppImageURL) as response, open(
#                     AppImageToolPath, 'wb') as out_f:
#                 shutil.copyfileobj(response, out_f)
#             subprocess.run(["chmod", "+x", AppImageToolPath])
#
#         subprocess.run([AppImageToolPath, install_dir])
#
#

def main():
    args = create_parser().parse_args()

    current_system = "windows" if os.name == "nt" else "unix"

    builder = MasterClassBuilder(args, current_system)
    builder.start()

if __name__ == "__main__":
    main()
