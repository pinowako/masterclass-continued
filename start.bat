@echo off

if not DEFINED IS_MINIMIZED set IS_MINIMIZED=1 && start "" /min "%~dpnx0" %* && exit
CALL %~dp0\root\bin\thisroot.bat
SET PATH=%~dp0\bin;%PATH%
SET ROOT_INCLUDE_PATH=%~dp0\headers

"%~dp0\bin\MasterClass.x.exe"

exit
